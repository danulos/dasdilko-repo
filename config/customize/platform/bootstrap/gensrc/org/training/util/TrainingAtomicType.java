package org.training.util;

import java.io.Serializable;

public class TrainingAtomicType implements Serializable {

    private String line;
    private int number;

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
