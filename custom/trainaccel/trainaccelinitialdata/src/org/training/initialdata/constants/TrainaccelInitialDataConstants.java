/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.initialdata.constants;

/**
 * Global class for all TrainaccelInitialData constants.
 */
public final class TrainaccelInitialDataConstants extends GeneratedTrainaccelInitialDataConstants
{
	public static final String EXTENSIONNAME = "trainaccelinitialdata";

	private TrainaccelInitialDataConstants()
	{
		//empty
	}
}
