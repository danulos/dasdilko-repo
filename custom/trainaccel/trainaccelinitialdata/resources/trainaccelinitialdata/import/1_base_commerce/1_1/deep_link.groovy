

    import de.hybris.platform.core.PK
    import de.hybris.platform.core.model.product.ProductModel
    import de.hybris.platform.deeplink.model.rules.DeeplinkUrlModel
    import de.hybris.platform.deeplink.services.DeeplinkUrlService
    import de.hybris.platform.servicelayer.model.ModelService

    ModelService modelService = spring.getBean("modelService")
    DeeplinkUrlService deeplinkUrlService = spring.getBean("deeplinkUrlService")

    DeeplinkUrlModel example = new DeeplinkUrlModel()
    example.setCode("testCode")

    DeeplinkUrlModel deeplinkUrl = modelService.getByExample(example)
    ProductModel product = modelService.get(PK.fromLong(8796289957889L))

    println(deeplinkUrlService.generateShortUrl(deeplinkUrl, product))





