/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.test.constants;

/**
 * 
 */
public class TrainaccelTestConstants extends GeneratedTrainaccelTestConstants
{

	public static final String EXTENSIONNAME = "trainacceltest";

}
