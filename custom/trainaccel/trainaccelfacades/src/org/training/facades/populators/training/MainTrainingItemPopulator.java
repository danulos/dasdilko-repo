package org.training.facades.populators.training;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.training.facades.training.data.SimpleTrainingItemData;
import org.training.model.SimpleTrainingItemModel;

public class MainTrainingItemPopulator implements Populator<SimpleTrainingItemModel, SimpleTrainingItemData> {

    @Override
    public void populate(final SimpleTrainingItemModel source, final SimpleTrainingItemData target) throws ConversionException {
        target.setCode(source.getCode());
        target.setRandomNumber(source.getRandomNumber());
    }
}
