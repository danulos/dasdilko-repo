package org.training.facades.populators.search;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;

    public class SearchResultProductTrainingPopulator implements Populator<SearchResultValueData, ProductData> {

        @Override
        public void populate(final SearchResultValueData source, final ProductData target) {
            target.setRandomValue((Boolean)source.getValues().get("randomValue"));
        }
    }
