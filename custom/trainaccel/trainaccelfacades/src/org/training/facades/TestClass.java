package org.training.facades;

import de.hybris.platform.converters.impl.DefaultModifableConfigurablePopulator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.facades.training.data.SimpleTrainingItemData;
import org.training.model.SimpleTrainingItemModel;

import javax.annotation.Resource;
import java.util.Arrays;

public class TestClass {

    private static final Logger LOG = LoggerFactory.getLogger(TestClass.class);

    @Resource
    private DefaultModifableConfigurablePopulator
            <SimpleTrainingItemModel, SimpleTrainingItemData, String> trainingItemConfiguredPopulator;

    public void populatorOption() {
        final SimpleTrainingItemModel source = new SimpleTrainingItemModel();
        source.setCode("code");
        source.setAttributeOne("one");
        source.setAttributeTwo("two");
        source.setAttributeThree("three");

        final SimpleTrainingItemData target = new SimpleTrainingItemData();

        trainingItemConfiguredPopulator
                .populate(source, target,
                        Arrays.asList("MAIN", "ONE", "TWO", "THREE"));

        LOG.info("Code - {}. number - {}, one - {}, two - {}, three - {}",
                target.getCode(), target.getRandomNumber(), target.getAttributeOne(),
                target.getAttributeTwo(), target.getAttributeThree());
    }

    public void setTrainingItemConfiguredPopulator(DefaultModifableConfigurablePopulator<SimpleTrainingItemModel, SimpleTrainingItemData, String> trainingItemConfiguredPopulator) {
        this.trainingItemConfiguredPopulator = trainingItemConfiguredPopulator;
    }
}
