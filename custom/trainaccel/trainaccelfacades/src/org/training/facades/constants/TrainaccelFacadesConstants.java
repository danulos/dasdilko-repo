/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.facades.constants;

/**
 * Global class for all TrainaccelFacades constants.
 */
public class TrainaccelFacadesConstants extends GeneratedTrainaccelFacadesConstants
{
	public static final String EXTENSIONNAME = "trainaccelfacades";

	private TrainaccelFacadesConstants()
	{
		//empty
	}
}
