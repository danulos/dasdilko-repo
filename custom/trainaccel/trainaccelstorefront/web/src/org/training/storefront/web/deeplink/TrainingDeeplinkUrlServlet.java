package org.training.storefront.web.deeplink;

import de.hybris.platform.core.Registry;
import de.hybris.platform.deeplink.DeeplinkUtils;
import de.hybris.platform.deeplink.services.DeeplinkUrlService;
import org.apache.commons.validator.GenericValidator;
import org.springframework.web.bind.ServletRequestUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TrainingDeeplinkUrlServlet extends HttpServlet {

    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        Registry.activateMasterTenant();
        final String barcodeToken = ServletRequestUtils.getStringParameter(req, DeeplinkUtils.getDeeplinkParameterName());
        if (!GenericValidator.isBlankOrNull(barcodeToken)) {
            final DeeplinkUrlService.LongUrlInfo generatedUrl = this.getDeeplinkUrlService().generateUrl(barcodeToken);
            if (generatedUrl != null) {
                if (generatedUrl.isUseForward()) {
                    RequestDispatcher requestDispatcher = req.getRequestDispatcher(generatedUrl.getUrl());
                    requestDispatcher.forward(req, resp);
                } else {
                    resp.sendRedirect(generatedUrl.getUrl());
                }
            }
        }
    }

    protected DeeplinkUrlService getDeeplinkUrlService() {
        return (DeeplinkUrlService) Registry.getApplicationContext().getBean("deeplinkUrlService");
    }
}