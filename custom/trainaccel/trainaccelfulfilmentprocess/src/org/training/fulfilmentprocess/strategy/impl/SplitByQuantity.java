package org.training.fulfilmentprocess.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.strategy.AbstractSplittingStrategy;


    public class SplitByQuantity extends AbstractSplittingStrategy {

        private final Long MAX = 3L;

        @Override
        public Object getGroupingObject(final AbstractOrderEntryModel orderEntry) {
            return orderEntry.getQuantity() > MAX;
        }

        @Override
        public void afterSplitting(final Object groupingObject, final ConsignmentModel createdOne) {
            //nothing to do
        }
    }
