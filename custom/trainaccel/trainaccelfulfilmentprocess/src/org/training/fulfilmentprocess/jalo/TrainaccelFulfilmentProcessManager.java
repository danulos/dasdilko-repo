/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.training.fulfilmentprocess.constants.TrainaccelFulfilmentProcessConstants;

public class TrainaccelFulfilmentProcessManager extends GeneratedTrainaccelFulfilmentProcessManager
{
	public static final TrainaccelFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (TrainaccelFulfilmentProcessManager) em.getExtension(TrainaccelFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
