package org.training.core.solr.comparator;

import de.hybris.platform.solrfacetsearch.search.FacetValue;

import java.util.Comparator;

public class FacetBooleanNameComparator implements Comparator<FacetValue> {

    @Override
    public int compare(final FacetValue value1, final FacetValue value2) {
        if (value1.getName().equalsIgnoreCase("true")) {
            return 1;
        }
        return -1;
    }
}
