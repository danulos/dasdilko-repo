package org.training.core.solr.provider.identity;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.provider.IdentityProvider;

import java.util.Random;

public class TrainingProductIdentityProvider implements IdentityProvider<ProductModel> {

    public String getIdentifier(final IndexConfig indexConfig, final ProductModel product) {
        final CatalogVersionModel catalogVersion = product.getCatalogVersion();
        final String productCode = product.getCode();
        final String catalogId = catalogVersion.getCatalog().getId();
        final int randomNumber = new Random().nextInt();
        return catalogId + "/" + catalogVersion.getVersion() + "/" + productCode + "/" + randomNumber;
    }

}
