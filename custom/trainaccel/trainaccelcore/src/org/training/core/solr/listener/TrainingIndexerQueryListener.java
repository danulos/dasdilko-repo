package org.training.core.solr.listener;

import de.hybris.platform.solrfacetsearch.indexer.IndexerQueryContext;
import de.hybris.platform.solrfacetsearch.indexer.IndexerQueryListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrainingIndexerQueryListener implements IndexerQueryListener {

    private static final Logger LOG = LoggerFactory.getLogger(TrainingIndexerQueryListener.class);

    public void beforeQuery(IndexerQueryContext indexerQueryContext) {
//        LOG.info("TrainingIndexerQueryListener.beforeQuery");
    }

    public void afterQuery(IndexerQueryContext indexerQueryContext) {
//        LOG.info("TrainingIndexerQueryListener.afterQuery");
    }

    public void afterQueryError(IndexerQueryContext indexerQueryContext) {
//        LOG.info("TrainingIndexerQueryListener.afterQueryError");
    }

}
