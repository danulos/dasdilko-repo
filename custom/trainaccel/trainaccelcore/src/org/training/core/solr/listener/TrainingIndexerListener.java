package org.training.core.solr.listener;

import de.hybris.platform.solrfacetsearch.indexer.IndexerContext;
import de.hybris.platform.solrfacetsearch.indexer.IndexerListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrainingIndexerListener implements IndexerListener {
    private static final Logger LOG = LoggerFactory.getLogger(TrainingIndexerListener.class);

    public void beforeIndex(IndexerContext indexerContext) {
//        LOG.info("TrainingIndexerListener.beforeIndex");
    }

    public void afterIndex(IndexerContext indexerContext) {
//        LOG.info("TrainingIndexerListener.afterIndex");
    }

    public void afterIndexError(IndexerContext indexerContext) {
//        LOG.info("TrainingIndexerListener.afterIndexError");
    }

}
