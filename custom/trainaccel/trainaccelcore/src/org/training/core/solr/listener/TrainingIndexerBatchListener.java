package org.training.core.solr.listener;

import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrainingIndexerBatchListener implements IndexerBatchListener {

    private static final Logger LOG = LoggerFactory.getLogger(TrainingIndexerBatchListener.class);

    public void beforeBatch(IndexerBatchContext indexerBatchContext) {
//        LOG.info("TrainingIndexerBatchListener.beforeBatch");
    }

    public void afterBatch(IndexerBatchContext indexerBatchContext) {
//        LOG.info("TrainingIndexerBatchListener.afterBatch");
    }

    public void afterBatchError(IndexerBatchContext indexerBatchContext) {
//        LOG.info("TrainingIndexerBatchListener.afterBatchError");
    }

}
