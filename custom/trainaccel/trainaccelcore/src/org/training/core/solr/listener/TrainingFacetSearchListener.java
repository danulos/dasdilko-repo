package org.training.core.solr.listener;

import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrainingFacetSearchListener implements FacetSearchListener {

    private static final Logger LOG = LoggerFactory.getLogger(TrainingFacetSearchListener.class);

    public void beforeSearch(FacetSearchContext facetSearchContext) throws FacetSearchException {
//        LOG.info("TrainingFacetSearchListener.beforeSearch");
    }

    public void afterSearch(FacetSearchContext facetSearchContext) throws FacetSearchException {
//        LOG.info("TrainingFacetSearchListener.afterSearch");
    }

    public void afterSearchError(FacetSearchContext facetSearchContext) throws FacetSearchException {
//        LOG.info("TrainingFacetSearchListener.afterSearchError");
    }

}
