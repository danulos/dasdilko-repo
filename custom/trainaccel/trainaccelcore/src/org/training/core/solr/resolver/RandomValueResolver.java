package org.training.core.solr.resolver;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;

import java.util.Random;

public class RandomValueResolver extends AbstractValueResolver<ProductModel, Object, Boolean> {

    @Override
    protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
                                  final IndexedProperty indexedProperty, final ProductModel model,
                                  final ValueResolverContext<Object, Boolean> resolverContext) throws FieldValueProviderException {
        document.addField(indexedProperty, new Random().nextBoolean(), resolverContext.getFieldQualifier());
    }

}
