package org.training.core.cart.hook;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.hook.CommerceAddToCartMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrainingCommerceAddToCartMethodHook implements CommerceAddToCartMethodHook {

    private static final Logger LOG = LoggerFactory.getLogger(TrainingCommerceAddToCartMethodHook.class);

    public void beforeAddToCart(CommerceCartParameter parameters) throws CommerceCartModificationException {
        final String trainingProperty = parameters.getTrainingProperty();
        LOG.info("trainingProperty - {}", trainingProperty);
    }

    public void afterAddToCart(CommerceCartParameter parameters, CommerceCartModification result)
            throws CommerceCartModificationException {
        LOG.info("TrainingCommerceAddToCartMethodHook.afterAddToCart");
    }
}
