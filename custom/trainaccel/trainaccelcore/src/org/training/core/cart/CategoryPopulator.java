package org.training.core.cart;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.converters.Populator;

	public class CategoryPopulator implements Populator<CategoryModel, CategoryData> {

		@Override
		public void populate(final CategoryModel source, final CategoryData target) {
			target.setCode(source.getCode());
			target.setName(source.getName());
		}

	}