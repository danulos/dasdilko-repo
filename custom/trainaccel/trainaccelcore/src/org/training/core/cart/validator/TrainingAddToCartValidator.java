package org.training.core.cart.validator;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.validator.AddToCartValidator;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.core.cart.CategoryPopulator;

import java.util.Random;

public class TrainingAddToCartValidator implements AddToCartValidator {

    private static final Logger LOG = LoggerFactory.getLogger(TrainingAddToCartValidator.class);

    public boolean supports(final CommerceCartParameter parameter) {
        final boolean result = new Random().nextBoolean();
        LOG.info("TrainingAddToCartValidator.supports, result = {}", result);
        return result;
    }

    public void validate(final CommerceCartParameter parameter) throws CommerceCartModificationException {
        LOG.info("TrainingAddToCartValidator.validate");
        CategoryPopulator p = new CategoryPopulator();
    }

}
