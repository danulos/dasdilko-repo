package org.training.core.cart.hook;

import de.hybris.platform.commerceservices.order.hook.CommercePlaceOrderMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.order.InvalidCartException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrainingCommercePlaceOrderMethodHook implements CommercePlaceOrderMethodHook {

    private static final Logger LOG = LoggerFactory.getLogger(TrainingCommercePlaceOrderMethodHook.class);

    public void beforePlaceOrder(CommerceCheckoutParameter parameter) throws InvalidCartException {
        LOG.info("TrainingCommercePlaceOrderMethodHook.beforePlaceOrder");
    }

    public void beforeSubmitOrder(CommerceCheckoutParameter parameter, CommerceOrderResult result) throws InvalidCartException {
        LOG.info("TrainingCommercePlaceOrderMethodHook.beforeSubmitOrder");
    }

    public void afterPlaceOrder(CommerceCheckoutParameter parameter, CommerceOrderResult orderModel) throws InvalidCartException {
        LOG.info("TrainingCommercePlaceOrderMethodHook.afterPlaceOrder");
    }
}
