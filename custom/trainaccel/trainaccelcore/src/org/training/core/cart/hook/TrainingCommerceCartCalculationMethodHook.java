package org.training.core.cart.hook;

import de.hybris.platform.commerceservices.order.hook.CommerceCartCalculationMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrainingCommerceCartCalculationMethodHook implements CommerceCartCalculationMethodHook {

    private static final Logger LOG = LoggerFactory.getLogger(TrainingCommerceCartCalculationMethodHook.class);

    public void beforeCalculate(CommerceCartParameter parameters) {
        LOG.info("TrainingCommerceCartCalculationMethodHook.beforeCalculate");
    }

    public void afterCalculate(CommerceCartParameter parameters) {
        LOG.info("TrainingCommerceCartCalculationMethodHook.afterCalculate");
    }
}
