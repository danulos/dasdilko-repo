package org.training.core.cart.hook;

import de.hybris.platform.commerceservices.order.hook.AuthorizePaymentMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrainingAuthorizePaymentMethodHook implements AuthorizePaymentMethodHook {

    private static final Logger LOG = LoggerFactory.getLogger(TrainingAuthorizePaymentMethodHook.class);

    public void beforeAuthorizePaymentAmount(CommerceCheckoutParameter parameter) {
        LOG.info("TrainingAuthorizePaymentMethodHook.beforeAuthorizePaymentAmount");
    }

    public void afterAuthorizePaymentAmount(CommerceCheckoutParameter parameter, PaymentTransactionEntryModel paymentTransactionEntryModel) {
        LOG.info("TrainingAuthorizePaymentMethodHook.afterAuthorizePaymentAmount");
    }
}
