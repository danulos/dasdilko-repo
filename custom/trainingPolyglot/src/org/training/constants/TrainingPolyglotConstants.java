/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.constants;

@SuppressWarnings({"deprecation","squid:CallToDeprecatedMethod"})
public class TrainingPolyglotConstants extends GeneratedTrainingPolyglotConstants
{
	public static final String EXTENSIONNAME = "trainingPolyglot";
	
	private TrainingPolyglotConstants()
	{
		//empty
	}
	
	
}
