/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.persistence.polyglot.repository.documentcart.cart;

import de.hybris.platform.persistence.polyglot.model.Key;
import de.hybris.platform.persistence.polyglot.PolyglotPersistence;
import de.hybris.platform.persistence.polyglot.model.SingleAttributeKey;


public final class CartAttributes
{
	private static final SingleAttributeKey ORDER_ATTRIBUTE = PolyglotPersistence.getNonlocalizedKey("order");
	private static final SingleAttributeKey CODE_ATTRIBUTE = PolyglotPersistence.getNonlocalizedKey("code");
	private static final SingleAttributeKey GUID_ATTRIBUTE = PolyglotPersistence.getNonlocalizedKey("guid");
	private static final SingleAttributeKey NUMBER_ATTRIBUTE = PolyglotPersistence.getNonlocalizedKey("number");
	private static final SingleAttributeKey LOCALIZED_ATTRIBUTE = PolyglotPersistence.getNonlocalizedKey("localized");

	private CartAttributes()
	{
		//no instantiation
	}

	public static SingleAttributeKey order()
	{
		return ORDER_ATTRIBUTE;
	}

	public static boolean isOrder(final Key key)
	{
		return order().equals(key);
	}

	public static SingleAttributeKey code()
	{
		return CODE_ATTRIBUTE;
	}

	public static boolean isCode(final Key key)
	{
		return code().equals(key);
	}

	public static SingleAttributeKey guid()
	{
		return GUID_ATTRIBUTE;
	}

	public static boolean isGuid(final Key key)
	{
		return guid().equals(key);
	}

	public static SingleAttributeKey number()
	{
		return NUMBER_ATTRIBUTE;
	}

	public static boolean isNumber(final Key key)
	{
		return number().equals(key);
	}

	public static SingleAttributeKey localized()
	{
		return LOCALIZED_ATTRIBUTE;
	}

	public static boolean isLocalized(final Key key)
	{
		return localized().equals(key);
	}

	static class ReferenceAttributes {
		static final SingleAttributeKey INNER_ITEM = PolyglotPersistence.getNonlocalizedKey("innerItem");
	}
}