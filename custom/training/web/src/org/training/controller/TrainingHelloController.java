package org.training.controller;

import static org.training.constants.TrainingConstants.PLATFORM_LOGO_CODE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.training.service.TrainingService;


import io.jsonwebtoken.Claims; //training_core
import org.training.service.data.AuditDemo;
import org.training.service.data.ModelServiceDemo;
import org.training.service.data.ValidationDemo;
import org.training.service.data.search.PolyglotDemo;
import org.training.service.data.search.TransactionDemo;
import org.training.service.functional.CacheDemo;
import org.training.service.functional.EventDemo;
import org.training.service.functional.JobDemo;
import org.training.service.functional.ProcessDemo;
import org.training.service.functional.ScriptDemo;
import org.training.service.functional.TaskDemo;
import org.training.service.functional.WorkflowDemo;

import javax.xml.ws.AsyncHandler; //training_web

//import com.google.gson.ExclusionStrategy; //req_training_core
//import io.vavr.Tuple; //req_training_web

@Controller
public class TrainingHelloController
{
	@Autowired
	private TrainingService trainingService;
	@Autowired
	private ProcessDemo processDemo;
	@Autowired
	private TaskDemo taskDemo;
	@Autowired
	private JobDemo jobDemo;
	@Autowired
	private WorkflowDemo workflowDemo;
	@Autowired
	private ScriptDemo scriptDemo;
	@Autowired
	private EventDemo eventDemo;
	@Autowired
	private ModelServiceDemo modelServiceDemo;
	@Autowired
	private ValidationDemo validationDemo;
	@Autowired
	private AuditDemo auditDemo;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(final ModelMap model)
	{
//		model.addAttribute("logoUrl", trainingService.getHybrisLogoUrl(PLATFORM_LOGO_CODE));
		processDemo.demo();
		return "welcome";
	}

	@RequestMapping(value = "/eventDemo", method = RequestMethod.GET)
	public String eventDemo() {
		eventDemo.demo();
		return "welcome";
	}

	@RequestMapping(value = "/modelServiceDemo", method = RequestMethod.GET)
	public String modelServiceDemo() {
		modelServiceDemo.demo();
		return "welcome";
	}

	@RequestMapping(value = "/validationDemo", method = RequestMethod.GET)
	public String validationDemo() {
		validationDemo.demo();
		return "welcome";
	}

	@RequestMapping(value = "/auditDemo", method = RequestMethod.GET)
	public String auditDemo() {
		auditDemo.demo();
		return "welcome";
	}

	@RequestMapping(value = "/event/{code}", method = RequestMethod.GET)
	public String event(@PathVariable("code") final String code) {
		processDemo.event(code);
		return "welcome";
	}

	@RequestMapping(value = "/choice/{option}", method = RequestMethod.GET)
	public String choice(@PathVariable("option") final String option) {
		processDemo.choice(option);
		return "welcome";
	}

	@RequestMapping(value = "/task", method = RequestMethod.GET)
	public String task() {
		taskDemo.demo();
		return "welcome";
	}

	@RequestMapping(value = "/task/event", method = RequestMethod.GET)
	public String taskEvent() {
		taskDemo.taskEvent();
		return "welcome";
	}

	@RequestMapping(value = "/task/event/1", method = RequestMethod.GET)
	public String taskEventOne() {
		taskDemo.taskEventOne();
		return "welcome";
	}

	@RequestMapping(value = "/task/event/2", method = RequestMethod.GET)
	public String taskEventTwo() {
		taskDemo.taskEventTwo();
		return "welcome";
	}

	@RequestMapping(value = "/job", method = RequestMethod.GET)
	public String job() {
		jobDemo.demo();
		return "welcome";
	}

	@RequestMapping(value = "/work", method = RequestMethod.GET)
	public String work() {
		workflowDemo.demo();
		return "welcome";
	}

	@RequestMapping(value = "/work/proceed", method = RequestMethod.GET)
	public String workProceed() {
		workflowDemo.proceed();
		return "welcome";
	}

	@RequestMapping(value = "/script", method = RequestMethod.GET)
	public String script() {
		scriptDemo.demo();
		return "welcome";
	}

	@RequestMapping(value = "/script/auto", method = RequestMethod.GET)
	public String scriptAuto() {
		scriptDemo.scriptFromDb_bad_auto_disable();
		return "welcome";
	}

	@RequestMapping(value = "/script/no", method = RequestMethod.GET)
	public String scriptNo() {
		scriptDemo.scriptFromDb_bad_no_disable();
		return "welcome";
	}
}
