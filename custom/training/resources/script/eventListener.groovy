import de.hybris.platform.servicelayer.event.impl.AbstractEventListener
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.training.event.async.AsyncTrainingEvent
import org.training.service.AbstractDemo

class ScriptingTrainingEventListener extends AbstractEventListener<AsyncTrainingEvent> {
    protected static final Logger LOG = LoggerFactory.getLogger(AbstractDemo.class)

    @Override
    void onEvent(AsyncTrainingEvent event) {
        LOG.info("ScriptingTrainingEventListener got " + event.getCode() + " event.")
    }
}
new ScriptingTrainingEventListener()