import de.hybris.platform.cronjob.enums.CronJobResult
import de.hybris.platform.cronjob.enums.CronJobStatus
import de.hybris.platform.servicelayer.cronjob.PerformResult
import org.training.model.SimpleTrainingItemModel

log.info("Scripting Job - " + cronjob.code + " is started")
List<SimpleTrainingItemModel> items = flexibleSearchService
        .search("SELECT {pk} FROM {SimpleTrainingItem} WHERE {randomNumber} > 10000").getResult()
log.info("Results found - " + items.size())
for (SimpleTrainingItemModel item : items) {
    item.setAttributeOne("someValue")
}
modelService.saveAll(new ArrayList(items))

log.info("Scripting Job - " + cronjob.code + " is finished")
new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED)
