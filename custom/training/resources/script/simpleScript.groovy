import org.training.model.SimpleTrainingItemModel

List<SimpleTrainingItemModel> items = flexibleSearchService
        .search("SELECT {pk} FROM {SimpleTrainingItem} WHERE {randomNumber} > 10000").getResult()
println("Results found - " + items.size())
for (SimpleTrainingItemModel item : items) {
    item.setAttributeOne("someValue")
}
modelService.saveAll(new ArrayList(items))