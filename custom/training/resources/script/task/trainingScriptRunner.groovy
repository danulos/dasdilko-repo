package script.task

import de.hybris.platform.task.TaskModel
import de.hybris.platform.task.TaskRunner
import de.hybris.platform.task.TaskService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.training.process.task.context.TrainingTaskContext
import org.training.service.AbstractDemo

class TrainingScriptRunner implements TaskRunner<TaskModel> {
    protected static final Logger LOG = LoggerFactory.getLogger(AbstractDemo.class)

    @Override
    void run(TaskService taskService, TaskModel task) {
        final TrainingTaskContext taskContext = task.getContext()
        LOG.info("MyScriptRunner run " + taskContext.getCode() + " task.")
    }

    @Override
    void handleError(TaskService taskService, TaskModel task, Throwable error) {
        LOG.info("MyScriptRunner handleError " + task.getPk().toString() + " task.")
    }
}
new TrainingScriptRunner()