package script.task.processor

import de.hybris.platform.core.PK
import de.hybris.platform.processing.model.SimpleBatchModel
import de.hybris.platform.processing.distributed.simple.SimpleBatchProcessor
import de.hybris.platform.servicelayer.model.ModelService
import org.training.model.SimpleTrainingItemModel
import org.training.service.TrainingService

class ScriptingBatchProcessor implements SimpleBatchProcessor {
    ModelService modelService
    TrainingService trainingService

    void process(final SimpleBatchModel inputBatch) {
        final List<PK> pks = (List<PK>)inputBatch.getContext()
        for (PK pk : pks) {
            SimpleTrainingItemModel item = modelService.<SimpleTrainingItemModel> get(pk)
            trainingService.printTrainingItem(item)
            modelService.remove(item)
        }
    }
}
new ScriptingBatchProcessor(modelService: modelService, trainingService: trainingService)
