import de.hybris.platform.processing.distributed.DistributedProcessService
import de.hybris.platform.processing.distributed.simple.data.QueryBasedCreationData

DistributedProcessService distributedProcessService

def data = QueryBasedCreationData.builder()
        .withQuery("SELECT {pk} FROM {simpleTrainingItem} WHERE {randomNumber} > 50")
        .withScriptCode("scriptingBatchProcessor")
        .build()
def process = distributedProcessService.create(data)
distributedProcessService.start(process.getCode())

