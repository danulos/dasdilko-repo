package org.training.util;

import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.config.ConfigurationService;

public class StaticConfigWrapper {
    private static ConfigurationService configurationService;

    public static String getConfig(final String key) {
        return getConfigurationService().getConfiguration().getString(key);
    }

    private static ConfigurationService getConfigurationService() {
        if (configurationService == null) {
            configurationService = (ConfigurationService)
                    Registry.getApplicationContext().getBean("configurationService");
        }
        return configurationService;
    }
}
