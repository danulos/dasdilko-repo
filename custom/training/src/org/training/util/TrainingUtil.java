package org.training.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class TrainingUtil {

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void printMessage(final String message) {
        printLine();
        System.out.println(message);
    }

    public static void printLine() {
        System.out.println("=======================================");
    }

    public static String getCode() {
        return UUID.randomUUID().toString();
    }

    public static void sleepFor(final long time) {
        try {
            Thread.sleep(time);
        } catch (Exception e) {

        }
    }

    public static String convert(final Date date) {
        return simpleDateFormat.format(date);
    }

    public static Date convert(final String date) {
        try {
            return simpleDateFormat.parse(date);
        } catch (ParseException e) {
            return new Date();
        }
    }
}
