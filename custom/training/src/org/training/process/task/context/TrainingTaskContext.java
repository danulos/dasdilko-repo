package org.training.process.task.context;

import java.io.Serializable;

public class TrainingTaskContext implements Serializable {

    private static final long serialVersionUID = 1L;

    private String code;
    private int number;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
