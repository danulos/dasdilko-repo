package org.training.process.task;

import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.task.TaskRunner;
import de.hybris.platform.task.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.task.model.TrainingTaskModel;

public class CustomTrainingTask implements TaskRunner<TrainingTaskModel> {
    private static final Logger LOG = LoggerFactory.getLogger(CustomTrainingTask.class);

    public void run(TaskService taskService, TrainingTaskModel task) throws RetryLaterException {
        LOG.info("CustomTrainingTask - run");
        LOG.info("data - {}, {};", task.getCode(), task.getNumber());
    }

    public void handleError(TaskService taskService, TrainingTaskModel task, Throwable error) {
        LOG.info("CustomTrainingTask - handleError");
    }
}
