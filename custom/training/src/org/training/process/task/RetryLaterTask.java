package org.training.process.task;

import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.TaskRunner;
import de.hybris.platform.task.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RetryLaterTask implements TaskRunner<TaskModel> {
    private static final Logger LOG = LoggerFactory.getLogger(RetryLaterTask.class);
    private static final int MAX_RETRIES = 10;

    public void run(TaskService taskService, TaskModel task) throws RetryLaterException {
        LOG.info("RetryLaterTask - run, retry - {}", task.getRetry());
        if(task.getRetry() <= MAX_RETRIES) {
            RetryLaterException ex = new RetryLaterException("Retry later...");
            ex.setDelay(1000); // 1 second
            throw ex;
        }
        LOG.info("RetryLaterTask - done");
    }

    public void handleError(TaskService taskService, TaskModel task, Throwable error) {
        LOG.info("TrainingTask - handleError");
    }
}
