package org.training.process.task;

import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.TaskRunner;
import de.hybris.platform.task.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.process.task.context.TrainingTaskContext;

public class TrainingTask implements TaskRunner<TaskModel> {
    private static final Logger LOG = LoggerFactory.getLogger(TrainingTask.class);

    public void run(TaskService taskService, TaskModel task) throws RetryLaterException {
        LOG.info("TrainingTask - run");
        if (task.getContext() instanceof TrainingTaskContext) {
            final TrainingTaskContext context = (TrainingTaskContext)task.getContext();
            LOG.info("context - {}, {};", context.getCode(), context.getNumber());
        }
    }

    public void handleError(TaskService taskService, TaskModel task, Throwable error) {
        LOG.info("TrainingTask - handleError");
    }
}
