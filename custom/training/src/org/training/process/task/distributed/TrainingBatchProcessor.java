package org.training.process.task.distributed;

import com.google.common.base.Preconditions;
import de.hybris.platform.core.PK;
import de.hybris.platform.processing.distributed.simple.SimpleBatchProcessor;
import de.hybris.platform.processing.model.SimpleBatchModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.model.SimpleTrainingItemModel;

import javax.annotation.Resource;
import java.util.List;

public class TrainingBatchProcessor implements SimpleBatchProcessor {
    private static final Logger LOG = LoggerFactory.getLogger(TrainingBatchProcessor.class);

    @Resource
    private ModelService modelService;

    @Override
    public void process(final SimpleBatchModel inputBatch) {
        final List<PK> pks = asListOfPks(inputBatch.getContext());
        pks.stream().map(pk -> modelService.<SimpleTrainingItemModel> get(pk)).forEach(item -> {
            LOG.info("Processing batch - {}", item.getCode());
        });
    }

    private List<PK> asListOfPks(final Object ctx) {
        Preconditions.checkState(ctx instanceof List, "ctx must be instance of a List");
        return (List<PK>) ctx;
    }
}
