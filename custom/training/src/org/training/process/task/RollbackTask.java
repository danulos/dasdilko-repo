package org.training.process.task;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.TaskRunner;
import de.hybris.platform.task.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.model.SimpleTrainingItemModel;
import org.training.process.task.context.TrainingTaskContext;
import org.training.util.TrainingUtil;

import javax.annotation.Resource;

public class RollbackTask implements TaskRunner<TaskModel> {
    private static final Logger LOG = LoggerFactory.getLogger(RollbackTask.class);

    @Resource
    private ModelService modelService;

    public void run(TaskService taskService, TaskModel task) throws RetryLaterException {
        LOG.info("RollbackTask - run");
        if (task.getContext() instanceof TrainingTaskContext) {
            final TrainingTaskContext context = (TrainingTaskContext)task.getContext();
            if (context.getNumber() > 10) {
                LOG.info("RollbackTask - throw exception");
                final SimpleTrainingItemModel item = modelService.create(SimpleTrainingItemModel.class);
                item.setCode(TrainingUtil.getCode());
                modelService.save(item);

                final RetryLaterException exception = new RetryLaterException();
                exception.setRollBack(false);
                throw exception;

            }
            LOG.info("context - {}, {};", context.getCode(), context.getNumber());
        }
    }

    public void handleError(TaskService taskService, TaskModel task, Throwable error) {
        LOG.info("RollbackTask - handleError");
    }
}
