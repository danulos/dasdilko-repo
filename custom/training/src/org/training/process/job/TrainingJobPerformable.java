package org.training.process.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.jalo.CronJobProgressTracker;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.job.model.TrainingCronJobModel;
import org.training.util.TrainingUtil;

public class TrainingJobPerformable extends AbstractJobPerformable<TrainingCronJobModel> {
    private static final Logger LOG = LoggerFactory.getLogger(TrainingJobPerformable.class.getName());
    private final int loop = 1;

    @Override
    public PerformResult perform(final TrainingCronJobModel cronJobModel) {
        LOG.info("TrainingJobPerformable - {} is executed", cronJobModel.getTrainingCode());
        final CronJobProgressTracker tracker
                = new CronJobProgressTracker(modelService.getSource(cronJobModel));
        for (int i = 0; i < 100; i++) {
            tracker.setProgress((double) i);
            if (clearAbortRequestedIfNeeded(cronJobModel)) {
                return new PerformResult(CronJobResult.UNKNOWN, CronJobStatus.ABORTED);
            }
            TrainingUtil.sleepFor(500);
        }
        tracker.close();
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    @Override
    public boolean isAbortable() {
        return true;
    }
}
