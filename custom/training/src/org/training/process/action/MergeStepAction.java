package org.training.process.action;

import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.processing.model.TrainingProcessModel;

import java.util.Random;

public class MergeStepAction extends AbstractSimpleDecisionAction<TrainingProcessModel> {
    private static final Logger LOG = LoggerFactory.getLogger(MergeStepAction.class);

    @Override
    public Transition executeAction(final TrainingProcessModel process) {
        LOG.info("MergeStepAction - {}", process.getCode());
        if (new Random().nextBoolean()) {
            LOG.info("MergeStepAction returns NOK");
            return Transition.NOK;
        }
        LOG.info("MergeStepAction returns OK");
        return Transition.OK;
    }
}
