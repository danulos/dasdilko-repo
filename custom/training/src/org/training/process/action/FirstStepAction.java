package org.training.process.action;

import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.processing.model.TrainingProcessModel;

public class FirstStepAction extends AbstractSimpleDecisionAction<TrainingProcessModel> {
    private static final Logger LOG = LoggerFactory.getLogger(FirstStepAction.class);

    @Override
    public Transition executeAction(final TrainingProcessModel process) {
        LOG.info("FirstStepAction - {}", process.getCode());
        if (StringUtils.isEmpty(process.getTrainingProcessAttribute())) {
            LOG.info("FirstStepAction returns NOK");
            return Transition.NOK;
        }
        LOG.info("FirstStepAction returns OK");
        return Transition.OK;
    }
}
