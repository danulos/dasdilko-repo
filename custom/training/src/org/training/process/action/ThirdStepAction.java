package org.training.process.action;

import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.task.RetryLaterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.processing.model.TrainingProcessModel;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class ThirdStepAction extends AbstractAction<TrainingProcessModel> {
    private static final Logger LOG = LoggerFactory.getLogger(ThirdStepAction.class);

    public enum Transition {
        OK, NOK, CUSTOM;

        public static Set<String> getStringValues() {
            return Arrays.stream(Transition.values())
                    .map(Transition::toString).collect(Collectors.toSet());
        }
    }

    @Override
    public String execute(final TrainingProcessModel process) {
        LOG.info("ThirdStepAction returns OK");
        return Transition.OK.toString();
//        LOG.info("ThirdStepAction returns CUSTOM");
//        return Transition.CUSTOM.toString();
    }

    @Override
    public Set<String> getTransitions() {
        return Transition.getStringValues();
    }
}
