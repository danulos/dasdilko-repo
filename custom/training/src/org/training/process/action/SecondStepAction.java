package org.training.process.action;

import de.hybris.platform.processengine.action.AbstractProceduralAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.processing.model.TrainingProcessModel;

public class SecondStepAction extends AbstractProceduralAction<TrainingProcessModel> {
    private static final Logger LOG = LoggerFactory.getLogger(SecondStepAction.class);

    @Override
    public void executeAction(final TrainingProcessModel process) {
        LOG.info("SecondStepAction returns OK");
    }
}
