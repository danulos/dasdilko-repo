package org.training.process.action;

import de.hybris.platform.processengine.action.AbstractProceduralAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.processing.model.TrainingProcessModel;

public class FourthStepAction extends AbstractProceduralAction<TrainingProcessModel> {
    private static final Logger LOG = LoggerFactory.getLogger(FourthStepAction.class);

    @Override
    public void executeAction(final TrainingProcessModel process) {
        LOG.info("FourthStepAction returns OK");
    }
}
