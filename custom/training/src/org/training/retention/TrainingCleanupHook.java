package org.training.retention;

import de.hybris.platform.retention.hook.ItemCleanupHook;
import de.hybris.platform.servicelayer.model.ModelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.model.SimpleTrainingItemModel;

import javax.annotation.Resource;

public class TrainingCleanupHook implements ItemCleanupHook<SimpleTrainingItemModel> {

    private static final Logger LOG = LoggerFactory.getLogger(TrainingCleanupHook.class);

    @Resource
    private ModelService modelService;

    @Override
    public void cleanupRelatedObjects(final SimpleTrainingItemModel trainingItem) {
        LOG.info("TrainingCleanupHook.cleanupRelatedObjects({})", trainingItem.getCode());
        if (trainingItem.getWrappedOne() != null) {
            modelService.remove(trainingItem.getWrappedOne());
        }
    }
}
