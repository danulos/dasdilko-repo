package org.training.retention;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.persistence.audit.gateway.WriteAuditGateway;
import de.hybris.platform.persistence.audit.internal.AuditEnablementService;
import de.hybris.platform.processing.model.AbstractRetentionRuleModel;
import de.hybris.platform.retention.ItemToCleanup;
import de.hybris.platform.retention.RetentionCleanupAction;
import de.hybris.platform.retention.job.AfterRetentionCleanupJobPerformable;
import de.hybris.platform.servicelayer.model.ModelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.model.SimpleTrainingItemModel;

import javax.annotation.Resource;

public class TrainingCleanupAction implements RetentionCleanupAction {

    private static final Logger LOG = LoggerFactory.getLogger(TrainingCleanupAction.class);

    @Resource
    private ModelService modelService;
    @Resource
    private WriteAuditGateway writeAuditGateway;
    @Resource
    private AuditEnablementService auditingEnablementService;

    public void cleanup(final AfterRetentionCleanupJobPerformable retentionJob,
                        final AbstractRetentionRuleModel rule, final ItemToCleanup item) {
        if (shouldBeCleanedUp(item)) {
            basicCleanUpLogic(item);
        }
    }

    private boolean shouldBeCleanedUp(final ItemToCleanup item) {
        boolean result = false;
        final ItemModel itemModel = modelService.get(item.getPk());
        if (itemModel instanceof SimpleTrainingItemModel) {
            final SimpleTrainingItemModel trainingItem = (SimpleTrainingItemModel)itemModel;
            result = !trainingItem.getCode().contains("reserved");
        }
        return result;
    }

    private void basicCleanUpLogic(final ItemToCleanup item) {
        LOG.info("Removing item {}", item);
        this.modelService.remove(item.getPk());
        if (this.auditingEnablementService.isAuditEnabledForType(item.getItemType())) {
            this.removeAuditRecords(item);
        }
    }

    private void removeAuditRecords(final ItemToCleanup item) {
        this.writeAuditGateway.removeAuditRecordsForType(item.getItemType(), item.getPk());
    }

}
