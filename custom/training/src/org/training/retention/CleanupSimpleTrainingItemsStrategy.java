package org.training.retention;

import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.jobs.maintenance.MaintenanceCleanupStrategy;
import de.hybris.platform.servicelayer.internal.model.MaintenanceCleanupJobModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.model.SimpleTrainingItemModel;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

public class CleanupSimpleTrainingItemsStrategy implements MaintenanceCleanupStrategy<SimpleTrainingItemModel, CronJobModel> {

    private static final Logger LOG = LoggerFactory.getLogger(CleanupSimpleTrainingItemsStrategy.class);

    @Resource
    private ModelService modelService;

    private Integer threshold = 20;

    @Override
    public FlexibleSearchQuery createFetchQuery(final CronJobModel cjm) {
        setThreshold(cjm);
        final String query = "SELECT {" + SimpleTrainingItemModel.PK + "} FROM {" + SimpleTrainingItemModel._TYPECODE + "}"
                + " WHERE {" + SimpleTrainingItemModel.CODE + "} LIKE '%maintenance%'"
                + " AND {" + SimpleTrainingItemModel.RANDOMNUMBER + "} > ?threshold";
        return new FlexibleSearchQuery(query,
                Collections.singletonMap("threshold", this.threshold));
    }

    @Override
    public void process(final List<SimpleTrainingItemModel> elements) {
        for (SimpleTrainingItemModel trainingItem : elements) {
            LOG.info("Removing {} item", trainingItem.getCode());
            modelService.remove(trainingItem);
        }
    }

    private void setThreshold(final CronJobModel cjm) {
        if (cjm.getJob() instanceof MaintenanceCleanupJobModel
                && ((MaintenanceCleanupJobModel) cjm.getJob()).getThreshold() != null) {
            final Integer threshold =((MaintenanceCleanupJobModel) cjm.getJob()).getThreshold();
            if (threshold < 0) {
                throw new IllegalArgumentException("threshold cannot be negative.");
            }
            this.threshold = threshold;
        }
    }
}
