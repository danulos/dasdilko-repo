package org.training.pojo;

import de.hybris.platform.validation.annotations.Dynamic;
import de.hybris.platform.validation.annotations.XorNotNull;
import de.hybris.platform.validation.enums.ValidatorLanguage;
import org.training.constraint.training.Divisible;

import javax.validation.constraints.*;
import java.util.Date;

    @Dynamic(language = ValidatorLanguage.BEANSHELL,
            expression = "return (getAttributeOne() == getAttributeTwo());")
    @XorNotNull(firstFieldName = "xorNotNullOne",
            secondFieldName = "xorNotNullTwo")
    public class SimpleTrainingPojo {

        private String xorNotNullOne;
        private String xorNotNullTwo;

        private boolean attributeOne;
        private boolean attributeTwo;

        public boolean getAttributeOne() {
            return attributeOne;
        }
        public boolean getAttributeTwo() {
            return attributeTwo;
        }

    @Pattern(regexp = "([a-zA-Z])")
    private String patterned;

    @Past
    private Date past;

    @Future
    private Date future;

    @Min(value = 5)
    private int min;

    @Max(value = 10)
    private int max;

    @DecimalMin(value = "5.0")
    private double decimalMin;

    @DecimalMax(value = "10.0", inclusive = false)
    private double decimalMax;

    @Size(min = 5, max = 10)
    private String sized;

    @AssertTrue
    private boolean eqTrue;

    @AssertFalse
    private boolean eqFalse;

    @Null
    private Object isNull;

    @NotNull
    private Object notNull;

    @NotEmpty
    private String notEmpty;

    @NotBlank
    private String notBlank;

    @Divisible(number = 3)
    private int divisible;

        public int getDivisible() {
            return divisible;
        }

        public void setDivisible(int divisible) {
            this.divisible = divisible;
        }

        public String getXorNotNullOne() {
        return xorNotNullOne;
    }

    public void setXorNotNullOne(String xorNotNullOne) {
        this.xorNotNullOne = xorNotNullOne;
        boolean v1 = getAttributeOne();
        boolean v2 = getAttributeTwo();
        if (v1 & v2) {

        }
    }

    public String getXorNotNullTwo() {
        return xorNotNullTwo;
    }

    public void setXorNotNullTwo(String xorNotNullTwo) {
        this.xorNotNullTwo = xorNotNullTwo;
    }

    public void setAttributeOne(boolean attributeOne) {
        this.attributeOne = attributeOne;
    }

    public void setAttributeTwo(boolean attributeTwo) {
        this.attributeTwo = attributeTwo;
    }

    public String getPatterned() {
        return patterned;
    }

    public void setPatterned(String patterned) {
        this.patterned = patterned;
    }

    public Date getPast() {
        return past;
    }

    public void setPast(Date past) {
        this.past = past;
    }

    public Date getFuture() {
        return future;
    }

    public void setFuture(Date future) {
        this.future = future;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public double getDecimalMin() {
        return decimalMin;
    }

    public void setDecimalMin(double decimalMin) {
        this.decimalMin = decimalMin;
    }

    public double getDecimalMax() {
        return decimalMax;
    }

    public void setDecimalMax(double decimalMax) {
        this.decimalMax = decimalMax;
    }

    public String getSized() {
        return sized;
    }

    public void setSized(String sized) {
        this.sized = sized;
    }

    public boolean isEqTrue() {
        return eqTrue;
    }

    public void setEqTrue(boolean eqTrue) {
        this.eqTrue = eqTrue;
    }

    public boolean isEqFalse() {
        return eqFalse;
    }

    public void setEqFalse(boolean eqFalse) {
        this.eqFalse = eqFalse;
    }

    public Object getIsNull() {
        return isNull;
    }

    public void setIsNull(String isNull) {
        this.isNull = isNull;
    }

    public Object getNotNull() {
        return notNull;
    }

    public void setNotNull(String notNull) {
        this.notNull = notNull;
    }

    public String getNotEmpty() {
        return notEmpty;
    }

    public void setNotEmpty(String notEmpty) {
        this.notEmpty = notEmpty;
    }

    public String getNotBlank() {
        return notBlank;
    }

    public void setNotBlank(String notBlank) {
        this.notBlank = notBlank;
    }
}
