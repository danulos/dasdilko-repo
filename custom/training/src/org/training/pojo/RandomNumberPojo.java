package org.training.pojo;

import org.training.constraint.random.RandomNumber;

public class RandomNumberPojo {

    private String code;
    @RandomNumber(bound = 10)
    private int number;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
