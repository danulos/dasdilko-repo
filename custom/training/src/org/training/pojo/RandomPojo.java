package org.training.pojo;

import org.training.constraint.random.RandomResult;

@RandomResult
public class RandomPojo {

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
