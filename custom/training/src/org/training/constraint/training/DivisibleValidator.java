package org.training.constraint.training;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DivisibleValidator implements ConstraintValidator<Divisible, Integer> {

    private int number;

    @Override
    public void initialize(final Divisible constraintAnnotation) {
        this.number = constraintAnnotation.number();
    }

    @Override
    public boolean isValid(final Integer value,
                           final ConstraintValidatorContext context) {
        return value >= 0
                && (value % number) == 0;
    }
}