package org.training.constraint.training;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = DivisibleValidator.class)
public @interface Divisible {
    String message() default "{org.training.constraint.training.Divisible.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    int number();
}
