package org.training.constraint.random;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Random;

public class RandomResultValidator implements ConstraintValidator<RandomResult, Object> {
    @Override
    public void initialize(final RandomResult constraintAnnotation) {
    }

    @Override
    public boolean isValid(final Object value,
                           final ConstraintValidatorContext context) {
        return new Random().nextBoolean();
    }
}