package org.training.constraint.random;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = RandomResultValidator.class)
public @interface RandomResult {
    String message() default "{org.training.constraint.random.RandomResult.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
