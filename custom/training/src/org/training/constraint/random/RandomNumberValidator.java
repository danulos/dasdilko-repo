package org.training.constraint.random;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Random;

public class RandomNumberValidator implements ConstraintValidator<RandomNumber, Integer> {

    private int bound;

    @Override
    public void initialize(final RandomNumber constraintAnnotation) {
        this.bound = constraintAnnotation.bound();
    }

    @Override
    public boolean isValid(final Integer value,
                           final ConstraintValidatorContext context) {
        return value >= 0
                && value <= new Random().nextInt(bound);
    }
}