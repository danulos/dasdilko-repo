package org.training.jmx.impl;

import de.hybris.platform.jmx.mbeans.impl.AbstractJMXMBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.training.jmx.TrainingMBean;

import java.util.Random;

@ManagedResource(description = "MBean added as example")
public class DefaultTrainingMBean extends AbstractJMXMBean implements TrainingMBean {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTrainingMBean.class);

    @Override
    @ManagedOperation(description = "Empty Method")
    public void doSome() {
        LOG.info("doSome");
    }

    @Override
    @ManagedAttribute(description = "Returns line with random number")
    public String getLine(){
        return "" + new Random().nextInt();
    }

    @Override
    @ManagedAttribute(description = "Returns random integer")
    public Integer getInteger(){
        return new Random().nextInt();
    }
}
