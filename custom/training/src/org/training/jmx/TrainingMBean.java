package org.training.jmx;

public interface TrainingMBean {

    void doSome();

    String getLine();

    Integer getInteger();
}
