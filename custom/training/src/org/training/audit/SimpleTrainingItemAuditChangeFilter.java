package org.training.audit;

import de.hybris.platform.persistence.audit.AuditChangeFilter;
import de.hybris.platform.persistence.audit.AuditableChange;
import org.training.model.SimpleTrainingItemModel;

public class SimpleTrainingItemAuditChangeFilter implements AuditChangeFilter {

    @Override
    public boolean ignoreAudit(final AuditableChange change) {
        if (change.getBefore() == null || change.getAfter() == null) {
            return false;
        }
        if (!SimpleTrainingItemModel._TYPECODE.equals(change.getBefore().getTypeCode())) {
            return false;
        }
        final int valueBefore = (int) change.getBefore()
                .getAttributeValue("randomnumber", null).getValue();
        final int valueAfter = (int) change.getAfter()
                .getAttributeValue("randomnumber", null).getValue();
        return (valueBefore * 2) <= valueAfter;
    }
}
