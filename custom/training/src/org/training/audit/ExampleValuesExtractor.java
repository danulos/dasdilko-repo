package org.training.audit;

import de.hybris.platform.audit.provider.internal.resolver.AuditRecordInternalProvider;
import de.hybris.platform.audit.provider.internal.resolver.VirtualReferenceValuesExtractor;
import de.hybris.platform.audit.provider.internal.resolver.impl.AuditTypeContext;
import de.hybris.platform.core.PK;
import de.hybris.platform.persistence.audit.internal.AuditRecordInternal;
import de.hybris.platform.servicelayer.model.ModelService;
import org.training.model.SimpleTrainingItemModel;
import org.training.model.WrappedOneModel;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ExampleValuesExtractor implements VirtualReferenceValuesExtractor {

    @Resource
    private ModelService modelService;

    @Override
    public <AUDITRECORD extends AuditRecordInternal> List<AUDITRECORD> extractValues(
            final AuditRecordInternalProvider<AUDITRECORD> provider, final AuditTypeContext<AUDITRECORD> ctx) {
        final Set<Object> result = new HashSet<>();
        for (final AUDITRECORD trainingItem : ctx.getPayloadsForBasePKs()) {
            final String code = trainingItem.getAttribute(SimpleTrainingItemModel.CODE.toLowerCase()).toString();
            final String randomNumber = trainingItem.getAttribute(SimpleTrainingItemModel.RANDOMNUMBER.toLowerCase()).toString();

            final String wrappedOnePKStr = trainingItem.getAttribute(SimpleTrainingItemModel.WRAPPEDONE.toLowerCase()).toString();
            final WrappedOneModel wrappedOne = modelService.get(PK.parse(wrappedOnePKStr));

            result.add(code + "_" + randomNumber + "_" + wrappedOne.getCode());

        }
        return provider.queryRecords(result);
    }
}