package org.training.model.handler.simpletrainingitem;

import de.hybris.platform.servicelayer.model.attribute.DynamicLocalizedAttributeHandler;
import org.training.model.SimpleTrainingItemModel;

import java.util.Locale;
import java.util.UUID;

public class LocalizedDynamicPropertyAttributeHandler
        implements DynamicLocalizedAttributeHandler<String, SimpleTrainingItemModel> {
    @Override
    public String get(SimpleTrainingItemModel trainingItem) {
        return trainingItem.getCode() + UUID.randomUUID();
    }

    @Override
    public String get(SimpleTrainingItemModel trainingItem, Locale loc) {
        return trainingItem.getCode() + "_" + loc.getDisplayName() + "_" + UUID.randomUUID();
    }

    @Override
    public void set(SimpleTrainingItemModel trainingItem, String value) {
        throw new UnsupportedOperationException("The attribute is readonly");
    }

    @Override
    public void set(SimpleTrainingItemModel trainingItem, String value, Locale loc) {
        throw new UnsupportedOperationException("The attribute is readonly");
    }
}


