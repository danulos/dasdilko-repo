package org.training.model.handler.simpletrainingitem;

import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import org.training.model.SimpleTrainingItemModel;

import java.util.UUID;

public class DynamicPropertyAttributeHandler
        implements DynamicAttributeHandler<String, SimpleTrainingItemModel> {

    @Override
    public String get(SimpleTrainingItemModel trainingItem) {
        return trainingItem.getCode() + UUID.randomUUID();
    }

    @Override
    public void set(SimpleTrainingItemModel trainingItem, String value) {
        throw new UnsupportedOperationException("The attribute is readonly");
    }

}