package org.training.model.interceptor.three;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import org.training.model.WrappedThreeModel;
import org.training.util.TrainingUtil;
import org.training.util.StaticConfigWrapper;

import static org.training.constants.TrainingConstants.INTERCEPTOR_MODEL_LAYER_CONFIG;

public class WrappedThreeRemoveInterceptor implements RemoveInterceptor<WrappedThreeModel> {

    @Override
    public void onRemove(final WrappedThreeModel simpleTrainingItem, final InterceptorContext ctx) {
        TrainingUtil.printMessage("WrappedThreeRemoveInterceptor");

        if (simpleTrainingItem.getWrappedFour() != null) {
            if (Boolean.valueOf(StaticConfigWrapper.getConfig(INTERCEPTOR_MODEL_LAYER_CONFIG))) {
                ctx.getModelService().remove(simpleTrainingItem.getWrappedFour());
            } else {
                ctx.registerElementFor(simpleTrainingItem.getWrappedFour(), PersistenceOperation.DELETE);
            }
        }
    }
}
