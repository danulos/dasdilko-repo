package org.training.model.interceptor.three;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.training.model.WrappedThreeModel;
import org.training.util.TrainingUtil;

public class WrappedThreeValidateInterceptor implements ValidateInterceptor<WrappedThreeModel> {

    @Override
    public void onValidate(final WrappedThreeModel simpleTrainingItem, final InterceptorContext ctx) {
        TrainingUtil.printMessage("WrappedThreeValidateInterceptor");
    }
}
