package org.training.model.interceptor.three;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.training.model.WrappedThreeModel;
import org.training.util.TrainingUtil;
import org.training.util.StaticConfigWrapper;

import java.util.Date;

import static org.training.constants.TrainingConstants.INTERCEPTOR_MODEL_LAYER_CONFIG;

public class WrappedThreePrepareInterceptor implements PrepareInterceptor<WrappedThreeModel> {

    @Override
    public void onPrepare(final WrappedThreeModel simpleTrainingItem, final InterceptorContext ctx) {
        TrainingUtil.printMessage("WrappedThreePrepareInterceptor");

        if (simpleTrainingItem.getWrappedFour() != null) {
            simpleTrainingItem.getWrappedFour().setDate(new Date());
            if (Boolean.valueOf(StaticConfigWrapper.getConfig(INTERCEPTOR_MODEL_LAYER_CONFIG))) {
                ctx.getModelService().save(simpleTrainingItem.getWrappedFour());
            } else {
                ctx.registerElementFor(simpleTrainingItem.getWrappedFour(), PersistenceOperation.SAVE);
            }
        }

    }
}
