package org.training.model.interceptor.one;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import org.training.model.WrappedOneModel;
import org.training.util.TrainingUtil;
import org.training.util.StaticConfigWrapper;

import static org.training.constants.TrainingConstants.INTERCEPTOR_MODEL_LAYER_CONFIG;

public class WrappedOneRemoveInterceptor implements RemoveInterceptor<WrappedOneModel> {

    @Override
    public void onRemove(final WrappedOneModel simpleTrainingItem, final InterceptorContext ctx) {
        TrainingUtil.printMessage("WrappedOneRemoveInterceptor");

        if (simpleTrainingItem.getWrappedTwo() != null) {
            if (Boolean.valueOf(StaticConfigWrapper.getConfig(INTERCEPTOR_MODEL_LAYER_CONFIG))) {
                ctx.getModelService().remove(simpleTrainingItem.getWrappedTwo());
            } else {
                ctx.registerElementFor(simpleTrainingItem.getWrappedTwo(), PersistenceOperation.DELETE);
            }
        }
    }
}
