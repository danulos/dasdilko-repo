package org.training.model.interceptor.one;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.training.model.WrappedOneModel;
import org.training.util.TrainingUtil;
import org.training.util.StaticConfigWrapper;

import java.util.Date;

import static org.training.constants.TrainingConstants.INTERCEPTOR_MODEL_LAYER_CONFIG;

public class WrappedOnePrepareInterceptor implements PrepareInterceptor<WrappedOneModel> {

    @Override
    public void onPrepare(final WrappedOneModel simpleTrainingItem, final InterceptorContext ctx) {
        TrainingUtil.printMessage("WrappedOnePrepareInterceptor");

        if (simpleTrainingItem.getWrappedTwo() != null) {
            simpleTrainingItem.getWrappedTwo().setDate(new Date());
            if (Boolean.valueOf(StaticConfigWrapper.getConfig(INTERCEPTOR_MODEL_LAYER_CONFIG))) {
                ctx.getModelService().save(simpleTrainingItem.getWrappedTwo());
            } else {
                ctx.registerElementFor(simpleTrainingItem.getWrappedTwo(), PersistenceOperation.SAVE);
            }
        }

    }
}
