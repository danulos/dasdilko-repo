package org.training.model.interceptor.one;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.training.model.WrappedOneModel;
import org.training.util.TrainingUtil;

public class WrappedOneValidateInterceptor implements ValidateInterceptor<WrappedOneModel> {

    @Override
    public void onValidate(final WrappedOneModel simpleTrainingItem, final InterceptorContext ctx) {
        TrainingUtil.printMessage("WrappedOneValidateInterceptor");
    }
}
