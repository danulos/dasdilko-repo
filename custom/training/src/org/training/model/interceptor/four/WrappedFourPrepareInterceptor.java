package org.training.model.interceptor.four;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.training.model.*;
import org.training.util.TrainingUtil;

public class WrappedFourPrepareInterceptor implements PrepareInterceptor<WrappedFourModel> {

    @Override
    public void onPrepare(final WrappedFourModel simpleTrainingItem, final InterceptorContext ctx) {
        TrainingUtil.printMessage("WrappedFourPrepareInterceptor");
    }
}
