package org.training.model.interceptor.four;

import de.hybris.platform.servicelayer.interceptor.InitDefaultsInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import org.training.model.WrappedFourModel;
import org.training.util.TrainingUtil;

public class WrappedFourInitDefaultsInterceptor implements InitDefaultsInterceptor<WrappedFourModel> {

    @Override
    public void onInitDefaults(final WrappedFourModel simpleTrainingItem, final InterceptorContext ctx) {
        TrainingUtil.printMessage("WrappedFourInitDefaultsInterceptor");
    }
}
