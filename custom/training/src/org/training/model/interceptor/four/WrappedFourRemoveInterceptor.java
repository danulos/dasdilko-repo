package org.training.model.interceptor.four;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import org.training.model.*;
import org.training.util.TrainingUtil;

public class WrappedFourRemoveInterceptor implements RemoveInterceptor<WrappedFourModel> {

    @Override
    public void onRemove(final WrappedFourModel simpleTrainingItem, final InterceptorContext ctx) {
        TrainingUtil.printMessage("WrappedFourRemoveInterceptor");
    }
}
