package org.training.model.interceptor.four;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.training.model.WrappedFourModel;
import org.training.util.TrainingUtil;

import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class WrappedFourValidateInterceptor implements ValidateInterceptor<WrappedFourModel> {

    @Override
    public void onValidate(final WrappedFourModel item, final InterceptorContext ctx) {
        TrainingUtil.printMessage("WrappedFourValidateInterceptor");

        Map<String, Set<Locale>> dirtyAttributes = ctx.getDirtyAttributes(item);

        for (Map.Entry<String, Set<Locale>> entry : dirtyAttributes.entrySet()) {
            System.out.println("key: " + entry.getKey());
            if (entry.getValue() == null) {
                System.out.println("value: " + ctx.getModelService().getAttributeValue(item, entry.getKey()));
            } else {
                for (Locale loc : entry.getValue()) {
                    System.out.println("value: " +
                            ctx.getModelService().getAttributeValue(item, entry.getKey(), loc)   );
                }
            }
        }
    }
}
