package org.training.model.interceptor.simpletrainingitem;

import de.hybris.platform.servicelayer.interceptor.InitDefaultsInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import org.training.model.SimpleTrainingItemModel;

public class SimpleTrainingItemInitDefaultsInterceptor implements InitDefaultsInterceptor<SimpleTrainingItemModel> {

    @Override
    public void onInitDefaults(final SimpleTrainingItemModel simpleTrainingItem, final InterceptorContext ctx) {
//        PrintUtil.print("SimpleTrainingItemInitDefaultsInterceptor");
    }
}
