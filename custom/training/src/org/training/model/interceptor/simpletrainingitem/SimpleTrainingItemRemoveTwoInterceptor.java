package org.training.model.interceptor.simpletrainingitem;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import org.training.model.SimpleTrainingItemModel;
import org.training.util.TrainingUtil;

public class SimpleTrainingItemRemoveTwoInterceptor implements RemoveInterceptor<SimpleTrainingItemModel> {

    @Override
    public void onRemove(final SimpleTrainingItemModel simpleTrainingItem, final InterceptorContext ctx) {
        TrainingUtil.printMessage("SimpleTrainingItemRemoveTwoInterceptor");
    }
}
