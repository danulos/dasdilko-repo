package org.training.model.interceptor.simpletrainingitem;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import org.training.model.SimpleTrainingItemModel;
import org.training.util.TrainingUtil;
import org.training.util.StaticConfigWrapper;

import static org.training.constants.TrainingConstants.INTERCEPTOR_MODEL_LAYER_CONFIG;

public class SimpleTrainingItemRemoveInterceptor implements RemoveInterceptor<SimpleTrainingItemModel> {

    @Override
    public void onRemove(final SimpleTrainingItemModel simpleTrainingItem, final InterceptorContext context) {
        TrainingUtil.printMessage("SimpleTrainingItemRemoveInterceptor");

//        TrainingUtil.printLine();
//        System.out.println("isRemoved: " + context.isRemoved(simpleTrainingItem));

        SimpleTrainingItemModel exampleItem = new SimpleTrainingItemModel();
        boolean isRemoved = context.isRemoved(exampleItem);

        if (isRemoved) {

        }

        if (simpleTrainingItem.getWrappedOne() != null) {
            if (Boolean.parseBoolean(StaticConfigWrapper.getConfig(INTERCEPTOR_MODEL_LAYER_CONFIG))) {
                context.getModelService().remove(simpleTrainingItem.getWrappedOne());
            } else {
                context.registerElementFor(simpleTrainingItem.getWrappedOne(), PersistenceOperation.DELETE);
            }
        }

    }
}
