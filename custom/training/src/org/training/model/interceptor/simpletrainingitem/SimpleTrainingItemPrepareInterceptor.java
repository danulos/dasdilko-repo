package org.training.model.interceptor.simpletrainingitem;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.training.model.SimpleTrainingItemModel;
import org.training.util.TrainingUtil;
import org.training.util.StaticConfigWrapper;

import java.util.Date;
import java.util.Random;

import static org.training.constants.TrainingConstants.INTERCEPTOR_MODEL_LAYER_CONFIG;

public class SimpleTrainingItemPrepareInterceptor implements PrepareInterceptor<SimpleTrainingItemModel> {

    private final int bound = 200;

    @Override
    public void onPrepare(final SimpleTrainingItemModel simpleTrainingItem, final InterceptorContext context) {
//        TrainingUtil.printMessage("SimpleTrainingItemPrepareInterceptor");

        context.setAttribute("p1", "p1");
        context.getTransientStorage().put("p11", "p11");

        if (simpleTrainingItem.getWrappedOne() != null) {
            simpleTrainingItem.getWrappedOne().setDate(new Date());

            if (Boolean.valueOf(StaticConfigWrapper.getConfig(INTERCEPTOR_MODEL_LAYER_CONFIG))) {
                context.getModelService().save(simpleTrainingItem.getWrappedOne());
            } else {
                context.registerElementFor(simpleTrainingItem.getWrappedOne(), PersistenceOperation.SAVE);
            }
        }

        if (simpleTrainingItem.getRandomNumber() == 0) {
            simpleTrainingItem.setRandomNumber(new Random().nextInt(bound) - 100);
        }
    }
}
