package org.training.model.interceptor.simpletrainingitem;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.training.model.SimpleTrainingItemModel;
import org.training.util.TrainingUtil;

public class SimpleTrainingItemValidateTwoInterceptor implements ValidateInterceptor<SimpleTrainingItemModel> {

    @Override
    public void onValidate(final SimpleTrainingItemModel simpleTrainingItem, final InterceptorContext context) {
//        TrainingUtil.printMessage("SimpleTrainingItemValidateTwoInterceptor");

//        printInfo(context);
    }

    private void printInfo(InterceptorContext context) {
        System.out.println("v2 -> p1 " + context.getAttribute("p1"));
        System.out.println("v2 -> p11 " + context.getTransientStorage().get("p11"));
        System.out.println("v2 -> p2 " + context.getAttribute("p2"));
        System.out.println("v2 -> p21 " + context.getTransientStorage().get("p21"));
        System.out.println("v2 -> v1 " + context.getAttribute("v1"));
        System.out.println("v2 -> v11 " + context.getTransientStorage().get("v11"));
    }
}
