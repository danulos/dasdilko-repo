package org.training.model.interceptor.simpletrainingitem;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.LoadInterceptor;
import org.training.model.SimpleTrainingItemModel;

public class SimpleTrainingItemLoadInterceptor implements LoadInterceptor<SimpleTrainingItemModel> {

    @Override
    public void onLoad(final SimpleTrainingItemModel simpleTrainingItem, final InterceptorContext ctx) {
//        PrintUtil.print("SimpleTrainingItemLoadInterceptor");
    }
}
