package org.training.model.interceptor.simpletrainingitem;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.training.model.SimpleTrainingItemModel;
import org.training.util.TrainingUtil;

public class SimpleTrainingItemPrepareThreeInterceptor implements PrepareInterceptor<SimpleTrainingItemModel> {

    @Override
    public void onPrepare(final SimpleTrainingItemModel simpleTrainingItem, final InterceptorContext ctx) {
        TrainingUtil.printMessage("SimpleTrainingItemPrepareThreeInterceptor");

        //todo: зрозуміти інтерсептори краще
    }
}
