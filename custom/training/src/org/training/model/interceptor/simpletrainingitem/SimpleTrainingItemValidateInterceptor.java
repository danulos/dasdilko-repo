package org.training.model.interceptor.simpletrainingitem;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.training.model.SimpleTrainingItemModel;
import org.training.util.TrainingUtil;

import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class SimpleTrainingItemValidateInterceptor implements ValidateInterceptor<SimpleTrainingItemModel> {

    @Override
    public void onValidate(final SimpleTrainingItemModel simpleTrainingItem, final InterceptorContext context) {
//        TrainingUtil.printMessage("SimpleTrainingItemValidateInterceptor");

//        TrainingUtil.printLine();

//        printInfo(simpleTrainingItem, context);

    }

    private void printInfo(final SimpleTrainingItemModel simpleTrainingItem, final InterceptorContext context) {
        SimpleTrainingItemModel exampleItem = simpleTrainingItem;

        boolean isNew = context.isNew(exampleItem);

        boolean exists = context.exists(exampleItem);

        boolean isModified = context.isModified(exampleItem);

        boolean isModifiedAttribute = context.isModified(exampleItem, "code");

        Map<String, Set<Locale>> dirtyAttributes = context.getDirtyAttributes(exampleItem);

        boolean isRemoved = context.isRemoved(exampleItem);

        if (isNew) {

        }

        if (exists) {

        }

        if (isModified) {

        }

        if (isModifiedAttribute) {

        }

        if (isRemoved) {

        }

        for (Map.Entry<String, Set<Locale>> entry : dirtyAttributes.entrySet()) {
            System.out.println("key: " + entry.getKey());
            if (entry.getValue() == null) {
                System.out.println("value: " + context.getModelService().getAttributeValue(exampleItem, entry.getKey()));
            } else {
                for (Locale loc : entry.getValue()) {
                    System.out.println("value: " +
                            context.getModelService().getAttributeValue(exampleItem, entry.getKey(), loc)   );
                }
            }
        }

        System.out.println("isNew: " + isNew);
        System.out.println("exists: " + exists);
        System.out.println("isModified: " + isModified);
        System.out.println("isModifiedAttribute: " + isModifiedAttribute);
        System.out.println("isRemoved: " + isRemoved);



        System.out.println("v1 -> p1" + context.getAttribute("p1"));
        System.out.println("v1 -> p11" + context.getTransientStorage().get("p11"));
        System.out.println("v1 -> p2" + context.getAttribute("p2"));
        System.out.println("v1 -> p21" + context.getTransientStorage().get("p21"));

        context.setAttribute("v1", "v1");
        context.getTransientStorage().put("v11", "v11");


        context.setAttribute("locale", Locale.US);

        Locale locale = (Locale)context.getAttribute("locale");

        context.getTransientStorage().put(Locale.US, exampleItem);

        SimpleTrainingItemModel item = (SimpleTrainingItemModel)context.getTransientStorage().get(Locale.US);

        System.out.println(locale.getCountry());
        System.out.println(item.getCode());
    }
}
