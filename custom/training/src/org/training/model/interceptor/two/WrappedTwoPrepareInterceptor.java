package org.training.model.interceptor.two;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.training.model.WrappedTwoModel;
import org.training.util.TrainingUtil;
import org.training.util.StaticConfigWrapper;

import java.util.Date;

import static org.training.constants.TrainingConstants.INTERCEPTOR_MODEL_LAYER_CONFIG;

public class WrappedTwoPrepareInterceptor implements PrepareInterceptor<WrappedTwoModel> {

    @Override
    public void onPrepare(final WrappedTwoModel simpleTrainingItem, final InterceptorContext ctx) {
        TrainingUtil.printMessage("WrappedTwoPrepareInterceptor");


        if (simpleTrainingItem.getWrappedThree() != null) {
            simpleTrainingItem.getWrappedThree().setDate(new Date());
            if (Boolean.valueOf(StaticConfigWrapper.getConfig(INTERCEPTOR_MODEL_LAYER_CONFIG))) {
                ctx.getModelService().save(simpleTrainingItem.getWrappedThree());
            } else {
                ctx.registerElementFor(simpleTrainingItem.getWrappedThree(), PersistenceOperation.SAVE);
            }
        }

    }
}
