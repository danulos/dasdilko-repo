package org.training.model.interceptor.two;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.training.model.WrappedTwoModel;
import org.training.util.TrainingUtil;

public class WrappedTwoValidateInterceptor implements ValidateInterceptor<WrappedTwoModel> {

    @Override
    public void onValidate(final WrappedTwoModel simpleTrainingItem, final InterceptorContext ctx) {
        TrainingUtil.printMessage("WrappedTwoValidateInterceptor");
    }
}
