package org.training.model.interceptor.two;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import org.training.model.WrappedTwoModel;
import org.training.util.TrainingUtil;
import org.training.util.StaticConfigWrapper;

import static org.training.constants.TrainingConstants.INTERCEPTOR_MODEL_LAYER_CONFIG;

public class WrappedTwoRemoveInterceptor implements RemoveInterceptor<WrappedTwoModel> {

    @Override
    public void onRemove(final WrappedTwoModel simpleTrainingItem, final InterceptorContext ctx) {
        TrainingUtil.printMessage("WrappedTwoRemoveInterceptor");

        if (simpleTrainingItem.getWrappedThree() != null) {
            if (Boolean.valueOf(StaticConfigWrapper.getConfig(INTERCEPTOR_MODEL_LAYER_CONFIG))) {
                ctx.getModelService().remove(simpleTrainingItem.getWrappedThree());
            } else {
                ctx.registerElementFor(simpleTrainingItem.getWrappedThree(), PersistenceOperation.DELETE);
            }
        }
    }
}
