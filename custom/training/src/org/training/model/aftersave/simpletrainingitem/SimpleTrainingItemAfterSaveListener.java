package org.training.model.aftersave.simpletrainingitem;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.AfterSaveEvent;
import de.hybris.platform.tx.AfterSaveListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.constants.TrainingConstants;
import org.training.model.SimpleTrainingItemModel;

import javax.annotation.Resource;
import java.util.Collection;

public class SimpleTrainingItemAfterSaveListener implements AfterSaveListener {

    private static final Logger LOG = LoggerFactory.getLogger(SimpleTrainingItemAfterSaveListener.class);

    @Resource
    private ModelService modelService;

    @Override
    public void afterSave(final Collection<AfterSaveEvent> events) {
        for (final AfterSaveEvent event : events) {
            if (TrainingConstants.SIMPLE_TRAINING_ITEM_TYPE_CODE != event.getPk().getTypeCode()) {
                continue;
            }

            if (event.getType() == AfterSaveEvent.CREATE) {
                final SimpleTrainingItemModel item = modelService.get(event.getPk());
//                LOG.info("SimpleTrainingItem {} was created", item.getCode());
            }
        }
    }
}