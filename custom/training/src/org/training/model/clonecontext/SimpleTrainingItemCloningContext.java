package org.training.model.clonecontext;

import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.internal.model.ModelCloningContext;
import de.hybris.platform.servicelayer.type.TypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

public class SimpleTrainingItemCloningContext implements ModelCloningContext {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleTrainingItemCloningContext.class);

    @Resource
    private TypeService typeService;

    public boolean skipAttribute(final Object original, final String qualifier) {
        try {
            ComposedTypeModel composedType =
                    getComposedType(original);
            AttributeDescriptorModel attributeDescriptor =
                    getAttributeDescriptor(qualifier, composedType);
            return shouldBeSkipped(attributeDescriptor);
        } catch (Exception exception) {
            LOGGER.warn("Faced an issue during cloning {} attribute - {}",
                    qualifier, exception.getMessage());
        }
        return true;
    }

    private boolean shouldBeSkipped(
            final AttributeDescriptorModel attributeDescriptor) {
        return attributeDescriptor != null &&
                (!attributeDescriptor.getReadable()
                        || !attributeDescriptor.getWritable());
    }

    private ComposedTypeModel getComposedType(final Object original) {
        return this.typeService.getComposedTypeForClass(original.getClass());
    }

    private AttributeDescriptorModel getAttributeDescriptor(final String qualifier, final ComposedTypeModel composedType) {
        return this.typeService.getAttributeDescriptor(composedType, qualifier);
    }

    public boolean treatAsPartOf(Object original, String qualifier) {
        return false;
    }

    public boolean usePresetValue(Object original, String qualifier) {
        return false;
    }

    public Object getPresetValue(Object original, String qualifier) {
        return null;
    }
}
