package org.training.model.dao;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import org.training.model.SimpleTrainingItemModel;

public class DefaultSimpleTrainingItemGenericDao
        extends DefaultGenericDao<SimpleTrainingItemModel> {

    public DefaultSimpleTrainingItemGenericDao() {
        super(SimpleTrainingItemModel._TYPECODE);
    }
}
