/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.constants;

/**
 * Global class for all Training constants. You can add global constants for your extension into this class.
 */
public final class TrainingConstants extends GeneratedTrainingConstants
{
	public static final String EXTENSIONNAME = "training";

	private TrainingConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "trainingPlatformLogo";

	public static final String GIT_INFO_PROPERTIES = "git_info.properties";

	public static final String INTERCEPTOR_MODEL_LAYER_CONFIG = "interceptor.model";

	public static final int SIMPLE_TRAINING_ITEM_TYPE_CODE = 11004;

	public static final String INIT_OPTION_MULTI = "init.option.multi";
	public static final String INIT_OPTION_BOOLEAN = "init.option.boolean";
}
