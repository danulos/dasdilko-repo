package org.training.properties.profile;

public class ProfileName implements Profiled{

    @Override
    public void doAction() {
        System.out.println("====================");
        System.out.println("OneProfiled.doAction");
        System.out.println("====================");
    }
}
