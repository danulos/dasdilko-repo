package org.training.properties.profile;

public class NoProfile implements Profiled{

    @Override
    public void doAction() {
        System.out.println("====================");
        System.out.println("NoProfile.doAction");
        System.out.println("====================");
    }
}
