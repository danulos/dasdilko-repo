package org.training.properties;

import de.hybris.platform.core.MasterTenant;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.core.TenantListener;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.config.impl.HybrisConfiguration;
import de.hybris.platform.util.config.ConfigIntf;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.event.ConfigurationListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamSource;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import static org.training.constants.TrainingConstants.GIT_INFO_PROPERTIES;

public class TrainingPropertiesListener implements TenantListener {

    public TrainingPropertiesListener() {
        Registry.registerTenantListener(this);
    }

    @Override
    public void afterTenantStartUp(Tenant tenant) {
        if (tenant instanceof MasterTenant) {
            onApplicationLoad();
            onApplicationLoad2();
        }
    }

    private static final Logger LOG = LoggerFactory.getLogger(TrainingPropertiesListener.class);

    @Resource
    private ConfigurationService configurationService;

    private void onApplicationLoad() {
        try {
            final Properties buildProperties = loadProperties();
            final Configuration configuration = configurationService.getConfiguration();

            Set<Map.Entry<Object, Object>> entries = buildProperties.entrySet();
            entries.forEach(entry -> configuration
                    .addProperty(entry.getKey().toString(), entry.getValue()));
        } catch (JaloSystemException ex) {
            if (LOG.isDebugEnabled()) {
                LOG.debug(ex.toString());
            }
        } catch (IOException e) {
            LOG.warn("properties file is not found.");
            if (LOG.isDebugEnabled()) {
                LOG.debug("Exception:", e);
            }
        }
    }

    private Properties loadProperties() throws IOException {
        final Properties buildProperties = new Properties();
        final InputStreamSource resource = Registry
                .getApplicationContext().getResource(GIT_INFO_PROPERTIES);
        buildProperties.load(resource.getInputStream());
        return buildProperties;
    }

    private void onApplicationLoad2() {
        final Configuration cfg = configurationService.getConfiguration();
        final ConfigurationListener configurationListener = event -> {
            boolean isBefore = event.isBeforeUpdate();
            Object value = event.getPropertyValue();
            String key = event.getPropertyName();
            LOG.info("(ConfigurationListener) Property changed: key -> {}, value -> {}; " +
                    "beforeUpdate -> {}", key, value, isBefore);
        };
        ((HybrisConfiguration) cfg).addConfigurationListener(configurationListener);

        final ConfigIntf.ConfigChangeListener configChangeListener = (key, value) -> {
            LOG.info("(ConfigChangeListener) Property changed: key -> {}, value -> {};",
                    key, value);
        };
        final ConfigIntf config = Registry.getMasterTenant().getConfig();
        config.registerConfigChangeListener(configChangeListener);
    }

    @Override
    public void beforeTenantShutDown(final Tenant tenant) {
    }

    @Override
    public void afterSetActivateSession(final Tenant tenant) {
    }

    @Override
    public void beforeUnsetActivateSession(final Tenant tenant) {
    }

}
