package org.training.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.training.constants.TrainingConstants;
import org.training.service.TrainingService;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.training.constants.TrainingConstants.PLATFORM_LOGO_CODE;


@SystemSetup(extension = TrainingConstants.EXTENSIONNAME)
public class TrainingSystemSetup extends AbstractSystemSetup {

    protected static final Logger LOG = LoggerFactory.getLogger(TrainingSystemSetup.class);

    @SystemSetup(process = SystemSetup.Process.ALL, type = SystemSetup.Type.ALL)
    public void importTrainingData(final SystemSetupContext context) {
        importImpexFile(context, "/impex/default/option_default.impex");

        final boolean initOptionBoolean = getBooleanSystemSetupParameter
                (context, TrainingConstants.INIT_OPTION_BOOLEAN);
        if (initOptionBoolean) {
            LOG.info("option_boolean");
            importImpexFile(context, "/impex/boolean/option_boolean.impex");
        }

        final String[] initOptionMulti = context.getParameters
                (context.getExtensionName() + "_" + TrainingConstants.INIT_OPTION_MULTI);
        if (initOptionMulti != null) {
            for (String multiOption : initOptionMulti) {
                LOG.info(multiOption);
                importImpexFile(context, "/impex/multi/" + multiOption);
            }
        }
    }

    @Override
    @SystemSetupParameterMethod
    public List<SystemSetupParameter> getInitializationOptions() {
        final List<SystemSetupParameter> params = new ArrayList<>();
        params.add(createMultiContentParameter(TrainingConstants.INIT_OPTION_MULTI, "Training multi option"));
        params.add(createBooleanSystemSetupParameter(TrainingConstants.INIT_OPTION_BOOLEAN, "Training boolean option", false));
        return params;
    }

    private SystemSetupParameter createMultiContentParameter(final String parameterKey, final String label) {
        final SystemSetupParameter parameter = new SystemSetupParameter(parameterKey);
        parameter.setMultiSelect(true);
        parameter.setLabel(label);
        for (String option : getMultiOptions()) {
            parameter.addValue(option, false);
        }
        return parameter;
    }

    private List<String> getMultiOptions() {
        try {
            final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(getClass().getClassLoader());
            final Resource[] resources = resolver.getResources("impex/multi/*.impex");
            return Stream.of(resources).map(Resource::getFilename).collect(Collectors.toList());
        } catch (final IOException e) {
            return Collections.emptyList();
        }
    }

    private final TrainingService trainingService;

    public TrainingSystemSetup(final TrainingService trainingService) {
        this.trainingService = trainingService;
    }

    @SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
    public void createEssentialData() {
        trainingService.createLogo(PLATFORM_LOGO_CODE);
    }

    private InputStream getImageStream() {
        return TrainingSystemSetup.class.getResourceAsStream("/training/sap-hybris-platform.png");
    }
}
