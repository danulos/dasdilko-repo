package org.training.script;

public interface ScriptResult {

    String getProperty();

    void doAction(String input);

}
