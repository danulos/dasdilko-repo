package org.training.impex.decorator.simpletrainingitem;

import de.hybris.platform.util.CSVCellDecorator;
import org.training.util.TrainingUtil;

import java.util.Map;

public class CodeDecorator implements CSVCellDecorator {

    @Override
    public String decorate(final int position, final Map<Integer, String> srcLine) {
        final String cellValue = srcLine.get(position);
        return cellValue + TrainingUtil.getCode();
    }
}
