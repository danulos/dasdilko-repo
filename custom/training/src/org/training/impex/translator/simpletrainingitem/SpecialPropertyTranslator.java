package org.training.impex.translator.simpletrainingitem;

import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SpecialPropertyTranslator extends AbstractSpecialValueTranslator {

    private static final Logger LOG = LoggerFactory.getLogger(SpecialPropertyTranslator.class);

    private SpecialColumnDescriptor columnDescriptor;

    public SpecialPropertyTranslator() {
    }

    public void init(SpecialColumnDescriptor columnDescriptor) {
        this.columnDescriptor = columnDescriptor;
    }

    @Override
    public void performImport(String cellValue, Item processedItem) {
        final String qualifier = columnDescriptor.getQualifier().replaceFirst("@", "");
        final String value = StringUtils.reverse(cellValue);
        try {
            processedItem.setAttribute(qualifier, value);
        } catch (JaloBusinessException e) {
            LOG.warn("Couldn't process {} value", cellValue);
        }
    }
}
