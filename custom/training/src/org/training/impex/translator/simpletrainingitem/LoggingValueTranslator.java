package org.training.impex.translator.simpletrainingitem;

import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingValueTranslator extends AbstractValueTranslator {

    @Override
    public Object importValue(final String valueExpr, final Item toItem) throws JaloInvalidParameterException {
        LOG.info("Value in translator - {}", valueExpr);
        return valueExpr;
    }

    private static final Logger LOG = LoggerFactory.getLogger(LoggingValueTranslator.class);

    @Override
    public String exportValue(final Object value) throws JaloInvalidParameterException {
        return value == null ? "" : value.toString();
    }
}
