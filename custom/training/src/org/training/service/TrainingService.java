/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.service;

import org.training.model.SimpleTrainingItemModel;

public interface TrainingService
{
	String getHybrisLogoUrl(String logoCode);

	void createLogo(String logoCode);

	void doSomeActionWithParam(String param);

	void test();

	void printTrainingItem(SimpleTrainingItemModel trainingItem);
}
