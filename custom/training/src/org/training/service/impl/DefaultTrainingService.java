/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.service.impl;

import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.training.model.SimpleTrainingItemModel;
import org.training.service.TrainingService;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.*;

public class DefaultTrainingService implements TrainingService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultTrainingService.class);

    @Resource
    private MediaService mediaService;
    @Resource
    private ModelService modelService;
    @Resource
    private FlexibleSearchService flexibleSearchService;

    @Override
    public String getHybrisLogoUrl(final String logoCode) {
        final MediaModel media = mediaService.getMedia(logoCode);

        // Keep in mind that with Slf4j you don't need to check if debug is enabled, it is done under the hood.
        LOG.debug("Found media [code: {}]", media.getCode());

        return media.getURL();
    }

    @Override
    public void createLogo(final String logoCode) {
        final Optional<CatalogUnawareMediaModel> existingLogo = findExistingLogo(logoCode);

        final CatalogUnawareMediaModel media = existingLogo.isPresent() ? existingLogo.get()
                : modelService.create(CatalogUnawareMediaModel.class);
        media.setCode(logoCode);
        media.setRealFileName("sap-hybris-platform.png");
        modelService.save(media);

        mediaService.setStreamForMedia(media, getImageStream());
    }

    private final static String FIND_LOGO_QUERY = "SELECT {" + CatalogUnawareMediaModel.PK + "} FROM {"
            + CatalogUnawareMediaModel._TYPECODE + "} WHERE {" + CatalogUnawareMediaModel.CODE + "}=?code";

    private Optional<CatalogUnawareMediaModel> findExistingLogo(final String logoCode) {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_LOGO_QUERY);
        fQuery.addQueryParameter("code", logoCode);

        try {
            return Optional.of(flexibleSearchService.searchUnique(fQuery));
        } catch (final SystemException e) {
            return Optional.empty();
        }
    }

    private InputStream getImageStream() {
        return DefaultTrainingService.class.getResourceAsStream("/training/sap-hybris-platform.png");
    }

    @Override
    public void doSomeActionWithParam(final String param) {
        LOG.info("LOG doSomeAction - {}", param);
    }

    @Override
    public void test() {

    }

    @Override
    public void printTrainingItem(final SimpleTrainingItemModel trainingItem) {
        LOG.info("code - {}, randomNumber - {}", trainingItem.getCode(), trainingItem.getRandomNumber());
    }
}
