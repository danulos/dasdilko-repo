package org.training.service.data;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.model.constraints.ConstraintGroupModel;
import de.hybris.platform.validation.services.ValidationService;
import org.training.model.SimpleTrainingItemModel;
import org.training.model.WrappedFourModel;
import org.training.pojo.RandomNumberPojo;
import org.training.pojo.RandomPojo;
import org.training.pojo.SimpleTrainingPojo;
import org.training.service.AbstractDemo;

import javax.annotation.Resource;
import java.util.*;

public class ValidationDemo extends AbstractDemo {

    @Resource
    private ModelService modelService;

    @Resource
    private ValidationService validationService;

    public void demo() {
        createWithSmallNumber();
        testSimplePojo();
        validateProperty();
        validateValue();
        checkSeverity();

        checkRandomConst();
        checkRandomNumberConst();
    }

    private void createWithSmallNumber() {
        final SimpleTrainingItemModel item = modelService.create(SimpleTrainingItemModel.class);
        item.setRandomNumber(-33);

        Set<HybrisConstraintViolation> validationResults =
                validationService.validate(item, Collections.singletonList(getEmptyGroup()));

        LOG.info("###############################################");
        logValidationResults(validationResults);

        LOG.info("-----------------------------------------------");
        try {

            Collection<ConstraintGroupModel> constraintGroups = Collections.singletonList(getEmptyGroup());
            validationService.setActiveConstraintGroups(constraintGroups);
            modelService.save(item);

        } catch (Exception exception) {
            LOG.error(exception.getMessage(), exception);
        }

        Collection<ConstraintGroupModel> defaultConstraintGroup
                = Collections.singletonList(validationService.getDefaultConstraintGroup());
        validationService.setActiveConstraintGroups(defaultConstraintGroup);

        LOG.info("-----------------------------------------------");

        validationResults = validationService.validate(item, getConstraintGroups(Arrays.asList("groupOne", "groupTwo")));

        LOG.info("###############################################");
        logValidationResults(validationResults);

        LOG.info("-----------------------------------------------");
        try {
            validationService.setActiveConstraintGroups(getConstraintGroups(Arrays.asList("groupOne", "groupThree")));
            modelService.save(item);
        } catch (Exception exception) {
            LOG.error(exception.getMessage(), exception);
        }

        LOG.info("###############################################");
    }

    private void logValidationResults(final Set<HybrisConstraintViolation> validationResults) {
        for (HybrisConstraintViolation validationResult : validationResults) {
            LOG.info(validationResult.getLocalizedMessage());
        }
    }

    private void testSimplePojo() {
        final SimpleTrainingPojo pojo = new SimpleTrainingPojo();
        setWrongPojoValues(pojo);
        Set<HybrisConstraintViolation> validationResults = validationService.validate(pojo);
        LOG.info("###############################################");
        for (HybrisConstraintViolation validationResult : validationResults) {
            LOG.info(validationResult.getConstraintViolation().getMessage());
        }
        LOG.info("###############################################");
    }

    private void setWrongPojoValues(final SimpleTrainingPojo pojo) {
        pojo.setPatterned("123");

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 1);
        pojo.setPast(calendar.getTime());

        calendar.add(Calendar.YEAR, -5);
        pojo.setFuture(calendar.getTime());

        pojo.setMin(3);
        pojo.setMax(11);

        pojo.setDecimalMin(4.9);
        pojo.setDecimalMax(10.0);

        pojo.setSized("qwe");

        pojo.setEqTrue(false);
        pojo.setEqFalse(true);

        pojo.setIsNull("qwe");
        pojo.setNotNull(null);
        pojo.setNotEmpty("");
        pojo.setNotBlank("   ");

        pojo.setXorNotNullOne(null);
        pojo.setXorNotNullTwo(null);

//        pojo.setXorNotNullOne("null");
//        pojo.setXorNotNullTwo(null);
//
//        pojo.setXorNotNullOne("null");
//        pojo.setXorNotNullTwo("null");

//        pojo.setCode("COde1");
//        pojo.setCodeLowerCase("code");
    }

    private ConstraintGroupModel getEmptyGroup() {
        ConstraintGroupModel example = new ConstraintGroupModel();
        example.setId("emptyValidationGroup");
        return modelService.getByExample(example);
    }

    private List<ConstraintGroupModel> getConstraintGroups(final List<String> ids) {
        final List<ConstraintGroupModel> result = new ArrayList<>();

        for (String id : ids) {
            ConstraintGroupModel example = new ConstraintGroupModel();
            example.setId(id);
            result.add(modelService.getByExample(example));
        }

        return result;
    }

    private void validateProperty() {
        Collection<ConstraintGroupModel> constraintGroups = Collections.singletonList(getEmptyGroup());

        final SimpleTrainingItemModel item = modelService.create(SimpleTrainingItemModel.class);
        item.setRandomNumber(-33);

        Set<HybrisConstraintViolation> validationResults =
                validationService.validateProperty(item, SimpleTrainingItemModel.CODE, constraintGroups);

        logValidationResults(validationResults);
    }

    private void validateValue() {
        Collection<ConstraintGroupModel> constraintGroups = Collections.singletonList(getEmptyGroup());

        final String valueForCode = "";

        Set<HybrisConstraintViolation> validationResults = validationService
                .validateValue(SimpleTrainingItemModel.class, SimpleTrainingItemModel.CODE, valueForCode, constraintGroups);


        logValidationResults(validationResults);
    }

    private String sev = "info_group";

    private void checkSeverity() {
        LOG.info("####################  SEVERITY  ########################");
        severity("info_group");
        severity("warn_group");
        severity("err_group");

        LOG.info("-----------------------------------------------");
        try {
            final WrappedFourModel item = modelService.create(WrappedFourModel.class);
            Collection<ConstraintGroupModel> constraintGroups = getConstraintGroups(Collections.singletonList(sev));
            validationService.setActiveConstraintGroups(constraintGroups);
            modelService.save(item);

        } catch (Exception exception) {
            LOG.error(exception.getMessage(), exception);

            Collection<ConstraintGroupModel> defaultConstraintGroup
                    = Collections.singletonList(validationService.getDefaultConstraintGroup());
            validationService.setActiveConstraintGroups(defaultConstraintGroup);
        }
        LOG.info("####################  SEVERITY  ########################");
    }

    private void severity(final String group) {
        final WrappedFourModel item = modelService.create(WrappedFourModel.class);

        Set<HybrisConstraintViolation> validationResults = validationService
                .validate(item, getConstraintGroups(Collections.singletonList(group)));

        logValidationResults(validationResults);
    }

    private void checkRandomConst() {
        final RandomPojo randomPojo = new RandomPojo();

        Set<HybrisConstraintViolation> validationResults =
                validationService.validate(randomPojo);

        LOG.info("############### Random #################");
        for (HybrisConstraintViolation validationResult : validationResults) {
            LOG.info(validationResult.getConstraintViolation().getMessage());
        }
        LOG.info("############### Random #################");
    }

    private void checkRandomNumberConst() {
        final RandomNumberPojo randomPojo = new RandomNumberPojo();
        randomPojo.setNumber(5);

        Set<HybrisConstraintViolation> validationResults =
                validationService.validate(randomPojo);

        LOG.info("############### Random Number #################");
        for (HybrisConstraintViolation validationResult : validationResults) {
            LOG.info(validationResult.getConstraintViolation().getMessage());
        }
        LOG.info("############### Random Number #################");
    }

}
