package org.training.service.data;

import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.locking.ItemLockingService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.training.model.SimpleTrainingItemModel;
import org.training.service.AbstractDemo;

import javax.annotation.Resource;

public class RetentionDemo extends AbstractDemo {

    @Resource
    private CronJobService cronJobService;
    @Resource
    private ModelService modelService;
    @Resource
    private ItemLockingService itemLockingService;

    public void demo() {
        final SimpleTrainingItemModel arItem = getItem("after_17");
        itemLockingService.lock(arItem);
        final SimpleTrainingItemModel frItem = getItem("flex_param_19");
        itemLockingService.lock(frItem);

        arSecondsAndDate();
        arExpression();
        flexParam();
        flexTime();
        genericMaintenance();
        dynamicMaintenance();
    }

    private void arSecondsAndDate() {
        cronJobService.performCronJob(cronJobService.getCronJob("after_seconds_CronJob"));
    }

    private void arExpression() {
        cronJobService.performCronJob(cronJobService.getCronJob("after_expr_CronJob"));
    }

    private void flexParam() {
        cronJobService.performCronJob(cronJobService.getCronJob("flex_param_CronJob"));
    }

    private void flexTime() {
        cronJobService.performCronJob(cronJobService.getCronJob("flex_time_CronJob"));
    }

    private void genericMaintenance() {
        cronJobService.performCronJob(cronJobService.getCronJob("cleanupTrainingItemsCronJob"));
    }

    private void dynamicMaintenance() {
        cronJobService.performCronJob(cronJobService.getCronJob("dynamicCleanupTrainingItemsCronJob"));
    }

    private SimpleTrainingItemModel getItem(final String code) {
        SimpleTrainingItemModel item = new SimpleTrainingItemModel();
        item.setCode(code);
        return modelService.getByExample(item);
    }

}
