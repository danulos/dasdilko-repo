package org.training.service.data;

import de.hybris.platform.audit.TypeAuditReportConfig;
import de.hybris.platform.audit.internal.config.AtomicAttribute;
import de.hybris.platform.audit.internal.config.AuditReportConfig;
import de.hybris.platform.audit.internal.config.ReferenceAttribute;
import de.hybris.platform.audit.internal.config.ResolvesBy;
import de.hybris.platform.audit.internal.config.Type;
import de.hybris.platform.audit.view.AuditViewService;
import de.hybris.platform.audit.view.impl.ReportView;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.Transaction;
import org.training.model.SimpleTrainingItemModel;
import org.training.service.AbstractDemo;
import org.training.util.TrainingUtil;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

public class ReportDemo extends AbstractDemo {

    @Resource
    private ModelService modelService;
    @Resource
    private AuditViewService auditViewService;

    public void demo() {
        simpleReport();
        reportFromJavaConfig();
    }

    private void simpleReport() {

        final SimpleTrainingItemModel trainingItem = getTrainingItem();
        final List<ReportView> report =
                auditViewService.getViewOn(
                        TypeAuditReportConfig.builder()
                                .withConfigName("simpleItem")
                                .withRootTypePk(trainingItem.getPk())
                                .withLangIsoCodes("en")
                                .withFullReport()
                                .build()
                ).collect(Collectors.toList());

        LOG.info(report.toString());
    }

    private SimpleTrainingItemModel getTrainingItem() {
        SimpleTrainingItemModel example = new SimpleTrainingItemModel();
        example.setCode("audit_1");
        return modelService.getByExample(example);
    }

    private void reportFromJavaConfig() {
        final SimpleTrainingItemModel trainingItem = getTrainingItem();
        final TypeAuditReportConfig config = TypeAuditReportConfig.builder()
                .withConfig(generateReportConfig())
                .withRootTypePk(trainingItem.getPk()).build();

        final List<ReportView> report =
                auditViewService.getViewOn(config).collect(Collectors.toList());

        LOG.info(report.toString());
    }

    private AuditReportConfig generateReportConfig() {
        final Type wrappedOne = Type.builder().withCode("WrappedOne")
                .withAtomicAttributes(AtomicAttribute.builder().withQualifier("code").build())
                .build();
        final Type trainingItem = Type.builder().withCode("SimpleTrainingItem")
                .withAtomicAttributes(AtomicAttribute.builder().withQualifier("code").withDisplayName("Unique code")
                                .build())
                .withReferenceAttributes(ReferenceAttribute.builder()
                                .withQualifier("wrappedOne").withType(wrappedOne)
                                .withResolvesBy(ResolvesBy.builder()
                                                .withResolverBeanId("typeReferencesResolver")
                                                .withExpression("wrappedOne").build()).build())
                .build();
        return AuditReportConfig.builder()
                .withGivenRootType(trainingItem).withName("simpleItem")
                .withAuditRecordsProvider("auditRecordsProvider")
                .withTypes(trainingItem, wrappedOne)
                .build();
    }
}
