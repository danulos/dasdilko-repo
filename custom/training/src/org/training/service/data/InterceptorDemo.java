package org.training.service.data;

import de.hybris.platform.servicelayer.interceptor.impl.InterceptorExecutionPolicy;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import org.training.enums.StaticTrainingEnum;
import org.training.model.*;
import org.training.service.AbstractDemo;
import org.training.util.TrainingUtil;

import javax.annotation.Resource;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class InterceptorDemo extends AbstractDemo {

    @Resource
    private SessionService sessionService;
    @Resource
    private ModelService modelService;

    public void demo() {
        TrainingUtil.printMessage("CREATE");
        SimpleTrainingItemModel createdWithService = modelService.create(SimpleTrainingItemModel.class);

        final String code = UUID.randomUUID().toString();
        createdWithService.setCode(code);
        createdWithService.setLocalizedCode("US_" + code, Locale.US);
        createdWithService.setLocalizedCode("CA_" + code, Locale.CANADA);
        createdWithService.setLocalizedCode("DE_" + code, Locale.GERMAN);
        createdWithService.setTrainingEnum(StaticTrainingEnum.VALUE_ONE);

        WrappedFourModel w4 = new WrappedFourModel();

        WrappedThreeModel w3 = new WrappedThreeModel();
        w3.setWrappedFour(w4);

        WrappedTwoModel w2 = new WrappedTwoModel();
        w2.setWrappedThree(w3);

        WrappedOneModel w1 = new WrappedOneModel();
        w1.setWrappedTwo(w2);
        createdWithService.setWrappedOne(w1);


        TrainingUtil.printMessage("SAVE");
        modelService.save(createdWithService);

        TrainingUtil.printMessage("SAVE [2]");
        createdWithService.setCode(code + "[2]");
        modelService.save(createdWithService);

        disableInterceptorByType();
        disableInterceptorByBeanId();
        disableUniqAttrValidator();
    }

    private void disableInterceptorByType() {
        final Map<String, Object> params = Map.of(InterceptorExecutionPolicy.DISABLED_INTERCEPTOR_TYPES,
                Set.of(InterceptorExecutionPolicy.InterceptorType.INIT_DEFAULTS,
                        InterceptorExecutionPolicy.InterceptorType.VALIDATE));
        sessionService.executeInLocalViewWithParams(params, new SessionExecutionBody() {
            @Override
            public void executeWithoutResult() {
                SimpleTrainingItemModel simpleTrainingItem = modelService.create(SimpleTrainingItemModel.class);
                final String code = UUID.randomUUID().toString();
                simpleTrainingItem.setCode(code);
                modelService.save(simpleTrainingItem);
            }
        });
    }

    private void disableInterceptorByBeanId() {
        final Map<String, Object> params = Map.of(
                InterceptorExecutionPolicy.DISABLED_INTERCEPTOR_BEANS,
                Set.of("simpleTrainingItemValidateInterceptor", "simpleTrainingItemInitDefaultsInterceptor"));

        sessionService.executeInLocalViewWithParams(params, new SessionExecutionBody() {
            @Override
            public void executeWithoutResult() {
                SimpleTrainingItemModel simpleTrainingItem = modelService.create(SimpleTrainingItemModel.class);
                modelService.save(simpleTrainingItem);
            }
        });
    }

    private void disableUniqAttrValidator() {
        final Map<String, Object> params = Map.of(
                InterceptorExecutionPolicy.DISABLED_UNIQUE_ATTRIBUTE_VALIDATOR_FOR_ITEM_TYPES,
                Set.of("SimpleTrainingItem"));

        sessionService.executeInLocalViewWithParams(params, new SessionExecutionBody() {
            @Override
            public void executeWithoutResult() {
                SimpleTrainingItemModel simpleTrainingItem = modelService.create(SimpleTrainingItemModel.class);
                simpleTrainingItem.setUniqueProperty("not_unique");
                modelService.save(simpleTrainingItem);
                simpleTrainingItem = modelService.create(SimpleTrainingItemModel.class);
                simpleTrainingItem.setUniqueProperty("not_unique");
                modelService.save(simpleTrainingItem);
            }
        });
    }
}
