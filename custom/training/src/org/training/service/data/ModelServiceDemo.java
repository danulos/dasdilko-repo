package org.training.service.data;

import de.hybris.platform.core.PK;
import de.hybris.platform.servicelayer.internal.model.ModelCloningContext;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.tx.Transaction;
import org.training.enums.StaticTrainingEnum;
import org.training.model.*;
import org.training.service.AbstractDemo;
import org.training.util.TrainingUtil;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class ModelServiceDemo extends AbstractDemo {

    @Resource
    private SessionService sessionService;
    @Resource
    private ModelService modelService;
    @Resource
    private FlexibleSearchService flexibleSearchService;
    @Resource(name = "simpleTrainingItemCloningContext")
    private ModelCloningContext modelCloningContext;

    private final String code = TrainingUtil.getCode();

    public void demo() {
//        cloning();
//        cloningNonPartOf();
//        cloningPartOf();
//        getByExampleAndRefresh();
//        getByPkAndExample();
//        itemLock();
//        saveInTransaction();
//        itemState();
//        isUniqueConstraint();
//        defaultAreSetOnSave();
//        afterSaveEvent();
    }

    private void cloning() {
        SimpleTrainingItemModel cloneSource =
                modelService.create(SimpleTrainingItemModel.class);
        cloneSource.setCode(TrainingUtil.getCode());
        cloneSource.setMandatory(TrainingUtil.getCode());
        modelService.save(cloneSource);

        SimpleTrainingItemModel itemOne =
                modelService.create(SimpleTrainingItemModel.class);
        SimpleTrainingItemModel itemTwo =
                modelService.create("SimpleTrainingItem");

        SimpleTrainingItemModel itemThree =
                modelService.clone(cloneSource);
        SimpleTrainingItemModel itemFour =
                modelService.clone(cloneSource, SubTrainingItemModel.class);
        SimpleTrainingItemModel itemFive =
                modelService.clone(cloneSource, modelCloningContext);
        SimpleTrainingItemModel itemSix =
                modelService.clone(cloneSource,
                        SubTrainingItemModel.class, modelCloningContext);

        // all are attached to the model context
        // you can't clone items with readOnly attributes

        boolean itemOneIsAttached = modelService.isAttached(itemOne); // true
        boolean itemTwoIsAttached = modelService.isAttached(itemTwo); // true
        boolean itemThreeIsAttached = modelService.isAttached(itemThree); // true
        boolean itemFourIsAttached = modelService.isAttached(itemFour); // true
        boolean itemFiveIsAttached = modelService.isAttached(itemFive); // true

        System.out.println("itemOneIsAttached - " + itemOneIsAttached);
        System.out.println("itemTwoIsAttached - " + itemTwoIsAttached);
        System.out.println("itemThreeIsAttached - " + itemThreeIsAttached);
        System.out.println("itemFourIsAttached - " + itemFourIsAttached);
        System.out.println("itemFiveIsAttached - " + itemFiveIsAttached);
        Arrays.asList(itemOne, itemTwo, itemFive, itemSix);
    }

    private void cloningNonPartOf() {
        WrappedFourModel w4 = new WrappedFourModel();
        w4.setCode("w4_" + code);

        WrappedThreeModel w3 = new WrappedThreeModel();
        w3.setCode("w3_" + code);
        w3.setWrappedFour(w4);

        WrappedTwoModel w2 = new WrappedTwoModel();
        w2.setCode("w2_" + code);
        w2.setWrappedThree(w3);

        WrappedOneModel cloneSource = new WrappedOneModel();
        cloneSource.setCode("w1_" + code);
        cloneSource.setWrappedTwo(w2);

        modelService.save(cloneSource);
        WrappedOneModel itemThree = modelService.clone(cloneSource);
        itemThree.setCode("[c]w1_" + code);
        modelService.save(itemThree);
    }

    private void cloningPartOf() {
        WrappedFourModel w4 = new WrappedFourModel();
        w4.setCode("[p]w4_" + code);

        WrappedThreeModel w3 = new WrappedThreeModel();
        w3.setCode("[p]w3_" + code);
        w3.setPartOfWrappedFour(w4);

        WrappedTwoModel w2 = new WrappedTwoModel();
        w2.setCode("[p]w2_" + code);
        w2.setPartOfWrappedThree(w3);

        WrappedOneModel cloneSource = new WrappedOneModel();
        cloneSource.setCode("[p]w1_" + code);
        cloneSource.setPartOfWrappedTwo(w2);

        modelService.save(cloneSource);
        WrappedOneModel itemThree = modelService.clone(cloneSource);
        itemThree.setCode("[pc]w1_" + code);
        modelService.save(itemThree);
    }

    private void getByExampleAndRefresh() {
        SimpleTrainingItemModel item = new SimpleTrainingItemModel();
        item.setCode("code");
        try {
            item = modelService.getByExample(item);
        } catch (Exception e) {
            System.out.println("Model.getExample - failed");
            try {
                item = flexibleSearchService.getModelByExample(item);
            } catch (Exception e1) {
                System.out.println("Flex.getExample - failed");
                modelService.save(item);
            }
        }

        item.setEncrypted("previous");
        modelService.save(item);
        item.setEncrypted("new");
        modelService.refresh(item);
        // previous
        LOG.info("After refresh - " + item.getEncrypted());

        LOG.info("Model type - " + modelService.getModelType(item));
        LOG.info("Model type(class) - " + modelService.getModelType(item.getClass()));
    }

    private void getByPkAndExample() {
        SimpleTrainingItemModel itemOne =
                modelService.get(PK.parse("8796093065980"));

        SimpleTrainingItemModel example = new SimpleTrainingItemModel();
        example.setCode("code");

        SimpleTrainingItemModel itemTwo =
                modelService.getByExample(example);

        SimpleTrainingItemModel itemThree =
                modelService.getByExample(example);


        modelService.remove(itemOne);
        modelService.removeAll(itemTwo, itemThree);
        modelService.removeAll(
                Arrays.asList(itemTwo, itemThree));

        Arrays.asList(itemOne, itemOne);
    }

    void itemLock() {
        SimpleTrainingItemModel item = new SimpleTrainingItemModel();
        item.setCode("code");
        item = modelService.getByExample(item);

        Transaction tx = Transaction.current();
        tx.begin();
        try {
            modelService.lock(item.getPk());

            item.setCode("code");
            modelService.save(item);

            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }

    }

    private boolean enableTransaction = true;

    void saveInTransaction() {
        SimpleTrainingItemModel item = modelService.create(SimpleTrainingItemModel.class);

        final String code = TrainingUtil.getCode();
        item.setCode(code);
        item.setLocalizedCode("US_" + code, Locale.US);
        item.setLocalizedCode("CA_" + code, Locale.CANADA);
        item.setLocalizedCode("DE_" + code, Locale.GERMAN);
        item.setTrainingEnum(StaticTrainingEnum.VALUE_ONE);

        WrappedFourModel w4 = new WrappedFourModel();

        WrappedThreeModel w3 = new WrappedThreeModel();
        w3.setWrappedFour(w4);

        WrappedTwoModel w2 = new WrappedTwoModel();
        w2.setWrappedThree(w3);

        WrappedOneModel w1 = new WrappedOneModel();
        w1.setWrappedTwo(w2);
        item.setWrappedOne(w1);

        TrainingUtil.printMessage("SAVE");
        sessionService.executeInLocalView(new SessionExecutionBody() {
            @Override
            public Object execute() {
                if (enableTransaction) {
                    modelService.enableTransactions();
                } else {
                    modelService.disableTransactions();
                }
                modelService.save(item);
                modelService.clearTransactionsSettings();
                return item;
            }
        });

        enableTransaction = !enableTransaction;
    }

    void itemState() {
        SimpleTrainingItemModel item = getItem();

        boolean isNew =
                modelService.isNew(item);

        boolean isModified =
                modelService.isModified(item);

        boolean isUpToDate =
                modelService.isUpToDate(item);

        boolean isRemoved =
                modelService.isRemoved(item);

        System.out.println(isNew);
        System.out.println(isModified);
        System.out.println(isUpToDate);
        System.out.println(isRemoved);
    }

    void isUniqueConstraint() {
        SimpleTrainingItemModel item = getItem();

        modelService.initDefaults(item);
        String value = modelService.getAttributeValue(item, "code");
        modelService.setAttributeValue(item, "code", "new_code");

        try {
            item.setCode("not_unique");
            modelService.save(item);
        } catch (Exception exception) {
            if (modelService.isUniqueConstraintErrorAsRootCause(exception)) {

            }
        }

        modelService.refresh(item);

        System.out.println(value);
    }

    void defaultAreSetOnSave() {
        SimpleTrainingItemModel item = new SimpleTrainingItemModel();
        item.setCode("unique");
        modelService.save(item);
    }

    private SimpleTrainingItemModel getItem() {
        SimpleTrainingItemModel item = new SimpleTrainingItemModel();
        item.setCode("code");
        return modelService.getByExample(item);
    }

    private void afterSaveEvent() {
        final SimpleTrainingItemModel simpleTrainingItem = modelService.create(SimpleTrainingItemModel.class);
        simpleTrainingItem.setCode("simpleTrainingItem_" + TrainingUtil.getCode());
        final SubSeparateTrainingItemModel subSeparateTrainingItem = modelService.create(SubSeparateTrainingItemModel.class);
        subSeparateTrainingItem.setCode("subSeparateTrainingItem" + TrainingUtil.getCode());
        final SubSeparateSubSeparateTrainingItemModel subSeparateSubSeparateTrainingItem = modelService.create(SubSeparateSubSeparateTrainingItemModel.class);
        subSeparateSubSeparateTrainingItem.setCode("subSeparateSubSeparateTrainingItem" + TrainingUtil.getCode());
        final SubSubSeparateTrainingItemModel subSubSeparateTrainingItem = modelService.create(SubSubSeparateTrainingItemModel.class);
        subSubSeparateTrainingItem.setCode("subSubSeparateTrainingItem" + TrainingUtil.getCode());
        LOG.info("beforeSaveAll....");
        modelService.saveAll(simpleTrainingItem, subSeparateTrainingItem, subSeparateSubSeparateTrainingItem, subSubSeparateTrainingItem);
        LOG.info("afterSaveAll....");

        TrainingUtil.sleepFor(2_000L);

        int max = 35;
        for (int i = 0; i < max; i++) {
            final SimpleTrainingItemModel item = modelService.create(SimpleTrainingItemModel.class);
            item.setCode("single_" + i + "_" + TrainingUtil.getCode());
            LOG.info("before save...");
            modelService.save(item);
            LOG.info("after save...");
        }

    }
}
