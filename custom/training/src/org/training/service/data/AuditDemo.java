package org.training.service.data;

import de.hybris.platform.persistence.audit.gateway.AuditRecord;
import de.hybris.platform.persistence.audit.gateway.AuditSearchQuery;
import de.hybris.platform.persistence.audit.gateway.ReadAuditGateway;
import de.hybris.platform.persistence.audit.gateway.WriteAuditGateway;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.Transaction;
import org.training.model.AuditOneItemModel;
import org.training.model.AuditThreeItemModel;
import org.training.model.AuditTwoItemModel;
import org.training.model.SimpleTrainingItemModel;
import org.training.model.WrappedOneModel;
import org.training.service.AbstractDemo;
import org.training.util.TrainingUtil;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AuditDemo extends AbstractDemo {

    @Resource
    private ModelService modelService;
    @Resource
    private ReadAuditGateway readAuditGateway;
    @Resource
    private WriteAuditGateway writeAuditGateway;

    private SimpleTrainingItemModel itemOne;
    private SimpleTrainingItemModel itemTwo;

    public void demo() {
        filter();
        crudInTx();
        crudNoTx();
        search();
        clear();
    }

    private void filter() {
        createNotAuditedItemSimpleCase();
        createFilteredAuditedItemComplexCase();
        createAuditedItemComplexCase();

        LOG.info("==============================================");
        LOG.info("==============================================");

        printSizeAndClear("AuditOneItem");
        printSizeAndClear("AuditTwoItem");
        printSizeAndClear("AuditThreeItem");

        LOG.info("==============================================");
        LOG.info("==============================================");
    }

    private void printSizeAndClear(final String type) {
        final AuditSearchQuery query = AuditSearchQuery.forType(type)
                .build();
        final List<AuditRecord> records = readAuditGateway.search(query)
                .collect(Collectors.toList());

        LOG.info(type + " - " + records.size());
        writeAuditGateway.removeAuditRecordsForType(type);
    }

    private void createNotAuditedItemSimpleCase() {
        final SimpleTrainingItemModel item = modelService.create(SimpleTrainingItemModel.class);
        final WrappedOneModel wr1 = modelService.create(WrappedOneModel.class);
        item.setCode("wrap" + TrainingUtil.getCode());
        wr1.setCode("wrap" + TrainingUtil.getCode());
        modelService.save(wr1);
        item.setWrappedOne(wr1);
        item.setRandomNumber(123);
        modelService.save(item);
    }

    private void createAuditedItemComplexCase() {
        final AuditOneItemModel one = modelService.create(AuditOneItemModel.class);
        final AuditTwoItemModel two1 = modelService.create(AuditTwoItemModel.class);
        final AuditTwoItemModel two2 = modelService.create(AuditTwoItemModel.class);
        final AuditThreeItemModel three1 = modelService.create(AuditThreeItemModel.class);
        final AuditThreeItemModel three12 = modelService.create(AuditThreeItemModel.class);
        final AuditThreeItemModel three2 = modelService.create(AuditThreeItemModel.class);
        final AuditThreeItemModel three22 = modelService.create(AuditThreeItemModel.class);
        one.setCode("audit_one_" + TrainingUtil.getCode());
        two1.setCode("audit_two1" + TrainingUtil.getCode());
        two2.setCode("audit_two2" + TrainingUtil.getCode());
        one.setAuditTwoMany(Arrays.asList(two1, two2));
        three1.setCode("audit_three1" + TrainingUtil.getCode());
        three12.setCode("audit_three12" + TrainingUtil.getCode());
        two1.setAuditThreeMany(Arrays.asList(three1, three12));
        three2.setCode("audit_three2" + TrainingUtil.getCode());
        three22.setCode("audit_three22" + TrainingUtil.getCode());
        two2.setAuditThreeMany(Arrays.asList(three2, three22));
        modelService.save(one);
    }

    private void createFilteredAuditedItemComplexCase() {
        final AuditOneItemModel one = modelService.create(AuditOneItemModel.class);
        final AuditTwoItemModel two1 = modelService.create(AuditTwoItemModel.class);
        final AuditTwoItemModel two2 = modelService.create(AuditTwoItemModel.class);
        final AuditThreeItemModel three1 = modelService.create(AuditThreeItemModel.class);
        final AuditThreeItemModel three12 = modelService.create(AuditThreeItemModel.class);
        final AuditThreeItemModel three2 = modelService.create(AuditThreeItemModel.class);
        final AuditThreeItemModel three22 = modelService.create(AuditThreeItemModel.class);
        one.setCode("one_" + TrainingUtil.getCode());
        two1.setCode("two1" + TrainingUtil.getCode());
        two2.setCode("two2" + TrainingUtil.getCode());
        one.setAuditTwoMany(Arrays.asList(two1, two2));
        three1.setCode("three1" + TrainingUtil.getCode());
        three12.setCode("three12" + TrainingUtil.getCode());
        two1.setAuditThreeMany(Arrays.asList(three1, three12));
        three2.setCode("three2" + TrainingUtil.getCode());
        three22.setCode("three22" + TrainingUtil.getCode());
        two2.setAuditThreeMany(Arrays.asList(three2, three22));
        modelService.save(one);
    }

    private void crudInTx() {
        createAndUpdateInTx();
        updateTwiceInTx();
        updateAndRemoveInTx();
    }

    private void createAndUpdateInTx() {
        Transaction tx = Transaction.current();
        tx.begin();
        itemOne = modelService.create(SimpleTrainingItemModel.class);
        final WrappedOneModel wr1 = modelService.create(WrappedOneModel.class);
        wr1.setCode("audit_IN_create_" + TrainingUtil.getCode());
        itemOne.setWrappedOne(wr1);
        itemOne.setCode("audit_wrap_IN_create_" + TrainingUtil.getCode());
        itemOne.setRandomNumber(1);
        modelService.save(itemOne);
        itemOne.setUniqueProperty(TrainingUtil.getCode());
        modelService.save(itemOne);
        tx.commit();
    }

    private void updateTwiceInTx() {
        Transaction tx = Transaction.current();
        tx.begin();
        itemOne.setUniqueProperty(TrainingUtil.getCode());
        modelService.save(itemOne);
        itemOne.setRandomNumber(itemOne.getRandomNumber() + 5);
        modelService.save(itemOne);
        tx.commit();
    }

    private void updateAndRemoveInTx() {
        Transaction tx = Transaction.current();
        tx.begin();
        itemOne.setRandomNumber(itemOne.getRandomNumber() + 1);
        modelService.save(itemOne);
        modelService.remove(itemOne);
        tx.commit();
    }

    private void crudNoTx() {
        createAndUpdateNoTx();
        updateTwiceNoTx();
        updateAndRemoveNoTx();
    }

    private void createAndUpdateNoTx() {
        itemTwo = modelService.create(SimpleTrainingItemModel.class);
        final WrappedOneModel wr1 = modelService.create(WrappedOneModel.class);
        wr1.setCode("audit_NO_create_" + TrainingUtil.getCode());
        itemTwo.setWrappedOne(wr1);
        itemTwo.setCode("audit_wrap_NO_create_" + TrainingUtil.getCode());
        itemTwo.setRandomNumber(1);
        modelService.save(itemTwo);
        itemTwo.setUniqueProperty(TrainingUtil.getCode());
        modelService.save(itemTwo);
    }

    private void updateTwiceNoTx() {
        itemTwo.setUniqueProperty(TrainingUtil.getCode());
        modelService.save(itemTwo);
        itemTwo.setRandomNumber(itemTwo.getRandomNumber() + 5);
        modelService.save(itemTwo);
    }

    private void updateAndRemoveNoTx() {
        itemTwo.setRandomNumber(itemTwo.getRandomNumber() + 1);
        modelService.save(itemTwo);
        modelService.remove(itemTwo);
    }

    private void search() {
        simpleSearchInTx();
        simpleSearchNoTx();
        simpleSearchBoth();
        simpleSearchAll();
        simpleSearchFiltered();
    }

    private void simpleSearchInTx() {
        final AuditSearchQuery query = AuditSearchQuery.forType("SimpleTrainingItem")
                .withPkSearchRules(itemOne.getPk()).build();
        final List<AuditRecord> records = readAuditGateway.search(query).collect(Collectors.toList());

        LOG.info("simpleSearchInTx - " + records.size());
    }

    private void simpleSearchNoTx() {
        final AuditSearchQuery query = AuditSearchQuery.forType("SimpleTrainingItem")
                .withPkSearchRules(itemTwo.getPk()).build();
        final List<AuditRecord> records = readAuditGateway.search(query).collect(Collectors.toList());

        LOG.info("simpleSearchNoTx - " + records.size());
    }

    private void simpleSearchBoth() {

        AuditSearchQuery query = AuditSearchQuery.forType("SimpleTrainingItem")
          .withPkSearchRules(itemOne.getPk(), itemTwo.getPk()).build();
        List<AuditRecord> records = readAuditGateway.search(query)
                .collect(Collectors.toList());

        LOG.info("simpleSearchBoth - " + records.size());
    }

    private void simpleSearchAll() {

        AuditSearchQuery query = AuditSearchQuery.forType("SimpleTrainingItem")
                .build();
        List<AuditRecord> records = readAuditGateway.search(query)
                .collect(Collectors.toList());

        LOG.info("simpleSearchAll - " + records.size());
    }

    private void simpleSearchFiltered() {

        AuditSearchQuery query = AuditSearchQuery.forType("SimpleTrainingItem")
                .withPkSearchRules(itemTwo.getPk())
                .withPayloadSearchRule("mandatorystring", "value")
                .build();
        List<AuditRecord> records = readAuditGateway.search(query)
                .collect(Collectors.toList());

        LOG.info("simpleSearchFiltered - " + records.size());
    }

    private void clear() {
        clearSingleItemRecords();
        clearWholeTypeRecords();
    }

    private void clearSingleItemRecords() {
        writeAuditGateway.removeAuditRecordsForType("SimpleTrainingItem", itemTwo.getPk());
    }

    private void clearWholeTypeRecords() {
        writeAuditGateway.removeAuditRecordsForType("SimpleTrainingItem");
    }
}
