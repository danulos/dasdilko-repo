package org.training.service.data.search;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.training.model.PolyglotInnerItemModel;
import org.training.model.PolyglotItemModel;
import org.training.service.AbstractDemo;
import org.training.util.TrainingUtil;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class PolyglotDemo extends AbstractDemo {

    @Resource
    private ModelService modelService;
    @Resource
    private FlexibleSearchService flexibleSearchService;

    private String code = "";

    public void demo() {
        create();
        get();
    }

    private void create() {
        code = TrainingUtil.getCode();
        final String codeParam = "item_" + code;
        final String lodDeParam = "loc_de_" + code;
        final String innerCodeParam = "innerItem_" + code;

        final PolyglotItemModel item = modelService.create(PolyglotItemModel.class);
        item.setCode(codeParam);
        item.setNumber(new Random().nextInt(10));
        item.setLocalized(lodDeParam, Locale.GERMAN);
        item.setLocalized("loc_en_" + code, Locale.ENGLISH);
        final PolyglotInnerItemModel innerItem = modelService.create(PolyglotInnerItemModel.class);
        item.setInnerItem(innerItem);
        innerItem.setCode(innerCodeParam);
        modelService.saveAll(item, innerItem);

    }

    private void get() {
        final String codeParam = "item_" + code;

        final Map<String, Object> queryValues = new HashMap<>();
        queryValues.put("code", codeParam);
        queryValues.put("number", 0);

        final SearchResult<PolyglotItemModel> searchResult = flexibleSearchService.search(
                "GET {PolyglotItem} WHERE {code} = ?code OR {number} > ?number ORDER BY {number}", queryValues);

        LOG.info("searchResult.size - " + searchResult.getResult().size());

        for (PolyglotItemModel result : searchResult.getResult()) {
            LOG.info("code - " + result.getCode());
            LOG.info("number - " + result.getNumber());
            LOG.info("loc[de] - " + result.getLocalized(Locale.GERMAN));
            LOG.info("loc[en] - " + result.getLocalized(Locale.ENGLISH));
            if (result.getInnerItem() != null) {
                LOG.info("inner code - " + result.getInnerItem().getCode());
            }
        }
    }
}
