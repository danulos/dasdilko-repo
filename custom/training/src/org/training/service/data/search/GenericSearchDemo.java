package org.training.service.data.search;

import de.hybris.platform.core.*;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.genericsearch.GenericSearchQuery;
import de.hybris.platform.genericsearch.GenericSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;
import org.training.model.ManyToManyExampleItemModel;
import org.training.model.SimpleTrainingItemModel;
import org.training.model.WrappedOneModel;
import org.training.service.AbstractDemo;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class GenericSearchDemo extends AbstractDemo {

    @Resource
    private UserService userService;
    @Resource
    private GenericSearchService genericSearchService;

    public void demo() {
        selectAll();
        whereCodeLike();
        whereLocalizedCodeLike();
        whereNumberGreater();
        orderByBuildDate();
        innerJoinWrapOneWhereWrapCodeLike();
        joinManyToManyLocalizedRelation();
        outerJoinWrapOneWhereWrapCodeLike();
        selectCodeAndBuildDate();
        countAndAsUser();
        withResettableValues();
        selectCountPk();
        withSubQuery();
    }

    private void selectAll() {

        final GenericQuery query = new GenericQuery(SimpleTrainingItemModel._TYPECODE);

        final SearchResult<SimpleTrainingItemModel> searchResult = genericSearchService.search(query);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("selectAll: " + items.size());
    }

    private void whereCodeLike() {

        final GenericQuery query = new GenericQuery(SimpleTrainingItemModel._TYPECODE);

        final GenericSearchField codeField = new GenericSearchField(SimpleTrainingItemModel._TYPECODE, SimpleTrainingItemModel.CODE);
        final GenericCondition condition = GenericCondition.caseSensitiveLike(codeField, "code_%");

        query.addConditions(condition);

        final SearchResult<SimpleTrainingItemModel> searchResult = genericSearchService.search(query);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("whereCodeLike: " + items.size());
    }

    private void whereLocalizedCodeLike() {

        final GenericQuery query = new GenericQuery(SimpleTrainingItemModel._TYPECODE);

        final GenericSearchField localizedCodeField = new GenericSearchField(SimpleTrainingItemModel._TYPECODE, SimpleTrainingItemModel.LOCALIZEDCODE);
        localizedCodeField.setFieldTypes(Collections.singletonList(GenericSearchFieldType.LOCALIZED));

        final GenericCondition condition = GenericCondition.like(localizedCodeField, "de_value");
        query.addConditions(condition);

        final GenericSearchQuery searchQuery = new GenericSearchQuery(query);
        searchQuery.setLocale(Locale.GERMAN);

        final SearchResult<SimpleTrainingItemModel> searchResult = genericSearchService.search(searchQuery);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("whereLocalizedCodeLike: " + items.size());
    }

    private void whereNumberGreater() {

        final GenericQuery query = new GenericQuery(SimpleTrainingItemModel._TYPECODE);

        final GenericSearchField randomNumberField =
                new GenericSearchField(SimpleTrainingItemModel._TYPECODE, SimpleTrainingItemModel.RANDOMNUMBER);
        final GenericCondition condition = GenericCondition.greater(randomNumberField, 50);

        query.addConditions(condition);

        final SearchResult<SimpleTrainingItemModel> searchResult = genericSearchService.search(query);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("whereNumberGreater: " + items.size());
    }

    private void orderByBuildDate() {

        final GenericQuery query = new GenericQuery(SimpleTrainingItemModel._TYPECODE);

        final GenericSearchField buildDateField = new GenericSearchField(SimpleTrainingItemModel._TYPECODE, SimpleTrainingItemModel.BUILDDATE);
        final GenericSearchOrderBy orderBy = new GenericSearchOrderBy(buildDateField, false);

        query.addOrderBy(orderBy);

        final SearchResult<SimpleTrainingItemModel> searchResult = genericSearchService.search(query);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("orderByBuildDate: " + items.size());
    }

    private void innerJoinWrapOneWhereWrapCodeLike() {

        final GenericQuery query = new GenericQuery(SimpleTrainingItemModel._TYPECODE);

        final GenericSearchField simpleWrappedField = new GenericSearchField(SimpleTrainingItemModel._TYPECODE, SimpleTrainingItemModel.WRAPPEDONE);
        final GenericSearchField wrappedPkField = new GenericSearchField(WrappedOneModel._TYPECODE, ItemModel.PK);

        final GenericCondition joinCondition = GenericCondition.createJoinCondition(simpleWrappedField, wrappedPkField);
        query.addInnerJoin(WrappedOneModel._TYPECODE, joinCondition);

        final GenericSearchField codeField = new GenericSearchField(WrappedOneModel._TYPECODE, WrappedOneModel.CODE);

        final GenericCondition condition = GenericCondition.createConditionForValueComparison(codeField, Operator.LIKE, "code_1%");
        query.addConditions(condition);

        final SearchResult<SimpleTrainingItemModel> searchResult = genericSearchService.search(query);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("innerJoinWrapOneWhereWrapCodeLike: " + items.size());
    }

    private void joinManyToManyLocalizedRelation() {

        final GenericQuery query = new GenericQuery(SimpleTrainingItemModel._TYPECODE);

        final GenericSearchField trainingPk = new GenericSearchField(SimpleTrainingItemModel._TYPECODE, ItemModel.PK);
        final GenericSearchField source = new GenericSearchField("tr2mtm", "source");
        final GenericCondition sourceJoinCondition = GenericCondition.createJoinCondition(trainingPk, source);

        final GenericSearchField manyToManyPk = new GenericSearchField(ManyToManyExampleItemModel._TYPECODE, ItemModel.PK);
        final GenericSearchField target = new GenericSearchField("tr2mtm", "target");
        final GenericCondition targetJoinCondition = GenericCondition.createJoinCondition(manyToManyPk, target);

        query.addInnerJoin("LocalizedSimpleTrainingItem2ManyToManyExampleItemRelation", "tr2mtm", sourceJoinCondition);
        query.addInnerJoin(ManyToManyExampleItemModel._TYPECODE, targetJoinCondition);

        final GenericSearchField codeField = new GenericSearchField(ManyToManyExampleItemModel._TYPECODE, ManyToManyExampleItemModel.CODE);
        final GenericCondition condition = GenericCondition.createConditionForValueComparison(codeField, Operator.LIKE, "code%");
        query.addConditions(condition);

        final SearchResult<SimpleTrainingItemModel> searchResult = genericSearchService.search(query);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("joinManyToManyLocalizedRelation: " + items.size());
    }

    private void outerJoinWrapOneWhereWrapCodeLike() {

        final GenericQuery query = new GenericQuery(SimpleTrainingItemModel._TYPECODE);

        final GenericSearchField simpleWrappedField = new GenericSearchField(SimpleTrainingItemModel._TYPECODE, SimpleTrainingItemModel.WRAPPEDONE);
        final GenericSearchField wrappedPkField = new GenericSearchField(WrappedOneModel._TYPECODE, ItemModel.PK);

        final GenericCondition joinCondition = GenericCondition.createJoinCondition(simpleWrappedField, wrappedPkField);
        query.addOuterJoin(WrappedOneModel._TYPECODE, joinCondition);

        final GenericSearchField codeField = new GenericSearchField(WrappedOneModel._TYPECODE, WrappedOneModel.CODE);

        final GenericCondition condition = GenericCondition.createConditionForValueComparison(codeField, Operator.LIKE, "code_1%");
        query.addConditions(condition);

        final SearchResult<SimpleTrainingItemModel> searchResult = genericSearchService.search(query);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("outerJoinWrapOneWhereWrapCodeLike: " + items.size());
    }

    private void selectCodeAndBuildDate() {

        final GenericQuery query = new GenericQuery(SimpleTrainingItemModel._TYPECODE);

        final GenericSelectField codeField =
                new GenericSelectField(SimpleTrainingItemModel._TYPECODE, SimpleTrainingItemModel.CODE, String.class);
        final GenericSelectField buildDateField =
                new GenericSelectField(SimpleTrainingItemModel._TYPECODE, SimpleTrainingItemModel.BUILDDATE, String.class);

        query.addSelectField(codeField);
        query.addSelectField(buildDateField);

        final SearchResult<List<String>> searchResult = genericSearchService.search(query);
        final List<List<String>> items = searchResult.getResult();

        for (List<String> row : items) {
            LOG.info("codeField: " + row.get(0) + "; buildDateField: " + row.get(1));

            break;
        }

        LOG.info("selectCodeAndBuildDate: " + items.size());
    }

    private void countAndAsUser() {

        final GenericQuery query = new GenericQuery(SimpleTrainingItemModel._TYPECODE);

        final GenericSearchQuery searchQuery = new GenericSearchQuery(query);
        searchQuery.setCount(5);
        searchQuery.setUser(userService.getUserForUID("admin"));

        final SearchResult<SimpleTrainingItemModel> searchResult = genericSearchService.search(searchQuery);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("countAndAsUser: " + items.size());
    }

    private void selectCountPk() {

        final GenericQuery query = new GenericQuery(SimpleTrainingItemModel._TYPECODE);

        query.addSelectField(new GenericFunctionSelectField(SimpleTrainingItemModel.PK, Integer.class, "COUNT"));

        final SearchResult<Integer> searchResult = genericSearchService.search(query);
        final Integer result = searchResult.getResult().get(0);

        LOG.info("selectCountPk: " + result);
    }

    private void withResettableValues() {

        final GenericSearchField codeField = new GenericSearchField(SimpleTrainingItemModel._TYPECODE, SimpleTrainingItemModel.CODE);
        final GenericSearchField attrOneField = new GenericSearchField(SimpleTrainingItemModel._TYPECODE, SimpleTrainingItemModel.ATTRIBUTEONE);

        final String codeQualifier = "code_qual";
        final String attrOneQualifier = "attr1_qual";
        final GenericQuery query = new GenericQuery(SimpleTrainingItemModel._TYPECODE);

        query.addCondition(GenericCondition.createConditionForValueComparison(codeField, Operator.EQUAL, "code",
                codeQualifier));
        query.addCondition(GenericCondition.createConditionForValueComparison(attrOneField, Operator.EQUAL,
                "attr", attrOneQualifier));

        query.getCondition().setResettableValue(codeQualifier, "code_1");
        query.getCondition().setResettableValue(attrOneQualifier, "attr_1");

        final SearchResult<SimpleTrainingItemModel> searchResult = genericSearchService.search(query);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("withResettableValues: " + items.size());
    }

    private void withSubQuery() {

        final GenericSearchField wrapCodeField = new GenericSearchField(WrappedOneModel._TYPECODE, WrappedOneModel.CODE);

        GenericQuery query = new GenericQuery(SimpleTrainingItemModel._TYPECODE,
                GenericCondition.equals(SimpleTrainingItemModel.CODE, "code_1"));

        GenericQuery subQuery = query.addSubQuery(SimpleTrainingItemModel.WRAPPEDONE, Operator.IN, WrappedOneModel._TYPECODE);
        subQuery.addCondition(GenericCondition.equals(wrapCodeField, "w_code_1"));

        final SearchResult<SimpleTrainingItemModel> searchResult = genericSearchService.search(query);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("withSubQuery: " + items.size());
    }
}
