package org.training.service.data.search;

import com.google.common.collect.ImmutableMap;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.jalo.flexiblesearch.FlexibleSearch;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import org.training.model.*;
import org.training.service.AbstractDemo;

import javax.annotation.Resource;
import java.util.*;

public class FlexibleSearchDemo extends AbstractDemo {

    @Resource
    private UserService userService;
    @Resource
    private SessionService sessionService;
    @Resource
    private FlexibleSearchService flexibleSearchService;
    @Resource
    private SearchRestrictionService searchRestrictionService;

    public void demo() {
        selectAll();
        whereCodeAndLocalizedCodeOrderByBuildDesc();
        joinWrappedOne();
        joinManyToMany();
        getTwoStringColumnsCountFiveAsAdmin();
        getMaxRandomNumber();
        withSubQuery();
        getConcreteType();
        cacheDefault();
        cacheTenMin();
        noCache();
        paging();
        restriction();
        ignoreRestrictionViaAdmin();
        ignoreRestrictionViaAdminInQuery();
        ignoreRestrictionViaService();
        toggleableRestrictionExample();
    }

    private void selectAll() {

        final String query = "SELECT {pk} FROM {" + SimpleTrainingItemModel._TYPECODE + "}";

        final SearchResult<SimpleTrainingItemModel> searchResult = flexibleSearchService.search(query);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("selectAll: " + items.size());
    }

    private void whereCodeAndLocalizedCodeOrderByBuildDesc() {

        final String query = "SELECT {pk} FROM {" + SimpleTrainingItemModel._TYPECODE + "}"
                + " WHERE {" + SimpleTrainingItemModel.CODE + "} like ?code"
                + " AND {" + SimpleTrainingItemModel.LOCALIZEDCODE + "[de]} like ?locCode"
                + " ORDER BY {" + SimpleTrainingItemModel.BUILDDATE + "} DESC";

        final Map<String, Object> params = new HashMap<>();
        params.put("code", "code_1");
        params.put("locCode", "de_value");

        final FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(query);
        fsQuery.addQueryParameters(params);

        final SearchResult<SimpleTrainingItemModel> searchResult = flexibleSearchService.search(fsQuery);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("whereCodeAndLocalizedCodeOrderByBuildDesc: " + items.size());
    }

    private void joinWrappedOne() {

        final String query = "SELECT {pk} FROM {" + SimpleTrainingItemModel._TYPECODE + " as it " +
                " JOIN " + WrappedOneModel._TYPECODE + " as wr on {it." + SimpleTrainingItemModel.WRAPPEDONE + "}"
                + " = {wr." + WrappedOneModel.PK + "}}"
                + " WHERE {wr." + WrappedOneModel.CODE + "} like 'code_1%'";

        final SearchResult<SimpleTrainingItemModel> searchResult = flexibleSearchService.search(query);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("joinWrappedOne: " + items.size());
    }

    private void joinManyToMany() {

        final String query = "SELECT {pk} FROM {" + SimpleTrainingItemModel._TYPECODE + " as it " +
                " JOIN LocalizedSimpleTrainingItem2ManyToManyExampleItemRelation as rel on {rel.source}"
                + " = {it." + SimpleTrainingItemModel.PK + "}"
                + " JOIN " + ManyToManyExampleItemModel._TYPECODE + " as m2m on {rel.target} = {m2m." + ManyToManyExampleItemModel.PK + "}"
                + "} WHERE {m2m." + ManyToManyExampleItemModel.CODE + "} like 'code%'";

        final SearchResult<SimpleTrainingItemModel> searchResult = flexibleSearchService.search(query);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("joinManyToMany: " + items.size());
    }

    private void getTwoStringColumnsCountFiveAsAdmin() {

        final String query = "SELECT {" + SimpleTrainingItemModel.CODE + "}, {" + SimpleTrainingItemModel.BUILDDATE + "}"
                + " FROM {" + SimpleTrainingItemModel._TYPECODE + "}";

        final FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(query);
        fsQuery.setResultClassList(Arrays.asList(String.class, String.class));
        fsQuery.setCount(5);
        fsQuery.setUser(userService.getUserForUID("admin"));

        final SearchResult<List<String>> searchResult = flexibleSearchService.search(fsQuery);
        final List<List<String>> items = searchResult.getResult();

        LOG.info("getTwoStringColumnsCountFiveAsAdmin: " + items);
    }

    private void getMaxRandomNumber() {

        final String query = "SELECT MAX({" + SimpleTrainingItemModel.RANDOMNUMBER + "}) FROM {" + SimpleTrainingItemModel._TYPECODE + "}";

        final FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(query);
        fsQuery.setResultClassList(Collections.singletonList(Integer.class));

        final SearchResult<Integer> searchResult = flexibleSearchService.search(fsQuery);
        final List<Integer> items = searchResult.getResult();

        LOG.info("getMaxRandomNumber: " + items.get(0));
    }

    private void withSubQuery() {

        final String query = "SELECT {" + SimpleTrainingItemModel.PK + "} "
                + " FROM {" + SimpleTrainingItemModel._TYPECODE + "}"
                + " WHERE {" + SimpleTrainingItemModel.WRAPPEDONE + "} IN ({{"
                + " SELECT {" + WrappedOneModel.PK + "} FROM {" + WrappedOneModel._TYPECODE + "}"
                + " WHERE {" + WrappedOneModel.CODE + "} = 'w_code_1' }})";

        final SearchResult<Integer> searchResult = flexibleSearchService.search(query);
        final List<Integer> items = searchResult.getResult();

        LOG.info("withSubQuery: " + items.size());
    }

    private void getConcreteType() {

        final String query = "SELECT {pk} FROM {" + SimpleTrainingItemModel._TYPECODE + " as it " +
                " JOIN " + ComposedTypeModel._TYPECODE + " as ct on {it." + SimpleTrainingItemModel.ITEMTYPE + "}"
                + " = {ct." + ComposedTypeModel.PK + "}}"
                + " WHERE {ct." + ComposedTypeModel.CODE + "} IN (?types)";

        final List<String> types = new ArrayList<>();
        types.add(SubSubTrainingItemModel._TYPECODE);
        types.add(SubSeparateTrainingItemModel._TYPECODE);

        final Map<String, Object> params = new HashMap<>();
        params.put("types", types);

        final FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(query);
        fsQuery.addQueryParameters(params);

        final SearchResult<SimpleTrainingItemModel> searchResult = flexibleSearchService.search(fsQuery);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("getConcreteType: " + items.size());
    }

    private void cacheDefault() {

        final String query = "SELECT {pk} FROM {" + SimpleTrainingItemModel._TYPECODE + "}"
                + " WHERE {" + SimpleTrainingItemModel.ATTRIBUTEONE + "} IS NULL"
                + " OR {" + SimpleTrainingItemModel.ATTRIBUTEONE + "} like 'cacheDefault'";

        final SearchResult<SimpleTrainingItemModel> searchResult = flexibleSearchService.search(query);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("cacheDefault: " + items.size());
    }

    private void cacheTenMin() {

        final String query = "SELECT {pk} FROM {" + SimpleTrainingItemModel._TYPECODE + "}"
                + " WHERE {" + SimpleTrainingItemModel.ATTRIBUTEONE + "} IS NULL"
                + " OR {" + SimpleTrainingItemModel.ATTRIBUTEONE + "} like 'cacheTenMin'";

        final int ttlInSeconds = 300;

        sessionService.executeInLocalViewWithParams(ImmutableMap.of(FlexibleSearch.CACHE_TTL, ttlInSeconds), new SessionExecutionBody() {
            @Override
            public Object execute() {
                final SearchResult<SimpleTrainingItemModel> searchResult = flexibleSearchService.search(query);
                final List<SimpleTrainingItemModel> items = searchResult.getResult();

                LOG.info("cacheTenMin: " + items.size());

                return items;
            }
        });
    }

    private void noCache() {

        final String query = "SELECT {pk} FROM {" + SimpleTrainingItemModel._TYPECODE + "}"
                + " WHERE {" + SimpleTrainingItemModel.ATTRIBUTEONE + "} IS NULL"
                + " OR {" + SimpleTrainingItemModel.ATTRIBUTEONE + "} like 'cacheNo'";

        final FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(query);
        fsQuery.setDisableCaching(true);

        final SearchResult<SimpleTrainingItemModel> searchResult = flexibleSearchService.search(fsQuery);
        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        LOG.info("noCache: " + items.size());
    }

    private void paging() {
        int pageNumber = new Random().nextInt(4) + 1;
        int pageSize = new Random().nextInt(10) + 1;
        paging(pageNumber, pageSize);
    }

    private void paging(int pageNumber, int pageSize) {

        final String query = "SELECT {pk} FROM {" + SimpleTrainingItemModel._TYPECODE + "}"
                + " WHERE {" + SimpleTrainingItemModel.RANDOMNUMBER + "} IS NOT NULL"
                + " ORDER BY {" + SimpleTrainingItemModel.RANDOMNUMBER + "} ASC";

        final FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(query);
        fsQuery.setStart((pageNumber - 1) * pageSize);
        fsQuery.setCount(pageSize);
        fsQuery.setNeedTotal(true);

        final SearchResult<SimpleTrainingItemModel> searchResult = flexibleSearchService.search(fsQuery);
        final int count = searchResult.getCount();
        final int totalCount = searchResult.getTotalCount();

        final List<SimpleTrainingItemModel> items = searchResult.getResult();

        for (SimpleTrainingItemModel item : items) {
            LOG.info("paging: " + item.getCode());
        }
        LOG.info("--------------------------------------");
        LOG.info("Count: " + count);
        LOG.info("Total count: " + totalCount);
        LOG.info("######################################");

    }

    final String GET_TRAINING_ITEMS = "SELECT {" + SimpleTrainingItemModel.PK + "} FROM {" + SimpleTrainingItemModel._TYPECODE + "}";

    private void restriction() {

        final String query = "SELECT {" + SimpleTrainingItemModel.PK + "} FROM {" + SimpleTrainingItemModel._TYPECODE + "}";

        final FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(query);
        fsQuery.setUser(userService.getUserForUID("restricted"));

        final SearchResult<List<String>> searchResult = flexibleSearchService.search(fsQuery);
        final List<List<String>> items = searchResult.getResult();

        LOG.info("restriction: " + items.size());

    }

    private void ignoreRestrictionViaAdmin() {

        sessionService.executeInLocalView(new SessionExecutionBody() {
            @Override
            public Object execute() {
                userService.setCurrentUser(userService.getAdminUser());
                final SearchResult<List<String>> searchResult
                        = flexibleSearchService.search(GET_TRAINING_ITEMS);


                final List<List<String>> items = searchResult.getResult();
                LOG.info("ignoreRestrictionViaAdmin: " + items.size());
                return searchResult;
            }
        });
    }

    private void ignoreRestrictionViaAdminInQuery() {

        final FlexibleSearchQuery fsQuery
                = new FlexibleSearchQuery(GET_TRAINING_ITEMS);
        fsQuery.setUser(userService.getUserForUID("admin"));

        final SearchResult<List<String>> searchResult = flexibleSearchService.search(fsQuery);
        final List<List<String>> items = searchResult.getResult();

        LOG.info("ignoreRestrictionViaAdminInQuery: " + items.size());
    }

    private void ignoreRestrictionViaService() {

        searchRestrictionService.disableSearchRestrictions();
        final SearchResult<List<String>> searchResult = flexibleSearchService.search(GET_TRAINING_ITEMS);
        searchRestrictionService.enableSearchRestrictions();

        final List<List<String>> items = searchResult.getResult();
        LOG.info("ignoreRestrictionViaService: " + items.size());
    }

    private void toggleableRestrictionExample() {
        userService.setCurrentUser(userService.getUserForUID("toggle_restricted"));
        toggleableEnabled();
        toggleableDisabled();
        toggleableMissed();
        userService.setCurrentUser(userService.getAdminUser());
    }

    private void toggleableEnabled() {

        sessionService.setAttribute("toggleable_restriction.disabled", 0);
        final SearchResult<List<String>> searchResult = flexibleSearchService.search(GET_TRAINING_ITEMS);

        final List<List<String>> items = searchResult.getResult();
        LOG.info("toggleableEnabled: " + items.size());
    }

    private void toggleableDisabled() {
        sessionService.setAttribute("toggleable_restriction.disabled", 1);
        final SearchResult<List<String>> searchResult = flexibleSearchService.search(GET_TRAINING_ITEMS);

        final List<List<String>> items = searchResult.getResult();
        LOG.info("toggleableDisabled: " + items.size());
    }

    private void toggleableMissed() {
        try {
            final SearchResult<List<String>> searchResult = flexibleSearchService.search(GET_TRAINING_ITEMS);

            final List<List<String>> items = searchResult.getResult();
            LOG.info("toggleableMissed: " + items.size());
        } catch (Exception e) {
            LOG.error("EXCPETION! toggleableMissed: ", e);
        }
    }
}
