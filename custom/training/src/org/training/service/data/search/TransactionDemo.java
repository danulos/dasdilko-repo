package org.training.service.data.search;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.tx.TransactionBody;
import org.springframework.transaction.annotation.Transactional;
import org.training.model.SimpleTrainingItemModel;
import org.training.service.AbstractDemo;
import org.training.util.TrainingUtil;

import javax.annotation.Resource;
import java.util.Random;

public class TransactionDemo extends AbstractDemo {

    @Resource
    private ModelService modelService;

    public void demo() {
//        transactionalSave();
//        simpleSave();
        xmlTransaction();
        annotationTransaction();
    }

    boolean commit = true;
//    boolean commit = new Random().nextBoolean();

    private void transactionalSave() {
        Transaction tx = Transaction.current();
        tx.begin();
        final SimpleTrainingItemModel item = modelService.create(SimpleTrainingItemModel.class);
        item.setCode("trax_" + TrainingUtil.getCode());
        modelService.save(item);
        LOG.info("---------------------------");
//        nested();
//        LOG.info("---------------------------");
        if (commit) {
            LOG.info("commit");
            tx.commit();
        } else {
            LOG.info("rollback");
            tx.rollback();
        }
    }

    private void simpleSave() {
        final SimpleTrainingItemModel item = modelService.create(SimpleTrainingItemModel.class);
        item.setCode("no_trax_" + TrainingUtil.getCode());
        modelService.save(item);
    }

    private void ones() {

        final Transaction tx = Transaction.current();
        tx.begin();
        try {
            doLogic();
            tx.commit();
        } finally {
            tx.rollback();
        }

    }

    private void twos() throws Exception {

        final Object txResult = Transaction.current().execute(
                new TransactionBody() {
                    public Object execute() {
                        return doLogic();
                    }
                }
        );

    }

    private boolean doLogic() {
        final SimpleTrainingItemModel item = modelService.create(SimpleTrainingItemModel.class);
        item.setCode(TrainingUtil.getCode());
        modelService.save(item);
        return true;
    }

    private void nested() {
        Transaction tx = Transaction.current();
        tx.begin();
        final SimpleTrainingItemModel item = modelService.create(SimpleTrainingItemModel.class);
        item.setCode("nested" + TrainingUtil.getCode());
        modelService.save(item);
        tx.commit();
        LOG.info("nested_commit");
    }

    public void xmlTransaction() {
        final SimpleTrainingItemModel item = modelService.create(SimpleTrainingItemModel.class);
        item.setCode("xmlTransaction_" + TrainingUtil.getCode());
        modelService.save(item);

    }

    @Transactional
    public void annotationTransaction() {
        final SimpleTrainingItemModel item = modelService.create(SimpleTrainingItemModel.class);
        item.setCode("annotationTransaction_" + TrainingUtil.getCode());
        modelService.save(item);

    }
}
