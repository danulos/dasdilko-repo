package org.training.service.data.search;

import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.util.Utilities;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.training.model.SimpleTrainingItemModel;
import org.training.service.AbstractDemo;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class NativeSqlDemo extends AbstractDemo {

    @Resource
    private TypeService typeService;

    @Resource
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public void demo() {
//        bulkUpdate(Arrays.asList("gru20", "gru21", "gru22", "gru23", "gru24"));
//        bulkRemove(Arrays.asList("gru27", "gru28", "gru29"));
//        update(Arrays.asList("gru21", "gru22", "gru23"));
    }

    public void one() {
        final List<PK> pks = Arrays.asList(PK.fromLong(8796421073660L), PK.fromLong(8796421106428L),
                PK.fromLong(8796421139196L), PK.fromLong(8796421171964L), PK.fromLong(8796421204732L));
        basic_example(pks);
    }

    private void basic_example(final List<PK> pks) {
        final List<String> pksAsString = pks.stream()
                .map(PK::toString).collect(Collectors.toList());
        final SqlParameterSource parameters = new MapSqlParameterSource("pks", pksAsString);

        int result = namedParameterJdbcTemplate.update(
                "UPDATE simpletrainingitem SET p_attributetwo = 'basic_example' WHERE PK IN (:pks)"
                , parameters);

        LOG.info("basic_example - " + result);
    }

    private void bulkRemove(final List<String> codes) {
        final SqlParameterSource parameters = new MapSqlParameterSource("codes", codes);

        int result = namedParameterJdbcTemplate.update("DELETE FROM simpletrainingitem WHERE PK IN (:codes)"
                , parameters);

        LOG.info("bulkRemove - " + result);
    }

    public void two() {
        final List<PK> pks = Arrays.asList(PK.fromLong(8796421106428L), PK.fromLong(8796421139196L), PK.fromLong(8796421171964L));
        typed_example(pks);
    }

    private void typed_example(final List<PK> pks) {
        final List<String> pksAsString = pks.stream().map(PK::toString).collect(Collectors.toList());
        final SqlParameterSource parameters = new MapSqlParameterSource("pks", pksAsString);

        final ComposedTypeModel composedType = typeService.getComposedTypeForClass(SimpleTrainingItemModel.class);
        final String tableName = composedType.getTable();
        final String pkColumn = typeService.getAttributeDescriptor(composedType, "pk").getDatabaseColumn();
        final String attributeTwoColumn = typeService.getAttributeDescriptor(composedType, "attributeTwo").getDatabaseColumn();

        String queryBuilder = "UPDATE " + tableName + " SET " + attributeTwoColumn + " = 'typed_example'"
                + " WHERE " + pkColumn + " IN (:pks)";
        int result = namedParameterJdbcTemplate.update(queryBuilder, parameters);

        pks.forEach(Utilities::invalidateCache);

        LOG.info("typed_example - " + result);
    }

}
