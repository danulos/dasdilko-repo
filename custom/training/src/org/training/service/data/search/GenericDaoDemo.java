package org.training.service.data.search;

import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.servicelayer.internal.dao.SortParameters;
import org.training.model.SimpleTrainingItemModel;
import org.training.service.AbstractDemo;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GenericDaoDemo extends AbstractDemo {

    @Resource
    private GenericDao<SimpleTrainingItemModel> simpleTrainingItemGenericDao;
    @Resource
    private GenericDao<SimpleTrainingItemModel> defaultSimpleTrainingItemGenericDao;

    public void demo() {
        LOG.info("[c] find(): " + defaultSimpleTrainingItemGenericDao.find().size());

        final List<SimpleTrainingItemModel> allItems = simpleTrainingItemGenericDao.find();

        final SortParameters sortParameters = new SortParameters();
        sortParameters.addSortParameter(SimpleTrainingItemModel.BUILDDATE, SortParameters.SortOrder.ASCENDING);
        final List<SimpleTrainingItemModel> allItemsSorted = simpleTrainingItemGenericDao.find(sortParameters);


        final Map<String, String> params = new HashMap<>();
        params.put(SimpleTrainingItemModel.CODE, "code");
        final List<SimpleTrainingItemModel> filteredItems = simpleTrainingItemGenericDao.find(params);
        final List<SimpleTrainingItemModel> filteredItemsSorted = simpleTrainingItemGenericDao.find(params, sortParameters);
        final List<SimpleTrainingItemModel> filteredItemsSortedLimit = simpleTrainingItemGenericDao.find(params, sortParameters, 2);


        print(allItems);
        print(allItemsSorted);
        print(filteredItems);
        print(filteredItemsSorted);
        print(filteredItemsSortedLimit);

        LOG.info("find(): " + allItems.size());
        LOG.info("find(sortParameters): " + allItemsSorted.size());
        LOG.info("find(params): " + filteredItems.size());
        LOG.info("find(params, sortParameters): " + filteredItemsSorted.size());
        LOG.info("find(params, sortParameters, 5): " + filteredItemsSortedLimit.size());
    }

    private void print(final List<SimpleTrainingItemModel> items) {
        LOG.info("##########################################################");
        items.forEach(item -> LOG.info(item.getCode()));
        LOG.info("##########################################################");
    }
}
