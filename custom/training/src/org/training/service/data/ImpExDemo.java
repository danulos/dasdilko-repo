package org.training.service.data;

import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.ImpExMedia;
import de.hybris.platform.impex.jalo.exp.ExportConfiguration;
import de.hybris.platform.impex.model.ImpExMediaModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.exceptions.ModelInitializationException;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.impex.*;
import de.hybris.platform.servicelayer.impex.impl.ClasspathImpExResource;
import de.hybris.platform.servicelayer.impex.impl.FileBasedImpExResource;
import de.hybris.platform.servicelayer.impex.impl.MediaBasedImpExResource;
import de.hybris.platform.servicelayer.impex.impl.StreamBasedImpExResource;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.training.model.SimpleTrainingItemModel;
import org.training.service.AbstractDemo;
import org.training.util.TrainingUtil;

import javax.annotation.Resource;
import java.io.*;
import java.util.*;

import static org.apache.commons.lang.CharEncoding.UTF_8;

public class ImpExDemo extends AbstractDemo {

    @Resource
    private ModelService modelService;
    @Resource
    private ImportService importService;
    @Resource
    private ExportService exportService;
    @Resource
    private MediaService mediaService;

    public ImpExMedia getImpExMedia(final String code) {
        ImpExMediaModel example = new ImpExMediaModel();
        example.setCode(code);
        example = modelService.getByExample(example);
        return modelService.getSource(example);
    }

    public Collection<Item> getItemsForExport() {
        SimpleTrainingItemModel item = new SimpleTrainingItemModel();
        item.setCode("temp_" + new Date().getTime());
        modelService.save(item);
        return Collections.singletonList(modelService.getSource(item));
    }

    int max = 35;

    public void generate() throws ImpExException {
        List<SimpleTrainingItemModel> save = new ArrayList<>();
        for (int i = 0; i < max; i++) {
            SimpleTrainingItemModel item = new SimpleTrainingItemModel();
            item.setCode("temp_" + i + "_" + new Date().getTime());
            item.setRandomNumber(i);
            save.add(item);
        }
        modelService.saveAll(save);
    }

    public void demo() throws IOException {
        streamBased();
        fileBased();
        classpathBased();
        mediaBased();
    }

    private void impexExamples() throws IOException {

        final String impex = "INSERT_UPDATE SimpleTrainingItem;code[unique=true];\n ;streamBased";
        final InputStream inputStream = new ByteArrayInputStream(impex.getBytes());
        final ImpExResource streamBasedResource = new StreamBasedImpExResource(inputStream, UTF_8);

        final File file = getImpExFile();
        final ImpExResource fileBasedResource = new FileBasedImpExResource(file, UTF_8);

        final String filePath = "/import/impex/training.impex";
        final ImpExResource classPathResource = new ClasspathImpExResource(filePath, UTF_8);

        final ImpExMediaModel impexMedia = getImpExMedia();
        final ImpExResource mediaBasedResource = new MediaBasedImpExResource(impexMedia);

        final ImpExResource impExResource = getImpExResource();
        final ImportResult importResult = this.importService.importData(impExResource);
        final ExportResult exportResult = this.exportService.exportData(impExResource);

        System.out.println(streamBasedResource.toString());
        System.out.println(fileBasedResource.toString());
        System.out.println(classPathResource.toString());
        System.out.println(mediaBasedResource.toString());
        System.out.println(importResult.isFinished());
        System.out.println(exportResult.isFinished());
    }

    private void impexExamplesWrapped() throws IOException {

        final ImpExResource impExResource = getImpExResource();
        final ImportConfig importConfig = new ImportConfig();

        importConfig.setScript(impExResource);
        importConfig.setEnableCodeExecution(true);
        importConfig.setMaxThreads(1);
        importConfig.setDumpingEnabled(false);
        importConfig.setLegacyMode(true);
        importConfig.setValidationMode(ImportConfig.ValidationMode.RELAXED);

        final ImportResult importResult = this.importService.importData(importConfig);

        System.out.println(importResult.isFinished());
    }

    private ImpExResource getImpExResource() {
        return new StreamBasedImpExResource(null, UTF_8);
    }

    private void streamBased() {

        final String impex = "INSERT_UPDATE " + SimpleTrainingItemModel._TYPECODE +
                ";code[unique=true];\n ;streamBased";

        final InputStream inputStream = new ByteArrayInputStream(impex.getBytes());
        final ImpExResource resource = new StreamBasedImpExResource(inputStream, UTF_8);
        final ImportResult importResult = this.importService.importData(resource);

        TrainingUtil.printMessage("streamBased - " + importResult.isSuccessful());
    }

    private void fileBased() throws IOException {

        final File file = getImpExFile();

        final ImportConfig importConfig = new ImportConfig();
        importConfig.setScript(new FileBasedImpExResource(file, UTF_8));
        final ImportResult importResult = this.importService.importData(importConfig);

        TrainingUtil.printMessage("fileBased - " + importResult.isSuccessful());
    }

    private void classpathBased() {

        final String filePath = "/import/impex/training.impex";

        final ImportConfig importConfig = new ImportConfig();
        importConfig.setScript(new ClasspathImpExResource(filePath, UTF_8));
        final ImportResult importResult = this.importService.importData(importConfig);

        TrainingUtil.printMessage("classpathBased - " + importResult.isSuccessful());
    }

    private void mediaBased() {

        final ImpExMediaModel impexMedia = getImpExMedia();

        final ImpExResource resource = new MediaBasedImpExResource(impexMedia);
        final ImportResult importResult = this.importService.importData(resource);
        TrainingUtil.printMessage("mediaBased - " + importResult.isSuccessful());

    }

    private File getImpExFile() throws IOException {
        final String data = "INSERT_UPDATE " + SimpleTrainingItemModel._TYPECODE +
                ";code[unique=true];\n ;fileBased";
        final File testFile = File.createTempFile("test", "test");
        final PrintWriter writer = new PrintWriter(testFile);
        writer.write(data);
        writer.close();
        return testFile;
    }

    private ImpExMediaModel getImpExMedia() {
        ImpExMediaModel result;
        ImpExMediaModel example = new ImpExMediaModel();
        example.setCode("impex_media");
        try {
            result = modelService.getByExample(example);
        } catch (Exception e) {
            result = modelService.create(ImpExMediaModel._TYPECODE);
            modelService.initDefaults(result);
            modelService.save(result);
            final String data = "INSERT_UPDATE " + SimpleTrainingItemModel._TYPECODE +
                    ";code[unique=true];\n ;mediaBased";
            mediaService.setDataForMedia(result, data.getBytes());
            modelService.save(result);
        }

        return result;
    }
}
