package org.training.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractDemo {
    protected static final Logger LOG = LoggerFactory.getLogger(AbstractDemo.class);
}
