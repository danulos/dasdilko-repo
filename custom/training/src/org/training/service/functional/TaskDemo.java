package org.training.service.functional;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.processing.distributed.DistributedProcessService;
import de.hybris.platform.processing.distributed.simple.data.QueryBasedCreationData;
import de.hybris.platform.processing.model.DistributedProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.TaskConditionModel;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.TaskService;
import org.training.model.SimpleTrainingItemModel;
import org.training.process.task.context.TrainingTaskContext;
import org.training.service.AbstractDemo;
import org.training.task.model.TrainingTaskModel;
import org.training.util.TrainingUtil;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class TaskDemo extends AbstractDemo {

    @Resource
    private ModelService modelService;
    @Resource
    private TaskService taskService;
    @Resource
    private DistributedProcessService distributedProcessService;

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public void demo() {
//        scheduleTaskNow();
//        scheduleTaskByEvent();
//        scheduleTaskExpireNow();
//        scheduleTaskExpireByConditions();
//        scheduleTaskWithContext();
//        scheduleRollbackTask();
//        scheduleCustomTask();
//        retryTenTask();
        scheduleDistributedTask();
    }

    private void scheduleTaskNow() {
        final TaskModel task = modelService.create(TaskModel.class);
        task.setRunnerBean("trainingTask");
        task.setExecutionDate(new Date());
        taskService.scheduleTask(task);
    }

    private void scheduleTaskByEvent() {
        final TaskModel task = modelService.create(TaskModel.class);
        task.setRunnerBean("trainingTask");
        final TaskConditionModel condition = modelService.create(TaskConditionModel.class);
        condition.setUniqueID("TrainingTaskCondition");
        task.setConditions(Collections.singleton(condition));
        taskService.scheduleTask(task);


        List<ItemModel> items = new ArrayList<>();
        items.add(new SimpleTrainingItemModel());
        items.add(new ProductModel());
        items.add(new CategoryModel());
        items.add(new UserModel());

        modelService.saveAll(items);

    }

    private void scheduleTaskExpireNow() {
        try {
            final TaskModel task = modelService.create(TaskModel.class);
            task.setRunnerBean("trainingTask");
            task.setExpirationDate(simpleDateFormat.parse("2044-04-04 00:00:00"));
            final long expirationTimeMillis = new Date().getTime() + 100L;
            task.setExpirationTimeMillis(expirationTimeMillis);
            taskService.scheduleTask(task);
        } catch (ParseException e) {
            LOG.error(e.getMessage());
        }
    }

    private void scheduleTaskExpireByConditions() throws ParseException {
        final TaskModel task = modelService.create(TaskModel.class);
        task.setRunnerBean("trainingTask");
        final TaskConditionModel conditionOne = modelService.create(TaskConditionModel.class);
        conditionOne.setUniqueID("conditionOne");
        conditionOne.setExpirationDate(simpleDateFormat.parse("2044-04-04 00:00:00"));
        final TaskConditionModel conditionTwo = modelService.create(TaskConditionModel.class);
        conditionTwo.setUniqueID("conditionTwo");
        conditionOne.setExpirationTimeMillis(new Date().getTime() + 30_000L);
        task.setConditions(Set.of(conditionOne, conditionTwo));
        taskService.scheduleTask(task);
    }

    private void scheduleTaskWithContext() {
        final TaskModel task = modelService.create(TaskModel.class);
        task.setRunnerBean("trainingTask");
        task.setExecutionDate(new Date());
        task.setContext(getContext());
        taskService.scheduleTask(task);
    }

    private TrainingTaskContext getContext() {
        final TrainingTaskContext taskContext = new TrainingTaskContext();
        taskContext.setCode(TrainingUtil.getCode());
        taskContext.setNumber(new Random().nextInt(100));
        return taskContext;
    }

    private void scheduleRollbackTask() {
        final TaskModel task = modelService.create(TaskModel.class);
        task.setRunnerBean("rollbackTask");
        task.setExecutionDate(new Date());
        task.setContext(getContext());
        taskService.scheduleTask(task);
    }

    private void scheduleCustomTask() {
        final TrainingTaskModel task = modelService.create(TrainingTaskModel.class);
        task.setRunnerBean("customTrainingTask");
        task.setExecutionDate(new Date());
        task.setCode(TrainingUtil.getCode());
        task.setNumber(new Random().nextInt(100));
        taskService.scheduleTask(task);
    }

    private void retryTenTask() {
        final TaskModel task = modelService.create(TaskModel.class);
        task.setRunnerBean("retryLaterTask");
        task.setExecutionDate(new Date());
        taskService.scheduleTask(task);
    }

    boolean create = true;

    private void scheduleDistributedTask() {
        if (create) {
//            createItems();
            create = false;
        }

        final QueryBasedCreationData processData = QueryBasedCreationData.builder()
                .withQuery("SELECT {PK} FROM {SimpleTrainingItem} WHERE {randomNumber} > ?rNmb")
                .withQueryParams(Collections.singletonMap("rNmb", 0))
                .withHandlerId("trainingProcessing")
                .withBatchSize(30)
                .useDatabasePaging()
                .build();

        final DistributedProcessModel process = distributedProcessService.create(processData);
        distributedProcessService.start(process.getCode());
    }

    private void createItems() {
        for (int i = 0; i < 10_000; i++) {
            final List<SimpleTrainingItemModel> saveList = new ArrayList<>();
            for (int j = 0; j < 1_000; j++, i++) {
                final SimpleTrainingItemModel item = modelService.create(SimpleTrainingItemModel.class);
                item.setCode(TrainingUtil.getCode() + "_" + i + "_" + j);
                item.setRandomNumber(i);
                saveList.add(item);
            }
            modelService.saveAll(saveList);
            LOG.info("Batch #{} saved", (i + 1));
        }
    }

    public void taskEvent() {
        taskService.triggerEvent("TrainingTaskCondition");
    }

    public void taskEventOne() {
        taskService.triggerEvent("conditionOne");
    }

    public void taskEventTwo() {
        taskService.triggerEvent("conditionTwo");
    }
}
