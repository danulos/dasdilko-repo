package org.training.service.functional;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.link.LinkModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.link.Link;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.WorkflowService;
import de.hybris.platform.workflow.constants.WorkflowConstants;
import de.hybris.platform.workflow.enums.WorkflowActionStatus;
import de.hybris.platform.workflow.enums.WorkflowActionType;
import de.hybris.platform.workflow.model.AbstractWorkflowActionModel;
import de.hybris.platform.workflow.model.AbstractWorkflowDecisionModel;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowActionTemplateModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionTemplateModel;
import de.hybris.platform.workflow.model.WorkflowModel;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;
import org.training.service.AbstractDemo;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.Assert.assertNotNull;
import static org.assertj.core.api.Assertions.assertThat;

public class WorkflowDemo extends AbstractDemo {

    @Resource
    private ModelService modelService;
    @Resource
    private UserService userService;
    @Resource
    private WorkflowProcessingService workflowProcessingService;
    @Resource
    private WorkflowService newestWorkflowService;
    @Resource
    private FlexibleSearchService flexibleSearchService;

    public void proceed() {

        userService.setCurrentUser(userService.getUserForUID("workflowUser"));
        final WorkflowActionModel action = getFirstPendingAction("workflowUser");
        workflowProcessingService.decideAction(action, action.getDecisions().iterator().next());

    }

    public void demo() {
        simpleWorkflow();
    }

    private WorkflowActionModel getFirstPendingAction(final String uid) {
        final String query = "SELECT {pk} FROM {WorkFlowAction} WHERE {status}=?status AND {principalAssigned}=?user";

        final Map params = new HashMap();
        params.put("status", WorkflowActionStatus.IN_PROGRESS);
        params.put("user", userService.getUserForUID(uid));

        final List<WorkflowActionModel> actions = flexibleSearchService.search(query, params).getResult();
        return actions.get(0);
    }

    private void simpleWorkflow() {
        WorkflowTemplateModel testTemplate = createWorkflowTemplate();

        WorkflowModel testWorkflow = newestWorkflowService.createWorkflow(testTemplate, userService.getAdminUser());
        workflowProcessingService.toggleActions(testWorkflow);
        modelService.saveAll(testTemplate, testWorkflow);

        workflowProcessingService.startWorkflow(testWorkflow);
    }

    private WorkflowTemplateModel createWorkflowTemplate() {
        // de.hybris.platform.workflow.integration.WorkflowIterationTest.createWorkflowTemplate

        final UserModel admin = userService.getAdminUser();
        final UserModel workflowUser = userService.getUserForUID("workflowUser");

        final WorkflowTemplateModel template = createWorkflowTemplate(admin, "Training Template", "Training Template");

        final WorkflowActionTemplateModel startAction = createAction(admin, "Start action", WorkflowActionType.START, template);
        final WorkflowActionTemplateModel proceedAction = createAction(workflowUser, "Proceed action", WorkflowActionType.NORMAL, template);
        final WorkflowActionTemplateModel validateAction = createAction(admin, "Validate action", WorkflowActionType.NORMAL, template);
        final WorkflowActionTemplateModel finishAction = createAction(admin, "Finish action", WorkflowActionType.END, template);

        final WorkflowDecisionTemplateModel simpleDecision = createDecision("Simple decision", startAction);
        final WorkflowDecisionTemplateModel proceedDecision = createDecision("Proceed decision", proceedAction);
        final WorkflowDecisionTemplateModel acceptDecision = createDecision("Accept decision", validateAction);
        final WorkflowDecisionTemplateModel denyDecision = createDecision("Deny decision", validateAction);

        createWorkflowActionTemplateModelLinkTemplateRelation(simpleDecision, proceedAction, Boolean.FALSE);
        createWorkflowActionTemplateModelLinkTemplateRelation(proceedDecision, validateAction, Boolean.FALSE);
        createWorkflowActionTemplateModelLinkTemplateRelation(denyDecision, proceedAction, Boolean.FALSE);
        createWorkflowActionTemplateModelLinkTemplateRelation(acceptDecision, finishAction, Boolean.FALSE);

        WorkflowModel testWorkflow = newestWorkflowService.createWorkflow(template, userService.getAdminUser());
        workflowProcessingService.toggleActions(testWorkflow);
        modelService.saveAll(template, testWorkflow);
        workflowProcessingService.startWorkflow(testWorkflow);

        return template;
    }

    private WorkflowTemplateModel createWorkflowTemplate(final UserModel owner, final String code, final String desc) {
        final WorkflowTemplateModel template = modelService.create(WorkflowTemplateModel.class);
        template.setOwner(owner);
        template.setCode(code);
        template.setDescription(desc);

        modelService.save(template);

        assertNotNull("Template should not be null", template);
        assertThat(modelService.isUpToDate(template)).isTrue();
        return template;
    }

    private WorkflowActionTemplateModel createAction(final UserModel user, final String code,
                                                     final WorkflowActionType actionType,
                                                     final WorkflowTemplateModel workflow) {
        final WorkflowActionTemplateModel action = modelService.create(WorkflowActionTemplateModel.class);
        action.setPrincipalAssigned(user);
        action.setWorkflow(workflow);
        action.setCode(code);
        action.setSendEmail(Boolean.FALSE);
        action.setActionType(actionType);
        modelService.save(action);

        final List<WorkflowActionTemplateModel> coll = new ArrayList(workflow.getActions());
        coll.add(action);
        workflow.setActions(coll);
        modelService.save(workflow);
        return action;
    }

    private WorkflowDecisionTemplateModel createDecision(final String code,
                                                         final WorkflowActionTemplateModel actionTemplate) {
        final WorkflowDecisionTemplateModel decision = modelService.create(WorkflowDecisionTemplateModel.class);
        decision.setCode(code);
        decision.setActionTemplate(actionTemplate);
        modelService.save(decision);

        final Collection<WorkflowDecisionTemplateModel> coll = new ArrayList(actionTemplate.getDecisionTemplates());
        coll.add(decision);
        actionTemplate.setDecisionTemplates(coll);
        modelService.save(actionTemplate);
        return decision;
    }

    private void createWorkflowActionTemplateModelLinkTemplateRelation(final WorkflowDecisionTemplateModel decisionTemplate,
                                                                       final WorkflowActionTemplateModel toAction,
                                                                       final Boolean hasAndConnection) {
        final List<WorkflowActionTemplateModel> toTemplateActions = new ArrayList<WorkflowActionTemplateModel>(
                decisionTemplate.getToTemplateActions());
        toTemplateActions.add(toAction);
        decisionTemplate.setToTemplateActions(toTemplateActions);
        modelService.save(decisionTemplate);

        final Collection<LinkModel> incomingLinkList = getLinkTemplates(decisionTemplate, toAction);

        for (final LinkModel link : incomingLinkList) {
            setAndConnectionTemplate(link, hasAndConnection);
        }
    }

    private Collection<LinkModel> getLinkTemplates(final AbstractWorkflowDecisionModel decision,
                                                   final AbstractWorkflowActionModel action) {
        SearchResult<LinkModel> results;
        final Map params = new HashMap();
        params.put("desc", decision);
        params.put("act", action);

        if (decision == null && action == null) {
            throw new NullPointerException("Decision and action cannot both be null");
        } else if (action == null) {
            results = flexibleSearchService.search("SELECT {" + ItemModel.PK + "} from {"
                            + "WorkflowActionTemplateLinkTemplateRelation" + "} where {" + LinkModel.SOURCE + "}=?desc",
                    params);
        } else if (decision == null) {
            results = flexibleSearchService.search("SELECT {" + ItemModel.PK + "} from {"
                            + "WorkflowActionTemplateLinkTemplateRelation" + "} where {" + LinkModel.TARGET + "}=?act",
                    params);
        } else {
            results = flexibleSearchService.search("SELECT {" + ItemModel.PK + "} from {"
                    + "WorkflowActionTemplateLinkTemplateRelation" + "} where {" + LinkModel.SOURCE + "}=?desc AND {"
                    + LinkModel.TARGET + "}=?act", params);
            if (results.getTotalCount() != 1) {
                // if source and target are specified only an Item can be returned.
                LOG.error("There is more than one WorkflowActionTemplateLinkTemplateRelation for source '" + decision.getCode()
                        + "' and target '" + action.getCode() + "'");
            }
        }
        return results.getResult();
    }

    private void setAndConnectionTemplate(final LinkModel item, final Boolean value) {
        final Link itemSource = modelService.getSource(item);
        itemSource
                .setProperty(WorkflowConstants.Attributes.WorkflowActionTemplateLinkTemplateRelation.ANDCONNECTIONTEMPLATE,
                        value);
    }
}
