package org.training.service.functional;

import de.hybris.platform.processengine.BusinessProcessEvent;
import de.hybris.platform.processengine.BusinessProcessService;
import org.training.processing.model.TrainingProcessModel;
import org.training.service.AbstractDemo;
import org.training.util.TrainingUtil;

import javax.annotation.Resource;

public class ProcessDemo extends AbstractDemo {

    private final String TRAINING_PROCESS = "training-process";

    @Resource
    private BusinessProcessService businessProcessService;

    public void demo() {
        createProcess();
    }

    private void createProcess() {
        final String processCode = TRAINING_PROCESS + "_" + TrainingUtil.getCode();
        final TrainingProcessModel process = businessProcessService
                .createProcess(processCode, TRAINING_PROCESS);
        process.setTrainingProcessAttribute(TrainingUtil.getCode());
        businessProcessService.startProcess(process);
        LOG.info("createProcess - {}", processCode);
    }

    public void event(final String code) {
        final String processCode = code + "_TrainingEvent";
        businessProcessService.triggerEvent(processCode);
        LOG.info("triggerEvent - {}", processCode);
    }

    public void choice(final String option) {
        final BusinessProcessEvent event = BusinessProcessEvent
                .builder("TrainingEventWithChoice")
                .withChoice(option).build();
        businessProcessService.triggerEvent(event);
        LOG.info("triggerEvent - {}, {};", event.getEvent(), event.getChoice());
    }
}
