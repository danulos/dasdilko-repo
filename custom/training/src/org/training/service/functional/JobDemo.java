package org.training.service.functional;

import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.cronjob.model.TriggerModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.internal.model.ServicelayerJobModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.training.job.model.TrainingCronJobModel;
import org.training.service.AbstractDemo;
import org.training.util.TrainingUtil;

import javax.annotation.Resource;

public class JobDemo extends AbstractDemo {

    @Resource
    private ModelService modelService;
    @Resource
    private CronJobService cronJobService;

    public void demo() {
        createAndRunJob();
    }

    private void createAndRunJob() {
        final ServicelayerJobModel job = modelService.create(ServicelayerJobModel.class);
        job.setSpringId("trainingJobPerformable");
        job.setCode("trainingJobPerformable");
        modelService.save(job);

        final TrainingCronJobModel cronJob = modelService.create(TrainingCronJobModel.class);
        cronJob.setActive(Boolean.TRUE);
        cronJob.setJob(job);
        cronJob.setCode(TrainingUtil.getCode());
        cronJob.setNodeGroup("background");
        modelService.save(cronJob);

        final TriggerModel triggerModel = modelService.create(TriggerModel.class);
        triggerModel.setActive(Boolean.TRUE);
        triggerModel.setMinute(20);
        triggerModel.setCronJob(cronJob);
        modelService.save(triggerModel);
    }

    public void abort() {
        cronJobService.requestAbortCronJob(getJob("trainingCronJob"));
    }

    private CronJobModel getJob(final String code) {
        CronJobModel example = new CronJobModel();
        example.setCode(code);
        return modelService.getByExample(example);
    }
}
