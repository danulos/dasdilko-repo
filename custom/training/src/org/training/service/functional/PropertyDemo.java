package org.training.service.functional;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.training.properties.profile.Profiled;
import org.training.service.AbstractDemo;

import javax.annotation.Resource;

public class PropertyDemo extends AbstractDemo {

    private String configuredVariable;

    @Resource
    private ConfigurationService configurationService;
    @Resource
    private Profiled profiled;

    public void demo() {
        LOG.info("configuredVariable - {}", configuredVariable);

        final String someSettingWithoutDot
                = configurationService.getConfiguration().getString("someSettingWithoutDot");
        LOG.info("someSettingWithoutDot - {}", someSettingWithoutDot);

        final int notExisting
                = configurationService.getConfiguration().getInt("not.existing", 123);
        LOG.info("notExisting - {}", notExisting);

        configurationService.getConfiguration()
                .setProperty("property.key", "property.value");
        profiled.doAction();
    }

    public void setConfiguredVariable(final String configuredVariable) {
        this.configuredVariable = configuredVariable;
    }
}
