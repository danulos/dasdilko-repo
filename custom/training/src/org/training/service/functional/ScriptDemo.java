package org.training.service.functional;

import de.hybris.platform.scripting.engine.ScriptExecutable;
import de.hybris.platform.scripting.engine.ScriptExecutionResult;
import de.hybris.platform.scripting.engine.ScriptingLanguagesService;
import de.hybris.platform.scripting.engine.content.ScriptContent;
import de.hybris.platform.scripting.engine.content.impl.ModelScriptContent;
import de.hybris.platform.scripting.engine.content.impl.ResourceScriptContent;
import de.hybris.platform.scripting.engine.content.impl.SimpleScriptContent;
import de.hybris.platform.scripting.enums.ScriptType;
import de.hybris.platform.scripting.events.ScriptingEventService;
import de.hybris.platform.scripting.model.ScriptModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.TaskService;
import de.hybris.platform.task.model.ScriptingTaskModel;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.UrlResource;
import org.training.process.task.context.TrainingTaskContext;
import org.training.script.ScriptResult;
import org.training.service.AbstractDemo;
import org.training.util.TrainingUtil;

import javax.annotation.Resource;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

public class ScriptDemo extends AbstractDemo {

    @Resource
    private ScriptingLanguagesService scriptingLanguagesService;
    @Resource
    private ModelService modelService;
    @Resource
    private CronJobService cronJobService;
    @Resource
    private ScriptingEventService scriptingEventService;
    @Resource
    private TaskService taskService;

    private final String SCRIPT = "" +
            "import org.training.model.SimpleTrainingItemModel\n" +
            "\n" +
            "List<SimpleTrainingItemModel> items = flexibleSearchService\n" +
            "        .search(\"SELECT {pk} FROM {SimpleTrainingItem} WHERE {randomNumber} > 10000\").getResult()\n" +
            "println(\"Results found - \" + items.size())\n" +
            "for (SimpleTrainingItemModel item : items) {\n" +
            "    item.setAttributeOne(\"someValue\")\n" +
            "}\n" +
            "modelService.saveAll(new ArrayList(items))";

    private final String SCRIPT_PARAM = "" +
            "import org.training.model.SimpleTrainingItemModel\n" +

            "import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;\n" +
            "\n" +
            "query = new FlexibleSearchQuery(\"SELECT {pk} FROM {SimpleTrainingItem} WHERE {randomNumber} > ?randomNumber\")\n" +
            "query.addQueryParameter(\"randomNumber\", randomNumber)\n" +
            "\n" +

            "List<SimpleTrainingItemModel> items = flexibleSearchService.search(query).getResult()\n" +
            "println(\"Results found - \" + items.size())\n" +
            "for (SimpleTrainingItemModel item : items) {\n" +
            "    item.setAttributeOne(\"someValue\")\n" +
            "}\n" +
            "modelService.saveAll(new ArrayList(items))";

    private final String SCRIPT_IMPL = "" +
            "import org.training.script.ScriptResult\n" +
            "\n" +
            "class ScriptClass implements ScriptResult {\n" +
            "    String property = \"\"\n" +
            "    \n" +
            "    def void doAction(String input) {\n" +
            "        property = property + \"_\" + input + \";\"\n" +
            "    }\n" +
            "}\n" +
            "\n" +
            "new ScriptClass()";

    private final String BAD_SCRIPT = "" +
            "List<String> list = new ArrayList<>()\n" +
            "println(list.get(2))";

    private boolean register = true;

    public void demo() {
//        scriptFromString();
//        scriptFromFileOnPc();
//        scriptFromClasspath();
//        scriptFromWeb();
//        scriptFromDb();
//        repositories();
//        scriptImpl();
//        scriptWithParam();
//        version();
//        runScriptJob();
        if (register) {
            registerScriptedEventListener();
        } else {
            unregisterScriptedEventListener();
        }
        register = !register;
        startScriptedTask();
    }

    private void scriptFromString() {
        final ScriptContent scriptContent = new SimpleScriptContent("groovy", SCRIPT);
        final ScriptExecutable executable = scriptingLanguagesService.getExecutableByContent(scriptContent);

        final ScriptExecutionResult result = executable.execute();
        System.out.println("scriptFromString - " + result.getOutputWriter().toString());
    }

    private void scriptFromFileOnPc() {
        final org.springframework.core.io.Resource resource
                = new FileSystemResource("/Work/hybris/script/simpleScript.groovy");
        final ScriptContent scriptContent = new ResourceScriptContent(resource);
        final ScriptExecutable executable = scriptingLanguagesService.getExecutableByContent(scriptContent);

        final ScriptExecutionResult result = executable.execute();
        System.out.println("scriptFromFileOnPc - " + result.getOutputWriter().toString());
    }

    private void scriptFromClasspath() {
        final org.springframework.core.io.Resource resource
                = new ClassPathResource("script/simpleScript.groovy");
        final ScriptContent scriptContent = new ResourceScriptContent(resource);
        final ScriptExecutable executable = scriptingLanguagesService.getExecutableByContent(scriptContent);

        final ScriptExecutionResult result = executable.execute();
        System.out.println("scriptFromClasspath - " + result.getOutputWriter().toString());
    }

    private void scriptFromWeb() {
        try {
            String path = "https://bitbucket.org/danulos/openrepo/raw/92a9e998a5031688477ee616c62895593375a4c7/simpleScript.groovy";
            final org.springframework.core.io.Resource resource
                    = new UrlResource(path);

            final ScriptContent scriptContent = new ResourceScriptContent(resource);
            final ScriptExecutable executable = scriptingLanguagesService.getExecutableByContent(scriptContent);

            final ScriptExecutionResult result = executable.execute();
            System.out.println("scriptFromWeb - " + result.getOutputWriter().toString());
        } catch (MalformedURLException e) {

        }
    }

    private void scriptFromDb() {
        final ScriptModel script = getScript("trainingScript");
        final ScriptContent scriptContent = new ModelScriptContent(script);
        final ScriptExecutable executable = scriptingLanguagesService.getExecutableByContent(scriptContent);
        final ScriptExecutionResult result = executable.execute();

        System.out.println("scriptFromDb - " + result.getOutputWriter().toString());
    }

    public void scriptFromDb_bad_no_disable() {
        final ScriptModel script = modelService.create(ScriptModel.class);
        script.setScriptType(ScriptType.GROOVY);
        script.setContent(BAD_SCRIPT);
        script.setCode("scriptFromDb_bad_no_disable" + TrainingUtil.getCode());
        script.setDisabled(false);
        script.setAutodisabling(false);
        modelService.save(script);

        final ScriptContent scriptContent = new ModelScriptContent(script);
        final ScriptExecutable executable = scriptingLanguagesService.getExecutableByContent(scriptContent);
        final ScriptExecutionResult result = executable.execute();

        System.out.println("scriptFromDb - " + result.getOutputWriter().toString());
    }

    public void scriptFromDb_bad_auto_disable() {
        final ScriptModel script = modelService.create(ScriptModel.class);
        script.setScriptType(ScriptType.GROOVY);
        script.setContent(BAD_SCRIPT);
        script.setCode("scriptFromDb_bad_auto_disable" + TrainingUtil.getCode());
        script.setDisabled(false);
        script.setAutodisabling(true);
        modelService.save(script);

        final ScriptContent scriptContent = new ModelScriptContent(script);
        final ScriptExecutable executable = scriptingLanguagesService.getExecutableByContent(scriptContent);
        final ScriptExecutionResult result = executable.execute();

        System.out.println("scriptFromDb - " + result.getOutputWriter().toString());
    }

    private ScriptModel getScript(final String code) {
        ScriptModel example = new ScriptModel();
        example.setCode(code);
        try {
            example = modelService.getByExample(example);
        } catch (Exception e) {
            example.setScriptType(ScriptType.GROOVY);
            example.setContent(SCRIPT);
            modelService.save(example);
        }
        return example;
    }

    private void repositories() {
        modelRepo();
        fileRepo();
        classPathRepo();
        urlRepo();
    }

    private void modelRepo() {
        final ScriptExecutable executable = scriptingLanguagesService
                .getExecutableByURI("model://trainingScript");

        final ScriptExecutionResult result = executable.execute();
        System.out.println("modelRepo - " + result.getOutputWriter().toString());
    }

    private void fileRepo() {
        final ScriptExecutable executable = scriptingLanguagesService
                .getExecutableByURI("file:///Work/hybris/script/simpleScript.groovy");

        final ScriptExecutionResult result = executable.execute();
        System.out.println("fileRepo - " + result.getOutputWriter().toString());
    }

    private void classPathRepo() {
        final ScriptExecutable executable = scriptingLanguagesService
                .getExecutableByURI("classpath://script/simpleScript.groovy");

        final ScriptExecutionResult result = executable.execute();
        System.out.println("classPathRepo - " + result.getOutputWriter().toString());
    }

    private void urlRepo() {
        final ScriptExecutable executable = scriptingLanguagesService
                .getExecutableByURI("https://bitbucket.org/danulos/openrepo/raw/92a9e998a5031688477ee616c62895593375a4c7/simpleScript.groovy");

        final ScriptExecutionResult result = executable.execute();
        System.out.println("urlRepo - " + result.getOutputWriter().toString());
    }

    private void scriptImpl() {
        final ScriptContent scriptContent = new SimpleScriptContent("groovy", SCRIPT_IMPL);

        final ScriptExecutable executable = scriptingLanguagesService.getExecutableByContent(scriptContent);
        final ScriptExecutionResult result = executable.execute();
        for (int i = 0; i < 5; i++) {
            ((ScriptResult)result.getScriptResult()).doAction("" + i);
        }
        LOG.info("ScriptResult.getProperty - "
                + ((ScriptResult)result.getScriptResult()).getProperty());
    }

    private void scriptWithParam() {
        final ScriptContent scriptContent = new SimpleScriptContent("groovy", SCRIPT_PARAM);
        final ScriptExecutable executable = scriptingLanguagesService.getExecutableByContent(scriptContent);

        final Map<String, Object> params = new HashMap<>();
        params.put("randomNumber", "10000");

        final ScriptExecutionResult result = executable.execute(params);

        LOG.info("scriptWithParam - " + result.getOutputWriter().toString());
    }

    private void version() {
        final ScriptExecutable executable = scriptingLanguagesService
                .getExecutableByURI("model://trainingScript");

        final ScriptExecutionResult result = executable.execute();
        System.out.println("version - " + result.getOutputWriter().toString());
    }

    private void runScriptJob() {
        cronJobService.performCronJob(
                cronJobService.getCronJob("trainingScriptCronJob"));
    }

    private void registerScriptedEventListener() {
        scriptingEventService.registerScriptingEventListener
                ("classpath://script/eventListener.groovy");
    }

    private void unregisterScriptedEventListener() {
        scriptingEventService.unregisterScriptingEventListener
                ("classpath://script/eventListener.groovy");
    }

    private void startScriptedTask() {
        final ScriptingTaskModel scriptingTask = modelService.create(ScriptingTaskModel.class);
        scriptingTask.setScriptURI("classpath://script/task/trainingScriptRunner.groovy");

        final TrainingTaskContext taskContext = new TrainingTaskContext();
        taskContext.setCode("scriptedTask");
        scriptingTask.setContext(taskContext);

        taskService.scheduleTask(scriptingTask);
    }

    /*
    * also /script/task/processor/batchProcessor.groovy
    * is started with /script/task/processor/start.groovy
    * */
}
