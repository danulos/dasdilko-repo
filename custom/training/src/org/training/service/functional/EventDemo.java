package org.training.service.functional;

import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.tx.Transaction;
import org.springframework.context.ApplicationListener;
import org.training.event.async.AsyncTrainingEvent;
import org.training.event.dynamic.DynamicTrainingEvent;
import org.training.event.dynamic.DynamicTrainingEventListener;
import org.training.event.sync.SyncTrainingEvent;
import org.training.event.transaction.TransactionTrainingEvent;
import org.training.service.AbstractDemo;
import org.training.util.TrainingUtil;

import javax.annotation.Resource;

public class EventDemo extends AbstractDemo {

    @Resource
    private EventService eventService;

    private final ApplicationListener<DynamicTrainingEvent> dynamicListener = new DynamicTrainingEventListener();

    //todo: TransactionAwareEvent

    public void demo() {
        publishSyncEvent();
        TrainingUtil.printLine();
        publishAsyncEvent();
        dynamicEvent();
        TrainingUtil.printLine();
        transactionEvent();
    }

    private void publishSyncEvent() {
        final SyncTrainingEvent event = new SyncTrainingEvent();
        event.setCode("SYNC_" + TrainingUtil.getCode());
        eventService.publishEvent(event);
        LOG.info("SyncTrainingEvent was published");
    }

    private void publishAsyncEvent() {
        final AsyncTrainingEvent event = new AsyncTrainingEvent();
        event.setCode("ASYNC_" + TrainingUtil.getCode());
        eventService.publishEvent(event);
        LOG.info("AsyncTrainingEvent was published");
    }

    private void dynamicEvent() {
        publishDynamicEvent();
        registerDynamicEventListener();
        publishDynamicEvent();
        unregisterDynamicEventListener();
        publishDynamicEvent();
    }

    private void publishDynamicEvent() {
        final DynamicTrainingEvent event = new DynamicTrainingEvent();
        event.setCode("DYNAMIC_" + TrainingUtil.getCode());
        eventService.publishEvent(event);
        LOG.info("DynamicTrainingEvent was published");
    }

    private void registerDynamicEventListener() {
        eventService.registerEventListener(dynamicListener);
        LOG.info("DynamicTrainingEventListener was registered");
    }

    private void unregisterDynamicEventListener() {
        eventService.unregisterEventListener(dynamicListener);
        LOG.info("DynamicTrainingEventListener was unregistered");
    }

    private void transactionEvent() {
        publishTransactionEvent("before tx");
        Transaction tx = Transaction.current();
        tx.begin();
        try {
            publishTransactionEvent("static");
            publishTransactionEvent(TrainingUtil.getCode());
            publishTransactionEvent("static");
            publishTransactionEvent(TrainingUtil.getCode());
            publishTransactionEvent("static");
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        publishTransactionEvent("after tx");
    }

    private void publishTransactionEvent(final String code) {
        final TransactionTrainingEvent event = new TransactionTrainingEvent();
        event.setCode("TRANSACTION_" + code);
        eventService.publishEvent(event);
        LOG.info("TransactionTrainingEvent was published - {}", code);
    }
}
