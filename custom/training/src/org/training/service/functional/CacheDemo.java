package org.training.service.functional;

import de.hybris.platform.core.Registry;
import de.hybris.platform.regioncache.CacheController;
import de.hybris.platform.regioncache.CacheStatistics;
import de.hybris.platform.regioncache.region.impl.EHCacheRegion;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.Utilities;
import org.training.model.SimpleTrainingItemModel;
import org.training.service.AbstractDemo;

import javax.annotation.Resource;

public class CacheDemo extends AbstractDemo {

    private final String QUERY = "SELECT {pk} FROM {SimpleTrainingItem} WHERE {code} LIKE ?code";

    @Resource
    private FlexibleSearchService flexibleSearchService;
    @Resource
    private ModelService modelService;

    @Resource
    private CacheController cacheController;
    @Resource
    private EHCacheRegion trainingCacheRegion;

    public void demo() {
        cachedQuery();
        nonCachedQuery();
        checkCaching();
        invalidate();
        clearCache();
    }

    private void cachedQuery() {
        final FlexibleSearchQuery fsq = new FlexibleSearchQuery(QUERY);
        fsq.addQueryParameter("code", "r01_%");
        LOG.info("nonCachedQuery count: {}", flexibleSearchService.search(fsq).getCount());
    }

    private void nonCachedQuery() {

        final FlexibleSearchQuery fsq = new FlexibleSearchQuery(QUERY);
        fsq.setDisableCaching(true);

        fsq.addQueryParameter("code", "r01_%");
        LOG.info("nonCachedQuery count: {}", flexibleSearchService.search(fsq).getCount());
    }

    private void checkCaching() {
        final CacheStatistics stats = trainingCacheRegion.getCacheRegionStatistics();
        LOG.info("size before - {}/{}", stats.getHitCount(), stats.getMissCount());
        getItem("r01_1_0_00_first_file");
        LOG.info("size after(1) - {}/{}", stats.getHitCount(), stats.getMissCount());
        getItem("r01_1_0_01_second_file");
        LOG.info("size after(2) - {}/{}", stats.getHitCount(), stats.getMissCount());
        getItem("r01_2_0_00_first_file");
        LOG.info("size after(3) - {}/{}", stats.getHitCount(), stats.getMissCount());
        getItem("r01_1_0_01_second_file");
        LOG.info("size after(4) - {}/{}", stats.getHitCount(), stats.getMissCount());
        getItem("r01_1_0_00_first_file");
        LOG.info("size after(5) - {}/{}", stats.getHitCount(), stats.getMissCount());
    }

    private SimpleTrainingItemModel getItem(final String code) {
        SimpleTrainingItemModel item = new SimpleTrainingItemModel();
        item.setCode(code);
        item = modelService.getByExample(item);
        item.setRandomNumber(item.getRandomNumber() + 1);
        modelService.save(item);
        return item;
    }

    private void invalidate() {
        final CacheStatistics stats = trainingCacheRegion.getCacheRegionStatistics();

        SimpleTrainingItemModel item = getItem("r01_1_0_00_first_file");
        LOG.info("size before - {}/{}", stats.getHitCount(), stats.getMissCount());
        Utilities.invalidateCache(item.getPk());
        item = getItem("r01_1_0_00_first_file");
        LOG.info("size after - {}/{}", stats.getHitCount(), stats.getMissCount());
    }

    private void clearCache() {
        Registry.getCurrentTenant().getCache().clear();

        cacheController.getRegions().stream()
                .filter(r -> r.getName().equals("trainingCacheRegion"))
                .findAny().get().clearCache();
    }

}
