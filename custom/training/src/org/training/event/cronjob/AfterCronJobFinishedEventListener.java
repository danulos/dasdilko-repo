package org.training.event.cronjob;

import de.hybris.platform.servicelayer.event.events.AfterCronJobFinishedEvent;
import de.hybris.platform.servicelayer.event.events.BeforeCronJobStartEvent;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AfterCronJobFinishedEventListener extends AbstractEventListener<AfterCronJobFinishedEvent > {
    @Override
    protected void onEvent(final AfterCronJobFinishedEvent beforeCronJobStartEvent) {
//        LOG.info("Got AfterCronJobFinishedEvent: {} code,", beforeCronJobStartEvent.getCronJob());
    }

    protected static final Logger LOG = LoggerFactory.getLogger(AfterCronJobFinishedEventListener.class);
}
