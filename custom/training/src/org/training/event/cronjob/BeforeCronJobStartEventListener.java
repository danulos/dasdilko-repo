package org.training.event.cronjob;

import de.hybris.platform.servicelayer.event.events.BeforeCronJobStartEvent;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BeforeCronJobStartEventListener extends AbstractEventListener<BeforeCronJobStartEvent> {
    @Override
    protected void onEvent(final BeforeCronJobStartEvent beforeCronJobStartEvent) {
//        LOG.info("Got BeforeCronJobStartEvent: {} code,", beforeCronJobStartEvent.getCronJob());
    }

    protected static final Logger LOG = LoggerFactory.getLogger(BeforeCronJobStartEventListener.class);
}
