package org.training.event.async;

import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.io.Serializable;

public class AsyncTrainingEvent
        extends AbstractEvent implements ClusterAwareEvent {

    private String code;

    public AsyncTrainingEvent() {
        super();
    }

    public AsyncTrainingEvent(final Serializable source) {
        super(source);
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public boolean publish(int sourceNodeId, int targetNodeId) {
        // works on target node only
        return true;
    }

}
