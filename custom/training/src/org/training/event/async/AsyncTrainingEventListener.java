package org.training.event.async;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AsyncTrainingEventListener extends AbstractEventListener<AsyncTrainingEvent> {
    protected static final Logger LOG = LoggerFactory.getLogger(AsyncTrainingEventListener.class);

    @Override
    protected void onEvent(final AsyncTrainingEvent eventExportEvent) {
        LOG.info("Got AsyncTrainingEvent: {} code,", eventExportEvent.getCode());
    }
}
