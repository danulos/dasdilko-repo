package org.training.event.dynamic;

import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.io.Serializable;

public class DynamicTrainingEvent extends AbstractEvent {

    private String code;

    public DynamicTrainingEvent() {
        super();
    }

    public DynamicTrainingEvent(final Serializable source) {
        super(source);
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
