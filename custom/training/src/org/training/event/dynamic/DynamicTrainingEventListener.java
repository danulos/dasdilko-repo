package org.training.event.dynamic;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DynamicTrainingEventListener extends AbstractEventListener<DynamicTrainingEvent> {
    protected static final Logger LOG = LoggerFactory.getLogger(DynamicTrainingEventListener.class);

    @Override
    protected void onEvent(final DynamicTrainingEvent eventExportEvent) {
        LOG.info("Got DynamicTrainingEvent: {} code,", eventExportEvent.getCode());
    }
}
