package org.training.event.transaction;

import de.hybris.platform.servicelayer.event.TransactionAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import org.training.util.TrainingUtil;

import java.io.Serializable;

public class TransactionTrainingEvent
        extends AbstractEvent implements TransactionAwareEvent {

    @Override
    public boolean publishOnCommitOnly() {
        return true;
    }

    @Override
    public Object getId() {
        return code;
    }

    private String code;

    public TransactionTrainingEvent() {
        super();
    }

    public TransactionTrainingEvent(final Serializable source) {
        super(source);
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
