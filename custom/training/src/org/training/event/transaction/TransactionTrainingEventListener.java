package org.training.event.transaction;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TransactionTrainingEventListener extends AbstractEventListener<TransactionTrainingEvent> {
    protected static final Logger LOG = LoggerFactory.getLogger(TransactionTrainingEventListener.class);

    @Override
    protected void onEvent(final TransactionTrainingEvent eventExportEvent) {
        LOG.info("Got TransactionTrainingEvent: {} code,", eventExportEvent.getCode());
    }
}
