package org.training.event.sync;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SyncTrainingEventListener extends AbstractEventListener<SyncTrainingEvent> {
    protected static final Logger LOG = LoggerFactory.getLogger(SyncTrainingEventListener.class);

    @Override
    protected void onEvent(final SyncTrainingEvent eventExportEvent) {
        LOG.info("Got SyncTrainingEvent: {} code,", eventExportEvent.getCode());
    }
}
