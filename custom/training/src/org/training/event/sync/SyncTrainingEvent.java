package org.training.event.sync;

import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.io.Serializable;

public class SyncTrainingEvent extends AbstractEvent {

    private String code;

    public SyncTrainingEvent() {
        super();
    }

    public SyncTrainingEvent(final Serializable source) {
        super(source);
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
