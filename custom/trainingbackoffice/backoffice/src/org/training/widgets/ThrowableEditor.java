package org.training.widgets;

import com.hybris.cockpitng.core.util.Validate;
import com.hybris.cockpitng.editors.EditorContext;
import com.hybris.cockpitng.editors.EditorListener;
import com.hybris.cockpitng.editors.impl.AbstractCockpitEditorRenderer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Textbox;

public class ThrowableEditor extends AbstractCockpitEditorRenderer<Throwable> {
    public ThrowableEditor() { }

    public void render(Component parent, EditorContext<Throwable> context, EditorListener<Throwable> listener) {
        Validate.notNull("All parameters are mandatory", new Object[]{parent, context, listener});
        final Throwable value = context.getInitialValue();
        Textbox textbox = new Textbox();
        textbox.setParent(parent);
        textbox.setWidth("100%");
        textbox.setHeight("100%");
        textbox.setMultiline(true);
        textbox.setReadonly(true);
        textbox.setText((value != null ? value.getClass().getName() + ": " + value.getMessage() : ""));
    }
}
