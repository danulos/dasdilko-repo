/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.controller;

import static org.training.constants.IrequiretrainingConstants.PLATFORM_LOGO_CODE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.training.service.IrequiretrainingService;


import io.jsonwebtoken.Claims; //training_core
//import com.google.gson.ExclusionStrategy; //req_training_core
//import io.vavr.Tuple; //req_training_web

//import javax.xml.ws.AsyncHandler; //training_web

@Controller
public class IrequiretrainingHelloController
{
	@Autowired
	private IrequiretrainingService irequiretrainingService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(final ModelMap model)
	{
		model.addAttribute("logoUrl", irequiretrainingService.getHybrisLogoUrl(PLATFORM_LOGO_CODE));
		return "welcome";
	}
}
