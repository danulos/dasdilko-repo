/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.setup;

import static org.training.constants.IrequiretrainingConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import org.training.constants.IrequiretrainingConstants;
import org.training.service.IrequiretrainingService;


@SystemSetup(extension = IrequiretrainingConstants.EXTENSIONNAME)
public class IrequiretrainingSystemSetup
{
	private final IrequiretrainingService irequiretrainingService;

	public IrequiretrainingSystemSetup(final IrequiretrainingService irequiretrainingService)
	{
		this.irequiretrainingService = irequiretrainingService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		irequiretrainingService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return IrequiretrainingSystemSetup.class.getResourceAsStream("/irequiretraining/sap-hybris-platform.png");
	}
}
