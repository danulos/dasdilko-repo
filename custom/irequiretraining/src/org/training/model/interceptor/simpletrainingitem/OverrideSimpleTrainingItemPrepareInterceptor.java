package org.training.model.interceptor.simpletrainingitem;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.training.model.SimpleTrainingItemModel;
import org.training.util.TrainingUtil;

public class OverrideSimpleTrainingItemPrepareInterceptor implements PrepareInterceptor<SimpleTrainingItemModel> {

    @Override
    public void onPrepare(final SimpleTrainingItemModel simpleTrainingItem, final InterceptorContext ctx) {
//        TrainingUtil.printMessage("OverrideSimpleTrainingItemPrepareInterceptor");
//        System.out.println("p2 -> p1 " + ctx.getAttribute("p1"));
//        System.out.println("p2 -> p11 " + ctx.getTransientStorage().get("p11"));

        ctx.setAttribute("p2", "p2");
        ctx.getTransientStorage().put("p21", "p21");


    }
}
