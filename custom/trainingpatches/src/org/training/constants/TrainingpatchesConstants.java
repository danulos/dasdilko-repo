/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.training.constants;

/**
 * Global class for all Trainingpatches constants. You can add global constants for your extension into this class.
 */
public final class TrainingpatchesConstants extends GeneratedTrainingpatchesConstants
{
	public static final String EXTENSIONNAME = "trainingpatches";

	private TrainingpatchesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "trainingpatchesPlatformLogo";
}
