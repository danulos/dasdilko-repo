package org.training.setup;

import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.patches.AbstractPatchesSystemSetup;
import org.training.constants.TrainingpatchesConstants;

import java.util.List;

@SystemSetup(extension = TrainingpatchesConstants.EXTENSIONNAME)
public class TrainingpatchesSystemSetup extends AbstractPatchesSystemSetup {

    @Override
    @SystemSetup(type = SystemSetup.Type.ESSENTIAL, process = SystemSetup.Process.ALL)
    public void createEssentialData(final SystemSetupContext setupContext) {
        super.createEssentialData(setupContext);
    }

    @Override
    @SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
    public void createProjectData(final SystemSetupContext setupContext) {
        super.createProjectData(setupContext);
    }

    @Override
    @SystemSetupParameterMethod
    public List<SystemSetupParameter> getInitializationOptions() {
        return super.getInitializationOptions();
    }
}
