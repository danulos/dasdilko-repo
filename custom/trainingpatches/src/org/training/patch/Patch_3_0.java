package org.training.patch;

import de.hybris.platform.patches.Rerunnable;

public class Patch_3_0 extends AbstractTrainingPatch implements Rerunnable {

    public Patch_3_0() {
        super("3_0", "3_0");
    }

    @Override
    protected void applyPatch() {
        importFile("r01_3_0_00_first_file.impex", false, null, null);
        importFile("r01_3_0_01_second_file.impex", false, null, null);
    }
}
