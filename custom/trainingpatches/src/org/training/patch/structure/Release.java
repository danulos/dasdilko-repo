package org.training.patch.structure;

public enum Release implements de.hybris.platform.patches.Release {

    R1("01"), R2("02"), E1("ERROR");

    private String releaseId;

    Release(final String releaseId) {
        this.releaseId = releaseId;
    }

    @Override
    public String getReleaseId() {
        return this.releaseId;
    }

}
