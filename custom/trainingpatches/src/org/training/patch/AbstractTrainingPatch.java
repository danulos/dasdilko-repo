package org.training.patch;

import de.hybris.platform.patches.AbstractPatch;
import de.hybris.platform.patches.data.ImpexHeaderOption;
import de.hybris.platform.patches.data.ImpexImportUnitOption;
import org.training.patch.structure.Release;
import org.training.patch.structure.StructureState;

import java.util.Collections;

public abstract class AbstractTrainingPatch extends AbstractPatch {

    public AbstractTrainingPatch(String patchId, String patchName) {
        super(patchId, patchName, Release.R1, StructureState.V1);
    }

    @Override
    public void createProjectData(de.hybris.platform.patches.organisation.StructureState structureState) {
        applyPatch();
    }

    protected abstract void applyPatch();

    protected void importFile(final String fileName, final boolean runAgain,
                              final ImpexImportUnitOption[] importOptions,
                              final ImpexHeaderOption[][] headerOptions) {
        importData(fileName, null, Collections.emptyList(), runAgain, importOptions, headerOptions);
    }
}
