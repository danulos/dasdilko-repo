package org.training.patch;

import de.hybris.platform.patches.data.ImpexHeaderOption;
import de.hybris.platform.patches.data.ImpexImportUnitOption;

public class Patch_1_0 extends AbstractTrainingPatch {

    public Patch_1_0() {
        super("1_0", "1_0");
    }

    @Override
    protected void applyPatch() {
        final ImpexHeaderOption headerOption = new ImpexHeaderOption();
        headerOption.setMacro("$nmb=2");
        final ImpexHeaderOption[] headerOptions = new ImpexHeaderOption[] {headerOption};
        importFile("r01_1_0_00_first_file.impex", false, null, new ImpexHeaderOption[][] {headerOptions});

        importFile("not_found.impex", false, null, null);

        final ImpexImportUnitOption[] importOptions = new ImpexImportUnitOption[2];
        importOptions[0] = ImpexImportUnitOption.LEGACY_MODE;
        importOptions[1] = ImpexImportUnitOption.SINGLE_THREAD;
        importFile("r01_1_0_01_second_file.impex", false, importOptions, null);
    }
}
