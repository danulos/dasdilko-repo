package org.training.patch;

import de.hybris.platform.patches.exceptions.PatchImportException;

import java.util.Random;

public class Patch_2_0 extends AbstractTrainingPatch {

    public Patch_2_0() {
        super("2_0", "2_0");
    }

    @Override
    protected void applyPatch() {
        importFile("r01_2_0_00_first_file.impex", false, null, null);
        if (new Random().nextBoolean()) {
            throw new PatchImportException("Error message...");
        }
        importFile("r01_2_0_01_second_file.impex", false, null, null);
    }
}
