# README #

Welcome to the DasDilko repo!

Here you can find the code that I've used in my videos.

Contact:

* 100dilko.da@gmail.com
  
* [djinni](https://djinni.co/q/0b40447df4/)
  
* [LinkedIn](https://www.linkedin.com/in/danylo-stodilko-692326137)
  
* [YouTube](https://www.youtube.com/channel/UCoQA_b4OQMXgt14DI_0wfiw)

